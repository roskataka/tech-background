/* eslint-disable react/jsx-key */
import React, { useState } from 'react';
import { useHistory } from 'react-router';

const Sign = () => {
    const history = useHistory();
    const [form, setForm] = useState({
        username: {
            placeholder: 'username',
            value: '',
            type: 'text'
        },
        password:{
            placeholder: 'password',
            value: '',
            type: 'password'
        }
    })
    const registerRedirect = () => {
        history.push('/register')
    }
    const [par, setPar] = useState('');
    const handleSubmit = event =>{
        event.preventDefault();
        if(form.username.value.length<3 || form.password.value<3){
            setPar("Credentials are too short!")
        } else {
            const data = Object.keys(form)
            .reduce((acc, elementKey) => {
                return {
                    ...acc,
                    [elementKey]: form[elementKey].value
                }
            }, {})
            const dataRes = JSON.stringify(data);
            fetch('http://localhost:3500/users/signin',{
                mode: 'cors',
                method: 'POST',
                body: dataRes,
                headers: {
                    'Content-Type':'application/json',
                }
            })
            .then(result => result.json())
            .then((response)=> {
                if(response){
                    setPar('');
                    localStorage.setItem('token',response['token']);
                    history.push('/');
                }
            })
            .catch(setPar('Wrong username or password!'))
        }


    }

    const handleInputChange = event => {
        const {name, value} = event.target;

        const updatedElement = { ...form[name], value }
        const updatedForm = { ...form, [name]:updatedElement }
        setForm(updatedForm)

    }

    const formElements = Object.keys(form)
    .map(name => {
        return{
            id: name,
            config: form[name]
        }
    })
    .map(({id, config})=>{
        return (
            <div key={id+'login2'}className='login-duo' id={id+'-div'}>
                <label key={id+'login1'}>{id}:</label>
                <br></br>
                <input
                    className='login-input'
                    type={config.type}
                    key={id+'login'}
                    name={id}
                    placeholder={config.placeholder}
                    value={config.value}
                    onChange={handleInputChange}
                />
            </div>
        )
    })
    return <div className="login-screen">
        <div className="intro-div"></div>
        <div className="login-window">
            <div className="login-placeholder">
                <label id="login-screen-label">Login</label>
            </div>
            <form className="login-form" onSubmit={handleSubmit}>
                {formElements}
                <button type="submit" className="login-button">Sumbit</button>
            </form>
            <p className="above">{par}</p>
            <button onClick={registerRedirect} className="register-redirection">Register</button>
        </div>
        <div className="intro-div"></div>

    </div>
}

export default Sign;