import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import NavBar from '../side-services/NavBar';

// eslint-disable-next-line max-statements
const fetchUserData = () => {
    const history = useHistory();
    // eslint-disable-next-line no-console
    console.log(history.location.pathname)
    const url = `http://localhost:3500/users/username`
    const token = localStorage.getItem('token');
    const { username } = useParams();
    const [userInfo, loadUserInfo] = useState([]);
    let content = null;
    // Const goBack = history.goBack()
    useEffect(() => {

        fetch(`${url}/${username}`,{
            mode: 'cors',
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        })
            .then((res)=> res.json())
            .then((res) => loadUserInfo(res))
    } , []);
    if(userInfo){
        content =
        <div className="userPage" style={{textAlign:'center'}}>
            <h1 className="single-profile-nick">Username: {userInfo.username}</h1>
            <h1 className="single-profile-fname">First Name: {userInfo.first_name}</h1>
            <h1 className="single-profile-lname">Last Name: {userInfo.last_name}</h1>
            <h1 className="single-profile-date">Register date: {userInfo.createdOn}</h1>
        </div>
    }
    return <div>
            {content}
            <NavBar/>
    </div>
}

export default fetchUserData;