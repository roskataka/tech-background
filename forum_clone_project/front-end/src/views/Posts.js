/* eslint-disable react/prop-types */
import React from 'react';
import SinglePost from './SinglePost';
// eslint-disable-next-line capitalized-comments
// import SinglePost from './SinglePost';

const Posts = (props) => {

    return <>{props.obj.map(cur => {
        return <SinglePost key={'post ' + cur.id} obj={cur} />
    })}</>
}

export default Posts