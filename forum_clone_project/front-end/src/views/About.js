import AuthView from '../side-services/Auth';
import React from 'react';
import { useHistory } from 'react-router-dom';

/* eslint-disable */
const About = () => {
    const history = useHistory();
    const goBack = () => {
        history.goBack();
    }
    return  <div className="about-screen">
        <div className="about-window">
            <div className="about-data">
                <h1 className="about-header">About us:</h1>
                <div className="about-text">                <p>We're two students from Telerik alpha academy's courses! <br></br>
                <br></br>Here you can find our profiles!
                </p></div>
                <br></br>
                <a className="tooltip1" href="https://www.instagram.com/estabilish.ink/">Rosen Boyanov<span className="tooltiptext1">Oosuke Ren</span></a>
                <br></br>
                <br></br>
                <a href="https://www.instagram.com/estabilish.ink/">Georgi Dimov</a>
                <br></br>
                <br></br>
                <p className="about-goback" onClick={goBack}>Go back!</p>
                <AuthView props="hello"/>
            </div>
        </div>
    </div>
}

export default About;