/* eslint-disable camelcase */
/* eslint-disable react/jsx-key */
/* eslint-disable no-ternary */
/* eslint-disable no-alert */
/* eslint-disable no-lonely-if */
/* eslint-disable no-negated-condition */
/* eslint-disable max-statements */
import React, { useState } from 'react';
import Tooltip from '../side-services/Tooltip';
import renderTooltip from '../side-services/User-check';
import { useHistory } from 'react-router';

const Register = () => {
    const history = useHistory();
    const [form, setForm] = useState({
        username: {
            placeholder: 'username',
            value: '',
            type: 'text',
            validation: {
                required: true,
                minLength: 6,
                maxLength: 30
            },
            valid: false,
            touched: false
        },
        first_name: {
            placeholder: 'First name',
            value: '',
            type: 'text',
            validation: {
                required: true,
                minLength: 3,
                maxLength: 30
            },
            valid: false,
            touched: false
        },
        last_name: {
            placeholder: 'Last name',
            value: '',
            type: 'text',
            validation: {
                required: true,
                minLength: 3,
                maxLength: 30
            },
            valid: false,
            touched: false
        },
        email: {
            placeholder: 'e-mail',
            value: '',
            type: 'email',
            validation: {
                required: true,
                minLength: 10,
                maxLength: 30
            },
            valid: false,
            touched: false
        },
        password:{
            placeholder: 'password',
            value: '',
            type: 'password',
            validation: {
                required: true,
                minLength: 5,
                maxLength: 30
            },
            valid: false,
            touched: false
        },
        repeat: {
            placeholder: 'password',
            value: '',
            type: 'password',
            validation: {
                required: true,
                minLength: 6,
                maxLength: 30
            },
            valid: false,
            touched: false
        }

    });

    const [isFormValid, setFormValid] = useState(false);
    const [par, setPar] = useState('');

    const isInputValid = (value, validation) => {
        if(validation.minLength<=value.length<=validation.maxLength && value!==""){
            return true;
        }
        return false;
    }

    const check = renderTooltip(form.username.value);
    const handleSubmit = (event) => {
        event.preventDefault();
        const formValid = Object.values(form).every(el=>isInputValid(el.value, el.validation));
        setFormValid(formValid);
        if(!isFormValid){
            const res = Object.values(form).reduce((acc, val) =>{
                let add = '';
                if(val.valid === false){
                    if(acc !== "Invalid fields: "){
                        add += `,${val.placeholder}`
                    } else {
                        add += `${val.placeholder}`
                    }
                }
                return acc + add;
            },"Invalid fields: ")
            setPar(res);
        } else {
            if(!check){
                if(form.password.value !== form.repeat.value){
                    setPar("Password not matching!")
                } else {
                    setPar("")
                    const data = Object.keys(form)
                    .reduce((acc, key)=>{
                        return {
                            ...acc,
                            [key]:form[key].value
                        }
                    },{});
                    const dataRes = JSON.stringify(data);
                    fetch('http://localhost:3500/users/register',{
                        mode: 'cors',
                        method: 'POST',
                        body: dataRes,
                        headers: {
                            'Content-Type':'application/json',
                        }
                    })
                    .then(result => result.json())
                    .then((response)=> {
                        if(response){
                            setPar('');
                            localStorage.setItem('token',response['token']);
                            history.push('/');
                        } else {
                            setPar('There was an error with some of your information credentials!')
                        }
                    })
                    .catch(setPar("Connection error, please try again later!"))
                }
            } else {
                alert("A user with this name already exists, please try another one!");
            }


        }
    }
    const handleInputChange = (event) => {
        const { name, value } = event.target;

        const updatedControl = {...form[name]};

        updatedControl.value = value;
        updatedControl.touched = true;

        updatedControl.valid = isInputValid(value, updatedControl.validation)

        const updatedForm = {...form, [name]: updatedControl };
        setForm(updatedForm);

        const formValid = Object.values(updatedForm).every(el => el.valid);
        setFormValid(formValid);


    }

    const callData = {
        bool: check,
        tooltip: "tooltiptext"
    }
    const formElements = Object.keys(form)
    .map(name => {
        return{
            id: name,
            config: form[name]
        }
    })
    .map(({id, config})=>{
        const isValidCSSClass = config.valid
        ?'valid'
        :'invalid';
        const classes = isValidCSSClass

        return (
            <div key={id+'register'}className='reg-box' id={id+'-div1'}>
                <label>{config.placeholder}:</label>
                <br></br>
                <input
                    className='reg-input'
                    type={config.type}
                    key={id}
                    name={id}
                    valid={classes}
                    placeholder={config.placeholder}
                    value={config.value}
                    onChange={handleInputChange}
                />
            </div>
        )
    })

    const loginRedirect = () =>{
        history.push('/login')
    }
    return <div className="reg-screen">
        <div className="reg-window">
            <div className="reg-placeholder">
                <label id="reg-screen-label">Register</label>
            </div>
            <form className="reg-form" onSubmit={handleSubmit}>
                {formElements}
                <button type="submit" className="reg-button">Submit</button>
            </form>
            <div className="reg-response"><p className="above1">{par}</p></div>
            <button onClick={loginRedirect} className="login-redirection">Login</button>
            <Tooltip obj={callData}/>
        </div>
        </div>
}

export default Register;