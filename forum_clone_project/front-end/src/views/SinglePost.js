/* eslint-disable capitalized-comments */
/* eslint-disable multiline-comment-style */
/* eslint-disable max-statements */
/* eslint-disable no-empty */
/* eslint-disable no-alert */
/* eslint-disable no-console */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';

const SinglePost = (props) => {
    const token = localStorage.getItem('token');
    const [form, setForm] = useState({
        title: {
            placeholder: 'Title',
            value: props.obj.title,
            type: 'text'
        },
        content: {
            placeholder: 'Write your questions or discussions here!',
            value: props.obj.content,
            type: 'textarea'
        }
    });
    const [bool, flipBool] = useState('none');
    const [safe, turnOffSafe] = useState(true);
    const [deletePost, loadDelete] = useState('');
    const [deletedPost, deleteDiv] = useState('block');
    const [element, changeElement] = useState('div');
    const [username, changeUsername] =useState('');
    const [counter, increment] =useState(0);
    const deleteHandler =()=>{
        if(safe){
            alert("If you want to delete this post press the delete button again!");
            turnOffSafe(false);
        } else {
            loadDelete('Yes');
        }
    }
    const handleInputChange = event => {
        const {name, value} = event.target;
        const updatedElement = { ...form[name], value }
        const updatedForm = { ...form, [name]:updatedElement }
        setForm(updatedForm)
    }
    const updateHandler = () => {
        if(element==="div"){
            changeElement('textarea');
        } else {
            changeElement('div');
        }
    }
    useEffect(()=>{
        if(deletePost!==''){
            fetch(`http://localhost:3500/posts/${props.obj.id}`,{
                mode: 'cors',
                method: 'DELETE',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization':`Bearer ${token}`,
                }
            })
            .then(res=>res.json())
            .then(res => console.log(res));
            deleteDiv('none')
        }
    },[deletePost])
    useEffect(()=>{
        if(props.obj.truth){
            flipBool('inline')
        }
    },[bool]);
    useEffect(()=>{
        if(element==="div" && counter!==0){
            const data = {
                title: form.title.value,
                content: form.content.value
            }
            const dataRes = JSON.stringify(data)
            fetch(`http://localhost:3500/posts/${props.obj.id}`,{
                mode: 'cors',
                method: 'PUT',
                body: dataRes,
                headers: {
                    'Content-Type':'application/json',
                    'Authorization':`Bearer ${token}`,
                }
            })
            .then(res => res.json())
            .then(res => console.log(res))
        }
        if(element==="textarea"){
            increment(prev => prev+1);
        }
    },[element])
    useEffect(()=>{
        if(props.obj.users_id){
            fetch(`http://localhost:3500/users/${props.obj.users_id}`,{
            mode: 'cors',
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${token}`,
            }
        })
        .then(res=> res.json())
        .then(res=> {
            if(res.username){
                changeUsername(res.username)
            }
        })
        .catch(ape=>console.log(ape))
        }
    },[])
    const FormElements = () => {
         if(element ==="div"){
            return <div className="post-post-section"><div className="single-post-title"><h1>{form.title.value}</h1></div><p className="single-post-text">{form.content.value}</p></div>

         }
        return <form className="post-post-section">
            <div className="single-post-title"><h1>{form.title.value}</h1></div>
            <textarea
            autoFocus
            name="content"
            value={form.content.value}
            onChange={handleInputChange}
            key="content2"
            // eslint-disable-next-line func-names
            onFocus={function (event) {
                const val = event.target.value;
                event.target.value = '';
                event.target.value = val;
              }}
            className="single-post-text1">
                </textarea>
                <div className="update-button-div"><button className="update-post-button1" type= "button" onClick={updateHandler}>Update</button></div>
            </form>
    }
    return <>
        <div style={{display:`${deletedPost}`}} className="single-post">
        <div className="post-user-section"><a className="user-link" href={"/user/"+username}>{username}</a><a onClick={deleteHandler} style={{display:`${bool}`}} className="delete-post-button">X</a><a onClick={updateHandler} style={{display:`${bool}`}} className="update-post-button">🔧</a></div>
        <FormElements/>
        <div className="post-comment-section">Comments</div>
    </div>
    <br></br>
    </>
}

export default SinglePost;