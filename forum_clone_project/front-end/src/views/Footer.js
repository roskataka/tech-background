import React from 'react';

const Footer = () => {
    return <div className="footer">
        <p>© COPYRIGHT BY TRICK2TEL ALL RIGHTS RESERVED.</p>
        <a className="about-link" href="/about">About us</a>
    </div>
}

export default Footer;