/* eslint-disable multiline-comment-style */
/* eslint-disable capitalized-comments */
/* eslint-disable no-console */
/* eslint-disable max-statements */
/* eslint-disable eqeqeq */
// eslint-disable-next-line sort-imports

import React, { useEffect, useState } from 'react';

import NavBar from './side-services/NavBar';

import Posts from './views/Posts';
import backend from './constants.js';
import { useHistory } from 'react-router-dom';

const Home = () =>{
    const [obj, pushObj] = useState([]);
    const history = useHistory()
    const token = localStorage.getItem('token');

    const [form, setForm] = useState({
        title: {
            placeholder: 'Title',
            value: '',
            type: 'text'
        },
        content: {
            placeholder: 'Write your questions or discussions here!',
            value: '',
            type: 'textarea'
        }
    });
    const [click, getclicked] = useState(0);
    const [par, setPar]=useState('');
    const [scroll, scrollDown] = useState(0);
    // When scrolled to the bottom - increase the page offset for the query
    const handleScroll = (event) => {
        const bottom = event.target.scrollHeight - event.target.scrollTop === event.target.clientHeight;
        if (bottom) {
            scrollDown(prev => prev+1)
        }
    }
    const handleSubmit = event => {
        event.preventDefault();
        getclicked(prev=> prev+1);

    }
    const handleInputChange = event => {
        const {name, value} = event.target;

        const updatedElement = { ...form[name], value }
        const updatedForm = { ...form, [name]:updatedElement }
        setForm(updatedForm)

    }
    // When submitted!
    useEffect(()=>{
        if(!click==0){

            if(form.content.value.length<5){
                setPar('Post content is too short!');
            }
            if(form.title.value.length<3){
                setPar('Title is too short!');
            }
            if(form.content.value.length>5 && form.title.value.length>3){
                setPar('');
                const data = Object.keys(form)
                .reduce((acc, elementKey) => {
                    return {
                        ...acc,
                        [elementKey]: form[elementKey].value
                    }
                }, {})
                const dataRes = JSON.stringify(data);
                fetch('http://localhost:3500/posts/',{
                    mode: 'cors',
                    method: 'POST',
                    body: dataRes,
                    headers: {
                        'Content-Type':'application/json',
                        'Authorization':`Bearer ${token}`,
                    }
                })
                .then(res=> res.json())

                .then(res=> {
                    if(res.title){
                        // eslint-disable-next-line camelcase
                        pushObj([{title:res.title, content: res.content, id:res.content, users_id: res.users_id, truth:true},...obj])
                        setForm({title:{
                            placeholder: 'Title',
                            value: '',
                            type: 'text'
                        },
                        content: {
                            placeholder: 'Write your questions or discussions here!',
                            value: '',
                            type: 'textarea'
                        }})
                    } else {
                        setPar('A post with this title already exist!')
                    }
                })

                .catch(()=>{
                    localStorage.removeItem('token');
                    history.replace('/login');
                })
            }
        }

    },[click])
    // When the url is entered!
    useEffect(()=>{
        scrollDown(prev=>prev+1)
        if(!localStorage.getItem('token')){
            history.push('/login')
        }
    },[history]);
    useEffect(()=>{
        if(scroll===0){
            fetch(`http://localhost:3500/posts/recent/${scroll}`,{
            mode: 'cors',
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${token}`,
            }
        })
        .then(res=> res.json())
        .then(res=> pushObj([...res]))
        } else {
            fetch(`http://localhost:3500/posts/recent/${scroll}`,{
            mode: 'cors',
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${token}`,
            }
        })
        .then(res=> res.json())
        .then(res=> pushObj([...obj,...res]))
        }
    },[scroll])
    const formElements = Object.keys(form)
    .map(name => {
        return{
            id: name,
            config: form[name]
        }
    })
    .map(({id, config})=>{
        if(config.type==="text"){
            return(
                <div key={id+"home"} className="post-request-form">
                    <div className="post-request-label"><label className="post-creation-labels">{id[0].toUpperCase()+id.slice(1,id.length)}:</label></div>
                    <input
                    type="text"
                    key={id+"home1"}
                    name={id}
                    placeholder={config.placeholder}
                    value={config.value}
                    onChange={handleInputChange}
                    className={id+'-post'}
                    >
                    </input>
                </div>
            )
        }
            return(
                <div key={id+"home"} className="post-request-form">
                    <div className="post-request-label"><label className="post-creation-labels">{id[0].toUpperCase()+id.slice(1,id.length)}:</label></div>
                    <textarea
                    type="text"
                    key={id+"home1"}
                    name={id}
                    placeholder={config.placeholder}
                    value={config.value}
                    onChange={handleInputChange}
                    className={id+'-post'}
                    >
                    </textarea>
                </div>
            )

    })
    return <div className="home-screen">
    <div className="home-window" onScroll={handleScroll}>
        <form onSubmit={handleSubmit} className="post-creation">
            {formElements}
            <button type="submit" className="post-request-send">Post</button>
            <div className="tooltip2"><p>{par}</p></div>
        </form>
        <br></br>
        <Posts obj={obj}/>

    </div>
        <NavBar/>
    </div>
}

export default Home;