/* eslint-disable react/prop-types */

import React, { useEffect, useState } from 'react';
import LogOut from './LogOut';
import SearchBar from './SearchBar';


const AuthView = (props) => {
    const token = localStorage.getItem('token');
    const [vis, toggleVis] = useState('none');
    const word = props.props;
    useEffect(()=>{
        let mounted = true;
        if(mounted){
            fetch('http://localhost:3500/users/auth',{
                method: 'PUT',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization':`Bearer ${token}`,
                }
            })
            .then(res => res.json())
            .then(res => {
                if(res.Success){
                    toggleVis('block');
                }
            })
        }
        return function pesho () {
            mounted=false;
        }
    },[word]);
    return <div className="authenticate" style={{display:`${vis}`}}><SearchBar/>
        <LogOut/>
    </div>
}

export default AuthView;