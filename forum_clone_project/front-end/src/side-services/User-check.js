import {useEffect, useState} from 'react';

const renderTooltip = (props) => {
    const [exists, flipExists] = useState(false);
    // Checks if there's already a user with the specific username
    const checkUsername = async (value) => {
        await fetch(`http://localhost:3500/users/user/${value}`,{
            mode: 'cors',
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
            }
        })
        .then(response => response.json())
        .then(res => {
            flipExists(res);
        })
        .catch()
    }


    useEffect(()=> {
        let mounted = true;
            if(mounted){
                checkUsername(props);
            }
        return function cleanup (){
            mounted = false;
        }
    })
    // eslint-disable-next-line no-console
    return exists;
}

export default renderTooltip;