import React from 'react';
import { useHistory } from 'react-router-dom';

const LogOut = () => {
    const history = useHistory();
    const buttonHandler = () => {
        localStorage.removeItem('token');
        history.push('/login');
    }
    return <button className="logout" onClick={buttonHandler}>Log out</button>
}

export default LogOut;