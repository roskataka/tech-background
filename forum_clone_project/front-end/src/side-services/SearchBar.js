import React from 'react';

const SearchBar = () => {
    return <div className="search-container">
                <div className='cell'><select className='search-category'><option value='posts'>Posts</option><option value='users'>Users</option></select></div>
                <div className='cell'><input type='search' placeholder='Search'/></div>
                <div className='cell'><div className='search-button'>🔎</div></div>
            </div>
}

export default SearchBar;