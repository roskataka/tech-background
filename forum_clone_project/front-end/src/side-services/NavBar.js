import LogOut from './LogOut';
import React from 'react';
import SearchBar from './SearchBar';


const NavBar = ()=> {

    return <div><SearchBar/>
        <LogOut/>
    </div>
}

export default NavBar