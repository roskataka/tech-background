import './App.css';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import About from './views/About';
import Footer from './views/Footer';
import Header from './Header';
import Home from './Home.js';
// eslint-disable-next-line sort-imports
import React from 'react';
import Register from './views/Register';
import Sign from './Sign';
import SingleUser from './views/SingleUser.js'
import Users from './Users.js'


const App =() =>{

  /*
   * Const [path, changePath] = useState('/');
   * const history = useHistory();
   * UseEffect(()=>{
   *   return history.listen(location => changePath(location))
   * },history)
   */
  return (
    <BrowserRouter>
      <Header/>
      <Switch>
        <Route path="/user/:username" component = {SingleUser}></Route>
        <Route path="/" exact component={Home}></Route>
        <Route path="/users" exact component={Users}></Route>
        <Route path="/login" component={Sign}></Route>
        <Route path="/register" component={Register}></Route>
        <Route path="/about" exact component = {About}></Route>

        <Route path="*" component={Home}></Route>
      </Switch>
      <Footer/>
    </BrowserRouter>
  );

}

export default App;