export default (validator) => async (req, res, next) => {
    let error = null;

    if (Object.keys(validator).length !== Object.keys(req.body).length) {
        return res.status(400).json('Bad Request');
    }

    Object.keys(validator).forEach((key) => {
        if (!validator[key](req.body[key])) {
            error = key;
        }
    });

    if (error) {
        return res.status(400).json(`Bad Request - Invalid ${error}`);
    }

    await next();
};