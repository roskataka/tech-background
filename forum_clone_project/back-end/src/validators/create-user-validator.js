export default {
  username: (value) => typeof value === 'string' && value.length > 0,
  display_name: (value) => typeof value === 'string' && value.length > 0,
  password: (value) => typeof value === 'string' && value.length > 0,
};
