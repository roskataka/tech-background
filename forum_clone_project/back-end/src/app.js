import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import passport from 'passport';
import { jwtStrategy } from './auth/strategy.js';
import { usersRouter } from './controllers/users-controller.js';
import { postsRouter } from './controllers/posts-controller.js';



const PORT = 3500;

const app = express();

passport.use(jwtStrategy);

app.use(cors(), helmet(), express.json());

app.use(passport.initialize());


app.use('/users', usersRouter);
app.use('/posts', postsRouter);
app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));
