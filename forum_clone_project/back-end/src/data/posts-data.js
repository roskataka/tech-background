import pool from "./pool.js";
import errors from "../services/errors.js";


const getAllPosts = async () => {
    const sql = `
    SELECT *
    FROM posts
    `;
    
    return await pool.query(sql);
}


const getPostBy = async (column,value) => {
    const sql = `
    SELECT title, content , created_on, users_id
    FROM posts
    WHERE ${column} = '${value}';
    `;

    const res = await pool.query(sql,[column,value]);
    return res[0]
}
const deletePost = async(id, user_id) => {
    const post = await getPostBy('id', id);
    if(post.users_id == user_id){
        console.log("pesho")
        const sql = `
        DELETE from posts WHERE
        id = ${id}
        `
        return await pool.query(sql,[id]);
    }
    return {
        error: errors.OPERATION_NOT_ALLOWED
    }
}
const createPost = async (title, content,users_id,isDeleted=0) => {
    const res = await getPostBy('title',title);
    if(res){

        return {
            error: errors.EXISTING_POST
        }
    }
    const sql = `
    INSERT INTO posts (title,content, isDeleted, users_id)
    VALUES (?, ?, ?, ?)
    
    `;

    const result = await pool.query(sql,[title,content,isDeleted,users_id]);
    return{
        id:result.insertId,
        title:title, 
        content:content,
        users_id:users_id
    };
}

const createComment = async (content, users_id, posts_id)=>{
    const sql = `
    INSERT INTO comments (content, users_id, posts_id)
    VALUES (?,?,?)
    `;
    const result = await pool.query(sql, [content, users_id,posts_id]);
    return {
        id:result.insertId,
        content:content,
        users_id:users_id,
        posts_id:posts_id
    }
}

const postComments = async (id, user_id) => {
    const sql = `
    SELECT id, users_id, content,created_on from comments
    WHERE posts_id = ${id}
    `;
    
    const res = await pool.query(sql,[id]);
    return res.map(a=> {
        a.truth = a.users_id === user_id?true:false;
        return a;
    });
}

const findPost = async (column, value) => {
    const sql = `
    SELECT id,title
    FROM posts
    WHERE ${column} LIKE '%${value}%' AND isDeleted =0
    LIMIT 10;
    `
    const res = await pool.query(sql,[column,value])
    return res;
}

const getRecentPosts = async (offset, searcher) => {

    const sql = `
        SELECT id, title, content, created_on, users_id 
        FROM posts
        ORDER BY created_on DESC
        LIMIT 5
        OFFSET ${offset}
        `

    const res = await pool.query(sql,[offset]);
    return res.map(a=> {
        a.truth = a.users_id === searcher?true:false;
        return a;
    });
}

const getUserRecentPosts = async (offset, user, searcher) => {
    const sql = `
    SELECT id, title, content, created_on, users_id 
    FROM posts
    WHERE users_id=${user}
    ORDER BY created_on DESC
    LIMIT 5
    OFFSET ${offset}
    `

    const res = await pool.query(sql, [offset, user]);
    return res.map(a=> {
        a.truth = a.users_id === searcher?true:false;
        return a;
    });
}

const updatePost = async (id, title, content, user_id) => {
    const post = await getPostBy('title', title);
    if(post.users_id===user_id){
        const sql = `
        UPDATE posts
        SET title = '${title}',
        content = '${content}'
        WHERE id = ${id}
    `
        const res = await pool.query(sql, [title, content, id]);
        return res;
    } 
    return {
        error: errors.OPERATION_NOT_ALLOWED
    }


}

export default {
    getAllPosts,
    getPostBy,
    createPost,
    createComment,
    deletePost,
    postComments,
    findPost,
    getRecentPosts,
    getUserRecentPosts,
    updatePost
}