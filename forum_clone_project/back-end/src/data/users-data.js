import pool from "./pool.js";
const getAll = async () => {
    const sql = `
    SELECT * 
    FROM users
    `;
    return await pool.query(sql);
}
const getUserProfile = async(username) => {
    const sql = `
    SELECT id, username, password, is_admin
    FROM users
    WHERE username = '${username}'
    `
    const result = await pool.query(sql);
    return result[0];
}
const getBy = async (column, value) => {
    const sql = `
    SELECT id,username, email, first_name, last_name, createdOn
    FROM users
    WHERE ${column} = '${value}' AND isDeleted =0;
    `;

    const result = await pool.query(sql);
    return result[0];

}
const checkForExisting = async (column, value) => {
    const sql = `
    SELECT id,username
    FROM users
    WHERE ${column} = '${value}' AND isDeleted =0
    `;

    const result = await pool.query(sql,[column,value]);
    return result[0]
}
const getByUsername = async (column, value) => {
    const sql = `
    SELECT id,username
    FROM users
    WHERE ${column} LIKE '%${value}%' AND isDeleted =0
    LIMIT 10;
    `;

    const result = await pool.query(sql);
    return result;

}


const updateUser = async (user) => {
    const { id, first_name, last_name } = user;
    const sql = `
    UPDATE users 
    SET 
    first_name = ${first_name}
    last_name = ${last_name}
    WHERE id = ${id}
    `;

    return await pool.query(sql, [first_name, last_name, id]);
}

const createUser = async (username, password, email, first_name, last_name) => {

    const sql = `
    INSERT INTO users (username, password, email, first_name, last_name)
    VALUES (?,?,?,?,?)
    `;

    const result = await pool.query(sql,[username,password,email,first_name,last_name]);

    return {
        id:result.insertId,
        username: username,
        first_name: first_name,
        last_name: last_name
    };
}

const deleteUser = async(user) => {  // To be finished when delete posts/comments code is ready.
    const sql = `
        DELETE FROM users
        WHERE id = ?
    `;
    return await pool.query(sql, [user.id]);
};

const getUserPosts = async (value) => {
    const sql = `
    SELECT title,content, created_on
    FROM posts 
    WHERE users_id = ${value.id}
    `;
    return await pool.query(sql);
}

const createPost = async (title, content , users_id) => {
    const sql = `
    INSERT INTO posts (title,content,users_id)
    VALUES (?, ?, ?) 
    `;
    const result = await pool.query(sql,[title,content,users_id]);

    return {
        id:result.insertId,
        title:title, 
        content:content,
        users_id:users_id
    };
}

const makeAdmin = async(username) => {
    const sql = `
    UPDATE users 
        SET is_admin=2
        WHERE username = '${username}'
    `;
    const id = await checkForExisting('username',username)
    const result = await pool.query(sql,username)
    if(result){
        return {
            id: id.id,
            is_admin:2,
            username
        }
    } else {
        return false;
    }

} 
export default {
    getAll,
    getBy,
    updateUser,
    createUser,
    deleteUser,
    getUserPosts,
    createPost,
    getUserProfile,
    getByUsername,
    checkForExisting,
    makeAdmin,
}