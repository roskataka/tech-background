import express from 'express';
import userServices from '../services/user-service.js';
import authMiddleware from '../auth/auth.middleware.js';
import createToken from '../auth/create-token.js';
import errors from '../services/errors.js';
import validateBody from '../middlewares/validate-body.js';
import createUserValidator from '../validators/create-user-validator.js';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/errors.js'
export const usersRouter = express.Router();

usersRouter
     .get('',
           authMiddleware,
          async (req, res) => {
               const users = await userServices.getAllUsers();
               res.status(200).send(users);
          })
     .get('/:id',
           authMiddleware,
          async (req, res) => {

               const { id } = req.params;

               const user = await userServices.getUserById(id);

               res.status(200).send(user);
          })
     .get('/:id/posts',
          authMiddleware,
          async (req, res) => {
               const { id } = req.params;
               const userPosts = await userServices.getUserPosts(id);
               res.status(200).send(userPosts);
          }
     )
     .put('/auth',
          authMiddleware,
          async(req,res) => {
          res.status(200).send({"Success":"Yes"})
     }
     )
     .put('/:id',
          authMiddleware,
          async (req, res) => {

               const { id } = req.params;
               const updateData = req.body;

               const updatedUser = await userServices.updateUser(id, updateData);

               res.status(200).send(updatedUser);
          })
     .post('/register',
          async (req, res) => {
               const user = req.body;
               const newUser = await userServices.createUser(user);
               const id = newUser.id
               const username = user.username;
               if (newUser) {
                    const payload = {
                         id,
                         is_admin:1,
                         username
                    };
                    const token = createToken(payload);

                    res.status(200).send({
                         token: token,
                         user: newUser
                    })
               } else {
                    res.status(401).send(errors.NOT_FOUND);
               }
          }
     )
     .post('/signin',
          async (req, res) => {
               const { username, password } = req.body;
               const signAttempt = await userServices.signIn(username, password);
               const {id, is_admin} = signAttempt;

               if (signAttempt) {
                    const payload = {
                         id,
                         is_admin,
                         username
                    };
                    const token = createToken(payload);

                    res.status(200).send({
                         token: token
                    })
               } else {
                    res.status(402).send(errors.NOT_FOUND);
               }
          }
     )
     .post('/:id/posts',
          authMiddleware,
          async (req, res) => {
               const postData = req.body;
               const userId = req.params.id;
               const newPost = await userServices.createPost(postData, userId);
               res.status(201).send(newPost);
          }
     )
     .delete('/:id',        // To be finished when delete posts/comments code is ready.
          authMiddleware,
          async (req, res) => {
               const { id } = req.params;
               const deletedUser = await userServices.deleteUser(+id);
               res.status(200).send(deletedUser);
          }
     )
     .post('/search/:search',
          authMiddleware,
          async (req, res) => {
               const { search } = req.params;
               const foundUser = await userServices.findUser(search);
               res.status(200).send(foundUser);
          }
     )
     .get('/user/:username',  //for checking if there's already a user with that name
          async (req, res) => {
               const username = req.params;
               const existingUser = await userServices.checkForExisting(username.username)
               res.status(200).send(existingUser);
          }
     )
     .put('/user/:username',
          async (req, res) => {
               const {username} = req.params;
               const admin = await userServices.makeAdmin(username);
               res.status(200).send(admin);
          }
     )
     .get('/username/:username',
          async (req, res) =>{
               const { username } = req.params;
               const userInfo = await userServices.getUserInformation(username);
               console.log(userInfo);
               res.status(200).send(userInfo);
     }
     )
