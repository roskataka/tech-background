import express from 'express';
import postServices from '../services/posts-services.js';
import authMiddleware from '../auth/auth.middleware.js';

export const postsRouter = express.Router();

postsRouter
.get('',
    authMiddleware,
    async (req, res) =>{
        const posts = postServices.getAllPosts();
        res.status(200).send(posts);
    })
.get('/recent/:page',
    authMiddleware,
    async (req, res) =>{
        const page = req.params.page;
        const searcher = req.user.id;
        const posts = await postServices.getRecentPosts(+page, searcher);
        res.status(200).send(posts);
    }
)
.put('/:id',
    authMiddleware,
    async(req, res) => {
        const { id } = req.params;
        const {title, content}= req.body;
        const user_id = req.user.id
        const post = await postServices.updatePost(+id, title, content, user_id)
        console.log(id)
        res.status(200).send(post);
    }
)
.get('/:id',
    authMiddleware,
    async(req, res) => {
        const { id } = req.params;
        const post = await postServices.getPostBy('id',id);
        const comments = await postServices.getPostComments(id);
        post.truth = post.users_id === req.user.id?true:false;
        post.comments = comments;
        res.status(200).send(post);
    }
)
.get('/:id/comments',
    authMiddleware,
    async(req, res) => {
        console.log(req.user.username)
        const { id } = req.params;
        const user_id = req.user.id;
        const postComments = await postServices.getPostComments(id,user_id);
        res.status(200).send(postComments);
    }
)
.post('',
    authMiddleware,
    async(req, res) => {
        const post = req.body;
        const id = req.user.id
        const newPost = await postServices.createPost(post,id);
        res.status(201).send(newPost);
    }
)
.post('/:id',
    authMiddleware,
    async(req,res)=>{
        const userId = req.user.id
        const content = req.body.content
        const postId = req.params.id;
        const postComment = await postServices.createComment(content,userId,postId)
        res.status(201).send(postComment); 
    })
.post('/search/:search',
        authMiddleware,
        async (req, res) => {
            const { search } = req.params;
            const foundPosts = await postServices.findPost(search)
            res.status(201).send(foundPosts)
    }
)
.delete('/:id',
    authMiddleware,
    async(req,res)=>{
        const { id } = req.params;
        const user_id = req.user.id
        const deletePost = await postServices.deletePost(id, user_id);
        res.status(201).send(deletePost);
    }
)
.get('/:user_id/recent/:page',
    authMiddleware,
    async(req, res)=> {
        const user_id = req.params.user_id;
        const page = req.params.page
        const searcher = req.user.id
        const userPosts = await postServices.getUserRecentPosts((page*5), user_id, searcher)
        res.status(200).send(userPosts);
    }
)