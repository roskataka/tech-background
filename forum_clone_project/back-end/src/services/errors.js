export default {
  NOT_FOUND: 'User has not been found',
  POST_NOT_FOUND: 'A post with the following data does not exist',
  OPERATION_NOT_ALLOWED: 'This operation is not allowed!',
  DUPLICATE_RECORD: 'A user with this username already exists!',
  INCORRECT_PASSWORD: 'The password you have inputted is incorrect!',
  EXISTING_POST: 'A post with the same title already exists!',
  EMPTY_TEXT: 'You have not inputted any text',
};
