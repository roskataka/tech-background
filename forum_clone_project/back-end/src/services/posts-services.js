import posts from '../data/posts-data.js';
import errors from './errors.js';
const getAllPosts = async () => {
    return await posts.getAllPosts();
}

const getPostBy = async (column,value) => {
    return await posts.getPostBy(column, value);
}

const createPost = async (postData, id) => {
    const {title, content} = postData;
    return await posts.createPost(title,content,id);
}

const getRecentPosts = async (params, searcher) => {
    const page = params;
    return await posts.getRecentPosts(page*5, searcher);
}
const createComment = async (content,userId, postId) => {
    if(!content || !userId || !postId){
        return {
            error: errors.EMPTY_TEXT
        }
    }
    return await posts.createComment(content,userId, postId);
}

const deletePost = async(id, user_id) => {
    const exists = await getPostBy('id',id);
    if(!exists){
        return {
            error: errors.POST_NOT_FOUND
        }
    }
    return await posts.deletePost(id, user_id);
}

const getPostComments = async (id,user_id) => {
    return await posts.postComments(id,user_id);
}

const findPost = async (search) => {
    return await posts.findPost('title',search);
}

const getUserRecentPosts = async (offset, user, searcher) => {
    return await posts.getUserRecentPosts(offset, user, searcher);
}

const updatePost = async (id, title, content, user_id) => {
    return await posts.updatePost(id, title, content, user_id);
}
export default {
    getAllPosts,
    getPostBy,
    createPost,
    createComment,
    deletePost,
    getPostComments,
    findPost,
    getRecentPosts,
    getUserRecentPosts,
    updatePost
}