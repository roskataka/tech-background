
import users from "../data/users-data.js"
import serviceErrors from "../services/errors.js"
import bcrypt from 'bcrypt'
import errors from "../services/errors.js";

const getAllUsers = async () => {
    return await users.getAll();
};

const getUserById = async (value) => {
    const result = await users.getBy('id', value);
    if (result === null) {
        return {
            error: errors.NOT_FOUND
        }
    } 
return result
}

const updateUser = async (id, userData) => {
    let user = await users.getBy('id', id);

    const updated = { ...user, ...userData };
    const _ = await users.updateUser(updated);
    person = await users.getBy('id', id);
    return person;
}

const createUser = async (userData) => {
    const { username, password, repeat, email, first_name, last_name } = userData;
    const existingUser = await users.getBy('username', username);
    if (!existingUser) {
        if (password === repeat) {
            const passwordHash = await bcrypt.hash(password, 10);
            return await users.createUser(username, passwordHash, email, first_name, last_name);
        } else {
            return {
                error: serviceErrors.INCORRECT_PASSWORD,
                user: null
            }
        }
    } else {
        return {
            error: serviceErrors.DUPLICATE_RECORD,
            user: null
        }
    }

}

const deleteUser = async (id) => { // To be finished when delete posts/comments code is ready.
    const user = await users.getBy('id', id);
    const _ = await users.deleteUser(user);
    return user;
}

const getUserPosts = async (id) => {
    const user = await users.getBy('id', id);
    const userPosts = await users.getUserPosts(user);
    return await userPosts;
}

const createPost = async (postData, id) => {
    const { title, content } = postData;
    const user = await users.getBy('id', id);
    return await users.createPost(title, content, user.id);
}
const signIn = async (username, password) => {
    const existing = await users.getUserProfile(username);
    if (existing) {
        if(await bcrypt.compare(password, existing.password)){
            return {id: existing.id, is_admin: existing.is_admin, username: existing.username};
        }
    }
    return false;
}
const findUser = async (search) => {
    const result = await users.getByUsername('username',search);
    if(result){
        return result
    } else {
        return "Nothing found";
    }
}
const checkForExisting = async (username) => {
    const result = await users.checkForExisting('username',username);
    if(result){
        return true;
    } else {
        return false;
    }

}

const makeAdmin = async (username) => {
    if(checkForExisting(username)){
        return await users.makeAdmin(username);
    } else {
        return false;
    }
}

const getUserInformation = async (username) => {
    let result = await users.getBy('username', username);
    console.log(result);
    return result;
}
export default {
    getAllUsers,
    getUserById,
    updateUser,
    createUser,
    deleteUser,
    getUserPosts,
    createPost,
    signIn,
    findUser,
    checkForExisting,
    makeAdmin,
    getUserInformation
}