-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema end-to-end
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema end-to-end
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `end-to-end` DEFAULT CHARACTER SET latin1 ;
USE `end-to-end` ;

-- -----------------------------------------------------
-- Table `end-to-end`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `end-to-end`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(30) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `is_admin` TINYINT(1) NOT NULL DEFAULT 1,
  `isDeleted` TINYINT(1) NOT NULL DEFAULT 0,
  `createdOn` TIMESTAMP(1) NOT NULL DEFAULT CURRENT_TIMESTAMP(1),
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `end-to-end`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `end-to-end`.`posts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) NOT NULL,
  `content` TINYTEXT NOT NULL,
  `created_on` TIMESTAMP(1) NOT NULL DEFAULT CURRENT_TIMESTAMP(1),
  `isDeleted` TINYINT(1) NOT NULL DEFAULT 0,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_posts_users_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_posts_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `end-to-end`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `end-to-end`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `end-to-end`.`comments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `content` TINYTEXT NOT NULL,
  `created_on` TIMESTAMP(1) NOT NULL DEFAULT CURRENT_TIMESTAMP(1),
  `isDeleted` TINYINT(1) NOT NULL DEFAULT 0,
  `users_id` INT(11) NOT NULL,
  `posts_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_comments_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_comments_posts1_idx` (`posts_id` ASC) VISIBLE,
  CONSTRAINT `fk_comments_posts1`
    FOREIGN KEY (`posts_id`)
    REFERENCES `end-to-end`.`posts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `end-to-end`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `end-to-end`.`followers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `end-to-end`.`followers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user1id` INT(11) NOT NULL,
  `user2id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
