import errors from './errors.js';
import pool from './pool.js';
const getBy = async(column, value) => {
    const sql = `
    SELECT * from shipments 
    WHERE ${column} = '${value}'
    `;
    const res = (await pool.query(sql, [column, value]))[0];
    if(res) {
        return res;
    }
    return {error: errors[20]};
}
const createShipment = async (addressId, warehouseId) => {
    const sql = `
    INSERT INTO shipments (origin_idWarehouse, destination_idWarehouse)
    VALUES (?, ?)
    `;

    const res = await pool.query(sql, [addressId, warehouseId]);
    if(res){
        return {
            shipmentId: res.insertId,
            originAddress: addressId,
            destinationWarehouse: warehouseId,
        }
    }
    return {error: errors[19]}
}
const updateShipment = async (id, origin, destination) => {
    const sql = `
    UPDATE shipments SET
    origin_idWarehouse = ${origin},
    destination_idWarehouse = ${destination}
    WHERE idshipments = ${id}
    `;

    const res = await pool.query(sql, [id, origin, destination]);

    if(res){
        return await getBy("idshipments", id);
    }
    return {error: errors[3]}
}

const departShipment = async (id) => {
    const sql = `
    UPDATE shipments SET
    departure = NOW(),
    arrival = ADDTIME(NOW(), "3 0:00:0.000000"),
    state = 'on the way'
    WHERE idshipments = ${id}
    `;
    const res = await pool.query(sql, [id]);
    if(res){
        return await getBy("idshipments", id);
    }
    return {error: errors[3]}
}


const finishShipment = async (id) => {
    const sql = `    
    UPDATE shipments SET
    state = 'shipped'
    WHERE idshipments = ${id}
    `;
    const res = await pool.query(sql, [id]);
    if(res){
        return await getBy("idshipments", id);
    }
    return {error: errors[3]}
}

const deleteShipment = async (id) => {
    const sql = `
    DELETE FROM shipments WHERE idshipments = ${id}
    `;

    const found = await getBy("idshipments", id);
    if(!found.error){
        const res = await pool.query(sql, [id]);
        if(res){
            return found;
        }
        return {error: errors[23]}
    }
    return found;
}

export default {
    getBy,
    createShipment,
    updateShipment,
    departShipment,
    finishShipment,
    deleteShipment
}