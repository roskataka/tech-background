import errors from "./errors.js";
import pool from "./pool.js"
const getStates = async () => {
    const sql = `
    SELECT *
    FROM state
    `
    const res = await pool.query(sql);
    return res;
}

const createState = async (name, state_group) => {
    const sql = `
    INSERT INTO state (name, state_group)
    VALUES (?, ?)
    `
    const res = await pool.query(sql,[name, state_group]);
    return {
        id: res.insertId,
        name: name
    }
}

const getStateBy = async (column, value) => {
    const sql = `
    SELECT idState, state_group, name
    FROM state
    WHERE ${column} = '${value}';
    `
    const res = await pool.query(sql,[column,value]);
    return res[0]
}

const getCountries = async () => {
    const sql = `
    SELECT *
    FROM country
    `
    const res = await pool.query(sql);
    return res;
}

const createCountry = async (name) => {
    const sql = `
    INSERT INTO country (CountryName)
    VALUES (?)
    `
    const res = await pool.query(sql,[name]);
    return {
        id: res.insertId,
        name: name
    }
}
const getCountryBy = async (column, value) => {
    const sql = `
    SELECT idCountry, CountryName
    FROM country
    WHERE ${column} = '${value}';
    `
    const res = await pool.query(sql,[column,value]);
    return res[0]
}

// CITIES ---------------------------------------

const getCities = async (parentId) => {
    const sql = `
    SELECT *
    FROM cities
    WHERE country_idCountry=${parentId} OR state_idState=${parentId}
    `
    const res = await pool.query(sql,[parentId]);
    return res;
}

const createCity = async (name, parentId) => {
        const countryId = parentId>999?parentId:null;
        const stateId = parentId<1000?parentId:null;
        const sql = `
        INSERT INTO cities (cityName, country_idCountry, state_idState)
        VALUES (?, ?, ?)
        `
        const res = await pool.query(sql,[name,countryId,stateId]);
        return {
            id: res.insertId,
            name: name,
            isState: !!res.state_idState
        }
}
const getCityBy = async (column, value) => {
    const sql = `
    SELECT idCities, cityName, country_idCountry, state_idState
    FROM cities
    WHERE ${column} = '${value}'
    `
    const res = await pool.query(sql,[column,value]);
    const cityToReturn = res[0];
    if(!cityToReturn){
        return {error: errors[4]};
    }
    if(cityToReturn.state_idState){
        const state = await getStateBy("idState",cityToReturn.state_idState);
        return {
            parent: {
                id: state.idState,
                name: state.name,
                type: "state"
            },
            city: {
                id: cityToReturn.idCities,
                name: cityToReturn.cityName
            }
        }
    }
    if(cityToReturn.country_idCountry){
        const country = await getCountryBy("idCountry", cityToReturn.country_idCountry);
        return {
            parent: {
                id: country.idCountry,
                name: country.CountryName,
                type: "country"
            },
            city: {
                id: cityToReturn.idCities,
                name: cityToReturn.cityName
            }
        }
    }
}
// ADDRESSES----------------------------------------------------------------------
const getAddresses = async (cityId) => {
    const sql = `
    SELECT idAdress, address, cities_idCities
    FROM adress
    WHERE cities_idCities = '${cityId}'
    `
    const res = await pool.query(sql,[cityId]);
    if(!res[0]){
        return {error: errors[4]};
    } else {
        return res;
    }
}
const createAddress = async (address, cityId) => {
    const sql = `
    INSERT INTO adress (address, cities_idCities)
    VALUES (?, ?)
    `
    const res = await pool.query(sql,[address,cityId]);
    return {
        id: res.insertId,
        address: address,
        city: cityId
    }
}
const getAddressById = async (id) => {
    const sql = `
    SELECT idAdress, cities_idCities, address
    FROM adress
    WHERE idAdress = '${id}'
    `
    const res = (await pool.query(sql, [id]))[0];
    if(res){
        return {
            id: res.idAdress,
            city: res.cities_idCities,
            address: res.address
        }
    }
    return {error: errors[22]}
}
const checkAddress = async (address) => {
    const sql = `
    SELECT idAdress, cities_idCities, address
    FROM adress
    WHERE address LIKE '%${address}%'
    `
    const res = await pool.query(sql,[address]);
    if(res[0]){
        return res[0];
    } else {
        return false;
    }
}
const turnAddressToId = async (address) => {
    const sql = `
    SELECT idAdress, cities_idCities, address
    FROM adress
    WHERE address LIKE '%${address}%'
    `
    const res = await pool.query(sql,[address]);
    if(res[0]) {
        return res[0].idAdress
    } else {
        return {error: errors[4]}
    }
}
const deleteFromTable = async (table, column, value) => {
    const sql = `
    DELETE from ${table}
    where ${column} = '${value}'
    `

    const res = await pool.query(sql, [table, column, value])
    if(res){
        return `The ${table} with ${column} = ${value} has been deleted!`
    } else {
        return {error: errors[3]}
    }
}


export default{
    getStates,
    createState,
    getStateBy,
    getCountries,
    createCountry,
    getCountryBy,
    getCities,
    createCity,
    getCityBy,
    getAddresses,
    getAddressById,
    createAddress,
    checkAddress,
    turnAddressToId,
    deleteFromTable
}