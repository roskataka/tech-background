import errors from './errors.js';
import pool from './pool.js'
    const getCategoryBy = async (column, value) => {
        const sql = `
            SELECT idcategory, categoryName
            from category 
            where ${column} = '${value}'
            `

            const res = await pool.query(sql,[column, value]);
            return res[0]
        }
    
    const getAllCategories = async () => {
        const sql = `
        SELECT * FROM category
        `
        const res = await pool.query(sql);
        if(res){
            return res;
        }
        return {error: errors[3]};
    }
    const createCategory = async (name) => {
        const sql = `
        INSERT INTO category (categoryName)
        VALUES (?)
        `
        const res = await pool.query(sql,[name]);
        if(res){
            return {
                id: res.insertId,
                name: name
            }
        } 
        return {error: errors[16]}

    }

    const updateCategory = async (id, category) => {
        const sql = `
            update category SET categoryName = '${category}'
            where idcategory = '${id}'
        `
        const res = await pool.query(sql,[id.idcategory, category]);
        return res
    }

    const deleteCategory = async (id) => {
        const sql = `
        DELETE from category
        where idcategory = ${id}
        `
        const res = await pool.query(sql, [id])
        if(res){
            return res;
        } else {
            return {error: errors[3]}
        }
    }

    const getProductBy = async (column, value) => {
        const sql = `
        SELECT idProduct, Weight, category_idcategory
        FROM product
        WHERE ${column} = '${value}'
        `
        const res = (await pool.query(sql, [column, value]))[0];
        if(res){
            return {
                id: res.idProduct,
                weight: res.Weight,
                category: res.category_idcategory
            }
        } else {
            return {error: errors[18]}
        }

    }

    const deleteProduct = async (id) => {
        const sql = `
        DELETE from product
        where idproduct = ${id}
        `
        return await pool.query(sql, [id])
    }

    const createProduct = async (weight, id) => {
        const sql = `
        INSERT INTO product (Weight, category_idcategory)
        VALUES (?, ?)
        `
        const category = !!(await getCategoryBy("idcategory",id));
        if(category){
            const res = await pool.query(sql, [weight,id]);
            if(res){
                return {
                    productId: res.insertId,
                    weight: weight,
                    categoryId: id
                }
            }
            return {error: errors[3]}
        }
        return {error: errors[16]}

    }

    const updateProduct = async (product, weight, id) => {
        const sql = `
        update product SET 
        Weight = '${weight}',
        category_idcategory = '${id}'
        where idProduct = '${product}'
        `
        const res = pool.query(sql, [product, weight, id]);
        if(res){
            return {
                id: res.idproduct
            }
        }
    }

    export default {
        getCategoryBy,
        getAllCategories,
        createCategory,
        updateCategory,
        deleteCategory,
        getProductBy,
        createProduct,
        updateProduct,
        deleteProduct
    }