import mariadb from 'mariadb';
import dotenv from 'dotenv';
const dbConnection = true;
if(dbConnection){
  var pool = mariadb.createPool({
    host       : 'localhost', //dotenv.config().parsed.HOST,
    user       : 'root', //dotenv.config().parsed.MYSQL_ADDON_USER,
    password   : 'telerik', //dotenv.config().parsed.MYSQL_ADDON_PASSWORD,
    port       : '3306',
    database   : dotenv.config().parsed.MYSQL_ADDON_DB,
    max_user_connections: 1
  });
} else {
  var pool = mariadb.createPool({
    host       : dotenv.config().parsed.MYSQL_ADDON_HOST,
    user       : dotenv.config().parsed.MYSQL_ADDON_USER,
    password   : dotenv.config().parsed.MYSQL_ADDON_PASSWORD,
    port       : dotenv.config().parsed.MYSQL_ADDON_PORT,
    database   : dotenv.config().parsed.MYSQL_ADDON_DB,
    max_user_connections: 1,
    connectionLimit: 1
  });
}

export default pool;