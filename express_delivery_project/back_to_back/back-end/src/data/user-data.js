import errors from "./errors.js";
import pool from "./pool.js";
  const getUserBy = async (column, value) => {
        const sql = `
        SELECT idclients, username,email, firstname, lastname, adress_idAdress, idAdmin
        FROM clients
        WHERE ${column} = '${value}';
        `
        const res = (await pool.query(sql,[column,value]))[0];
        if(res){
            return res
        }
        return {error: errors[12]}
    }
    const createUser = async (username, email, password, firstname, lastname, addressId, idAdmin) => {
        const sql = `
            INSERT INTO clients (username,email, password, firstname, lastname, adress_idAdress, idAdmin )
            VALUES (?, ?, ?, ?, ?, ?, ?)
        `;
    
        const result = await pool.query(sql, [username,email, password, firstname, lastname, addressId, idAdmin]);
    
        return {
            id: result.insertId,
            username: username,
            idAdmin
        };
    };
    const getUserProfile = async (username) => {
        const sql = `
        SELECT idclients, username,password, idAdmin
        FROM clients
        WHERE username = '${username}';
        `
        const res = await pool.query(sql,[username]);
        return res[0]
    }
    const deleteUser = async(id) => {
        const sql = `
            DELETE FROM clients WHERE idclients = ${id}
        `
        const res = await pool.query(sql,[id]);
        if(res){
            return {
                id: id
            }
        } else {
            return {
                error: errors[12]
            }
        }
    }
    const getForUpdate = async (id) => {
        const sql = `
        SELECT * FROM clients WHERE idclients = ${id}
        `
        const res = await pool.query(sql, [id]);
        return res[0];
    }
    const updateUser = async(updateObj) => {
        const {idclients, username, password, email, firstname, lastname, adress_idAdress, idAdmin} = updateObj;
        console.log(updateObj)
        const sql = `
            UPDATE clients 
            SET username = "${username}",
            password = "${password}",
            email = "${email}",
            firstname = "${firstname}",
            lastname = "${lastname}",
            adress_idAdress = "${adress_idAdress}",
            idAdmin = "${idAdmin}"
            where idclients = ${idclients}
        `
        const res = await  pool.query(sql, [idclients, username, password, email, firstname, lastname, adress_idAdress, idAdmin]);
        return res;
    }
    const getCount = async() => {
        const sql = `
        SELECT COUNT(idclients)
        FROM clients
        WHERE idclients>0;
        `
        const res = (await pool.query(sql))[0];
        if(res){
            return res["COUNT(idclients)"];
        }
        return {error: errors[3]}
    }
export default {
    getUserBy,
    createUser,
    getUserProfile,
    deleteUser,
    getForUpdate,
    updateUser,
    getCount
}