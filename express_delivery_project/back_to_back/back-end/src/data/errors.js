const errors = {
    1: "A country/state with this name doesn't exist!",
    2: "Your delete function is used incorrectly!",
    3: "The query has lead to an error!",
    4: "Nothing matching the search queries has been found!",
    5: "Insufficient length!",
    6: "Incorrect Input!",
    7: "Address already exists in our database!",
    8: "A warehouse on this address already exists!",
    9: "A warehouse with this id doesn't exist!",
    10: "Passwords aren't matching!",
    11: "Invalid search category!",
    12: "Such user doesn't exist!",
    13: "Wrong username or password!",
    14: "A user with this username already exists!",
    15: "Product can't be created!",
    16: "Invalid product category!",
    17: "Invalid product!",
    18: "Product with this information could not be found!",
    19: "Could not issue a new shipment with the following warehouses!",
    20: "Such shipment doesn't exist!",
    21: "Invalid search query!",
    22: "Such address doesn't exist!",
    23: "Cannot delete shipment, there is data connected to it!"
}

export default errors;