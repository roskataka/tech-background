import errors from './errors.js';
import pool from './pool.js';

const getById = async(id) => {
    const sql = `
    SELECT idParcel, travelingTo, shipments_idshipments, shipments.origin_idWarehouse, shipments.destination_idWarehouse,
    parcel.adress_idAdress, parcel.warehouse_idWarehouse,
    clients_idclients, product_idProduct, Weight, 
    category_idcategory, categoryName, state, firstname, lastname, username FROM parcel 
    JOIN product ON product_idProduct = idProduct
    JOIN category ON category_idcategory = idcategory
    JOIN shipments on idshipments = shipments_idshipments
    JOIN clients on clients_idclients = idclients and
	idParcel = '${id}'
    `
    const res = (await pool.query(sql, [id]))[0];
    if(res){
        return res;
    }
    return {error: errors[3]}
}
const createParcel = async(shipmentId, addressId, clientId, productId, warehouseId, to='pick up from warehouse') => {
    const sql = `
    INSERT into parcel (shipments_idshipments, adress_idAdress, clients_idclients, product_idProduct, warehouse_idWarehouse, travelingTo)
    VALUES (?, ?, ?, ?, ?, ?)
    `
    const res = await pool.query(sql, [shipmentId, addressId, clientId, productId, warehouseId, to])
    if(res){
        return {
            parcelId: res.insertId,
            shipmentId,
            addressId,
            clientId,
            productId,
            warehouseId,
            to
        }
    } else {
        return {error: errors[3]}
    }
}
const updateParcel = async(id, shipmentId, addressId, clientId, productId, warehouseId, to='pick up from warehouse') => {
    const sql = `
    UPDATE parcel SET
    shipments_idshipments = ${shipmentId},
    adress_idAdress = ${addressId},
    clients_idclients = ${clientId},
    product_idProduct = ${productId},
    warehouse_idWarehouse = ${warehouseId},
    travelingTo = '${to}'
    WHERE idParcel = ${id}
    `
    const res = await pool.query(sql, [id, shipmentId, addressId, clientId, productId, warehouseId, to])
    if(res){
        return {
            id, 
            shipmentId, 
            addressId, 
            clientId, 
            productId, 
            warehouseId, 
            to
        }
    }
    return {error: errors[3]}
    
}
const deleteParcel = async(id) => {
    const sql = `
    DELETE FROM parcel WHERE idParcel = ${id}
    `
    const parcel = await getById(id);

    const res = await pool.query(sql, [id]);
    if(res){
        return parcel
    } else {
        return {error: errors[3]}
    }
}
const filterByWeight = async(criteria) => {
    const sql = `
    SELECT idParcel, travelingTo, shipments_idshipments, adress_idAdress, 
    clients_idclients, product_idProduct, warehouse_idWarehouse, Weight, 
    category_idcategory, categoryName FROM parcel 
    JOIN product ON product_idProduct = idProduct AND 
    Weight ${criteria}
    JOIN category ON category_idcategory = idcategory
    `

    const res = pool.query(sql, [criteria]);

    if(res){
        return res;
    } 
    return {error: errors[3]}
}
const filterByCriteria = async(column, value) => {
    const sql = `
    SELECT idParcel, travelingTo, shipments_idshipments, shipments.origin_idWarehouse, shipments.destination_idWarehouse,
    parcel.adress_idAdress, parcel.warehouse_idWarehouse,
    clients_idclients, product_idProduct, Weight, 
    category_idcategory, categoryName, state, firstname, lastname, username FROM parcel 
    JOIN product ON product_idProduct = idProduct
    JOIN category ON category_idcategory = idcategory
    JOIN shipments on idshipments = shipments_idshipments
    JOIN clients on clients_idclients = idclients and
	${column} = '${value}'
    `
    const res = await pool.query(sql, [column, value]);

    if(res){
        return res;
    }
    return {error: errors[3]}
}

const removeShipment = async(id) => {
    const sql = `
    UPDATE parcel SET
    shipments_idshipments = NULL
    WHERE idParcel = ${id}
    `
    const res = await pool.query(sql, [id]);
    if(res){
        return await getById(id);
    }
    return {error: errors[3]}
}

const addShipment = async(id, shipmentId) => {
    const sql = `
    UPDATE parcel SET
    shipments_idshipments = ${shipmentId}
    WHERE idParcel = ${id}
    `
    const res = await pool.query(sql, [id, shipmentId]);
    if(res){
        return await getById(id);
    }
    return {error: errors[3]}
}

export default {
    getById,
    createParcel,
    updateParcel,
    deleteParcel,
    filterByWeight,
    filterByCriteria,
    removeShipment,
    addShipment
}