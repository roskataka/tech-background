import addressServices from "../services/address-services.js";
import addressData from "./address-data.js";
import errors from "./errors.js";
import pool from "./pool.js"
const createWarehouse = async (id) => {
    const sql = `
        INSERT INTO warehouse (adress_idAdress)
        VALUES (?)
    `
    const res = (await pool.query(sql, [id]));
    return {
        addressId: id,
        warehouseId: res.insertId
    }
}

const checkForExistingWarehouse = async (id) => {
    const sql = `
        SELECT idWarehouse from warehouse
        WHERE adress_idAdress = ${id}
    `
    const res = (await pool.query(sql, [id]))[0];
    return res
}

const warehouseExists = async (id) => {
    const sql = `
        SELECT idWarehouse from warehouse
        WHERE idWarehouse = ${id}
    `
    const res = (await pool.query(sql, [id]));
    return res
}

const updateWarehouse = async (warehouseId, addressId) => {
        const sql = `
        UPDATE warehouse 
        SET adress_idAdress = ${addressId}
        WHERE idWarehouse = ${warehouseId}
    `
        const address = await addressData.getAddressById(addressId, true)
        const res = await pool.query(sql, [warehouseId, addressId]);
        if(res){
            return{
                warehouse: warehouseId,
                address: address
            }
        } else {
            return {error: errors[3]}
        }
}

const getWarehouse = async (column, value) => {
    const sql = `
        SELECT idWarehouse, adress_idAdress from warehouse
        WHERE ${column} = ${value}
    `
    const res = (await pool.query(sql, [column, value]))[0];


    if(res){
        const address = await addressServices.getAddressById(res.adress_idAdress, true)
        return{
            
            warehouse: res.idWarehouse,
            address: address
        }
    }
    return {error: errors[3]}
}

const getAllWarehouses = async (city) => {
    const id = await (await addressData.getCityBy("cityName", city)).city.id;
    const sql = `
    SELECT idWarehouse, idAdress, address, idCities, cityName FROM warehouse 
    JOIN adress ON adress_idAdress = idAdress
    JOIN cities ON cities_idCities = idCities WHERE idCities = ${id}
    `
    const res = await pool.query(sql, [id]);

    if(res){
        return res;
    }
    return {error: errors[3]}
}


const deleteWarehouse = async (id) => {
    const sql = `
    DELETE from warehouse where idWarehouse = ${id}
    `
    const res = await pool.query(sql, [id]);
    if(res){
        return {id};
    }
    return {error: errors[9]}
}
export default {
    createWarehouse,
    checkForExistingWarehouse,
    warehouseExists,
    updateWarehouse,
    getWarehouse,
    getAllWarehouses,
    deleteWarehouse
}