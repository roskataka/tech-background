import express from 'express';
import authMiddleware from '../auth/auth.middleware.js';
//import serviceErrors from '../services/errors.js'
export const parcelRouter = express.Router();
import parcel from '../services/parcel-services.js';
const visualizer = (result) => {
    if(!(result.error)){
        return result
    } else {
        return result.error;
    }
}
parcelRouter
    .get('/id/:id',
        authMiddleware,                                                                                                             //-------------------READY
        async (req, res) => {
            const result = await parcel.getById(req.params.id)
            if( req.user.idAdmin===1){
                res.status(200).send(visualizer(result));

            } else {
                res.status(200).send({error: "You're not authorized to see other people's parcels!"});
            }
        })
    .post('',
        //authMiddleware,                                                                                                                   //-------------------READY
        async (req, res) => {
            const {shipment, origin, client, product, destination, to} = req.body;
            const result = await parcel.createParcel(shipment, origin, req.user.id, product, destination, to);

            res.status(200).send(visualizer(result));
        })
    .put('/:id',                                                                                                                //-------------------READY
        async (req, res) => {
            const {shipment, origin, client, product, destination, to} = req.body;
            const result = await parcel.updateParcel(req.params.id, shipment, origin, client, product, destination, to);
            
            res.status(200).send(visualizer(result));
    })
    .delete('/:id',                                                                                                 //---------------------------- READY
        async (req, res) => {
            const result = await parcel.deleteParcel(req.params.id)
            res.status(200).send(visualizer(result));
    })
    .get('/weight/:criteria',                                                                                       //------------------------------READY
        async (req, res) => {
            const result = await parcel.filterByWeight(req.params.criteria)
            res.status(200).send(visualizer(result));
    })
    .get('/user/:criteria',                                                                                         // -------------------------------READY
        async (req, res) => {
            const result = await parcel.filterByCriteria("user", req.params.criteria)
            res.status(200).send(visualizer(result));
    })
    .get('/warehouse/:criteria',
    authMiddleware,                                                                                    // -------------------------------READY
        async (req, res) => {
            const result = await parcel.filterByCriteria("warehouse", req.params.criteria)
            res.status(200).send(visualizer(result));
    })
    .get('/category/:criteria',
    authMiddleware,                                                                                     //---------------------------------READY
        async (req, res) => {
            const result = await parcel.filterByCriteria("category", req.params.criteria)
            res.status(200).send(visualizer(result));
    })
    .get('/shipment/:criteria',
    authMiddleware,                                                                                     //---------------------------------READY
    async (req, res) => {
        const result = await parcel.filterByCriteria("shipment", req.params.criteria)
        res.status(200).send(visualizer(result));
    })
    .delete('/remove-shipment/:id',                                                                                 //---------------------------------READY
        async (req, res) => {
            const foundParcel = await parcel.getById(req.params.id);
            if(foundParcel){
                const result = await parcel.removeShipment(req.params.id)
                res.status(200).send(visualizer(result))
            }
        })
    .put('/add-shipment/:id',                                                                                       //--------------------------------READY
    async (req, res) => {
        const foundParcel = await parcel.getById(req.params.id);
        if(foundParcel){
            const result = await parcel.addShipment(req.params.id, req.body.shipmentId);
            res.status(200).send(visualizer(result));
        }
    })
    
    