import express from 'express';
//import serviceErrors from '../services/errors.js'
export const warehouseRouter = express.Router();
import address from '../services/address-services.js';
import warehouse from '../services/warehouse-services.js'
warehouseRouter
    .get('/warehouse/:category/:value',                                                                               //GET WAREHOUSE ---------------READY
        async (req, res) => {
            const category = req.params.category;
            const value = req.params.value
            const result = await warehouse.getWarehouse(category, value)
            res.status(200).send(result)
        })
        .get('/warehouses/:city',                                                                               //GET WAREHOUSEs ---------------READY
        async (req, res) => {
            const city = req.params.city
            const result = await warehouse.getAllWarehouses(city);
            res.status(200).send(result)
        })
    .post('/:id',                                                                                           //UPDATE WAREHOUSE ------------READY
        async (req, res) => {
            const id = req.params.id;
            const checkedAddress = await address.getAddressById(id, true)
            if(checkedAddress){
                const result = await warehouse.createWarehouse(id)
                res.status(200).send(result)
            } else {
                res.status(400).send("Such address doesn't exist in our database!")
            }
    })
    .put('/:id',                                                                                            // UPDATE WAREHOUSE -------- READY
        async (req, res) => {
            const id = req.params.id;
            const warehouseId = req.body.warehouse
            const checkedAddress = await address.getAddressById(id, true)
            if(checkedAddress){
                const updatedWarehouse = await warehouse.updateWarehouse(warehouseId, id);
                res.status(200).send(updatedWarehouse)
            } else {
                res.status(400).send("Such address doesn't exist in our database!")
            }

    })
    .delete('/warehouse/:id',                                                                               //DELETE WAREHOUSE ------------READY
    async (req, res) => {
        const id = req.params.id;
        const checkedAddress = await address.getAddressById(id, true);
        if(checkedAddress){
            const deletedId = await warehouse.deleteWarehouse(id);
            res.status(200).send(deletedId)
        } else {
            res.status(400).send("Such address doesn't exist in our database!")
        }
    })
