import express from 'express';
//import serviceErrors from '../services/errors.js']
import product from '../services/product-services.js';
export const productRouter = express.Router();

productRouter
    .get('/category/:column/:value/',                                                                               //GET PRODUCT CATEGORY BY ID ------------------ READY
    async (req, res) => {
        let {column, value} = req.params
        if(column === "id"){
            column = "idCategory";
        } else {
            column = "categoryName"
        }
        const result = await product.getCategoryBy(column ,value);
        if(!(result.error)){
            res.status(201).send(result);
        } else {
            res.status(400).send(result.error);
        }
    })
    .get('/categories',
    async (req, res)=>{
        const result = await product.getAllCategories();
        if(!result.error){
            res.status(201).send(result);   
        } else {
            res.status(400).send(result.error)
        }
    })
    .post('/category',                                                                                           //CREATE NEW PRODUCT CATEGORY ------------------ READY
    async (req, res)=>{
        const category = req.body.name;
        const result = await product.createCategory(category);
        res.status(201).send(result);   
    })
    .put('/category/:column/:value/',                                                                                            // UPDATE PRODUCT CATEGORY BY EITHER CREDENTIAL ------------------- READY
        async (req, res) => {
            let {column, value} = req.params
            if(column === "id"){
                column = "idCategory";
            } else {
                column = "categoryName"
            }
            const result = await product.updateCategory(column ,value, req.body.categoryName);
            if(!(result.error)){
                res.status(201).send(result);
            } else {
                res.status(400).send(result.error);
            }
    })
    .delete('/category/:id',                                                                               //DELETE CATEGORY BY ID------------------------------------ READY
    async (req, res) => {
        const id = req.params.id
        const result = await product.deleteCategory(id);
        if(!(result.error)){
            res.status(201).send(result);
        } else {
            res.status(400).send(result.error);
        }
    })
    .get('/product/:id/',                                                                               //GET PRODUCT  BY ID --------------------------- READY
    async (req, res) => {
        const id = req.params.id
        const result = await product.getProductById(id);
        res.status(201).send(result)
    })
    .get('/product-full/:id/',                                                                               //GET PRODUCT  BY ID --------------------------- READY
    async (req, res) => {
        const id = req.params.id
        const result = await product.getFullProductById(id);
        if(!result.error){
            res.status(201).send(result);
        } else {
            res.status(201).send(result.error);
        }
    })
    .post('/product/',                                                                                           //CREATE NEW PRODUCT ------------------------------ READY
        async (req, res) => {
            const {category, weight} = req.body;
            const result = await product.createProduct(category, weight)
            res.status(200).send(result)
    })
    .put('/product/:id',                                                                                            // UPDATE PRODUCT 
        async (req, res) => {
            res.status(200).send("The query isn't ready yet!")
    })
    .delete('/product/:id',                                                                               //DELETE PRODUCT BY ID ------------------------------------- READY
    async (req, res) => {
        const result = await product.deleteProduct(id);
        res.status(200).send(result);
    })

//IF IT'S CATEGORY IT ONLY TAKES AS INPUT A STRING FOR THE CATEGORY'S NAME

//IF IT'S A PRODUCT IT TAKES EXISTING CATEGORY AND CREATES A PRODUCT WITH THE FOLLOWING CATEGORY'S NAME, AND ALSO TAKES WEIGHT OF THE PRODUCT