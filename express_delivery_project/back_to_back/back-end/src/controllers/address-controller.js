import express, { json } from 'express';
//import serviceErrors from '../services/errors.js'
export const addressRouter = express.Router();
import address from '../services/address-services.js';
const visualizer = (res) => {
    if(!res.error){
        return res;
    } else {
        return `"${res.error}"`
    }
}
addressRouter
    .get('/states',                                                     //READY-----------------------------
    async (req, res) => {
        const result = await address.getStates();
        res.status(201).send(result)
    })
    .post('/states',                                                    //READY-----------------------------
    async (req, res)=>{
        let names = req.body.states;
        if(typeof names === "string"){
            names = names.split(',')
        }
        const obj = {"states":names, "group":req.body.group};
        const result = await address.createStates(obj);
        res.status(201).send(result);   
    })
    .get('/state/:category/:value',                                     //READY------------------------------
        async (req, res) => {
            const category = req.params.category;
            const value = req.params.value
            const foundState = await address.getStateBy(category, value);
            res.status(200).send(foundState)
        }
    )
    .post('/state',                                                     //READY------------------------------
    async (req, res) => {
        const createdState = await address.createState(req.body.name, req.body.state_group);
        res.status(200).send(createdState)
    }
    )
    .get('/countries',                                                  //READY------------------------------
    async (req, res) => {
        const result = await address.getCountries();
        res.status(200).send(result)
    }
    )
    .post('/countries',                                                 //READY---------------------------------
    async (req, res)=>{
        let names = req.body.name;
        if(typeof names ==='string'){
            names = names.split(',')
        }
        const result = await address.createCountries(names)
        res.status(201).send(result);
    })
    .get('/country/:category/:value',                                   //READY---------------------------------
    async (req, res) => {
        const category = req.params.category;
        const value = req.params.value;
        const result = await address.getCountryBy(category, value);
        res.status(200).send(result);
    })
    .post('/country',                                                   //READY----------------------------------
        async (req, res) => {
            const result = await address.createCountry(req.body.name);
            res.status(200).send(result)
        }
    )
    .get('/cities/:parent',                                             //READY------------------------------------
        async (req, res) => {
            const result = await address.getCities(req.params.parent);
            res.status(200).send(result)
    })
    .post('/cities/:parent',                                                    //READY----------------------------
    async (req, res) => {
        if(typeof req.body.cities === 'string'){
            const cities = req.body.cities.split(',');
            const result = await address.createCities(cities, req.params.parent);
            res.status(200).send(result);
        } else {
            const result = await address.createCities(req.body.cities, req.body.parent);
            res.status(200).send(result)
        }

    })
    .get('/city/:category/:value',                                     //READY-----------------------------------------
    async (req, res) => {
        const category = req.params.category;
        const value = req.params.value
        const foundCity = await address.getCityBy(category, value);
        res.status(200).send(foundCity)
    })
    .post('/city',                                                      //READY---------------------------------------------
    async (req, res) => {
        const result = await address.createCity(req.body.name, req.body.parent);
        res.status(200).send(result)
    })
    .get('/addresses/:city',                                            //READY-----------------------------------------------
    async (req, res) => {
            const result = await address.getAddresses(req.params.city);
            res.status(200).send(result);
    })
    .get('/address-full/:id',                                     //READY-----------------------------------------
    async (req, res) => {
        const id = req.params.id;
        const foundAddress = await address.getAddressById(id, true);
        res.status(200).send(visualizer(foundAddress))
    })
    .get('/address/:id',                                     //READY-----------------------------------------
    async (req, res) => {
        const id = req.params.id;
        const foundAddress = await address.getAddressById(id);
        res.status(200).send(visualizer(foundAddress))
    })
    .post('/addresses/:city',                                           //READY-----------------------------------------------
    async (req, res) => {
        let addresses = req.body.addresses;
        if(typeof addresses==="string"){
            addresses = addresses.split(',');
        }
        const result = await address.createAddresses(addresses, req.params.city)
        res.status(200).send(result);
    })
    .post('/address/:city',                                             //READY----------------------------------------
    async (req, res) => {
        const addressData = req.body.address;
        const city = req.params.city;
        const result = await address.createAddress(addressData, city)
        res.status(200).send(result);
    })
    .delete('/:table/:category/:value',                                //READY------------------------------------
    async (req, res) => {
        const table = req.params.table;
        const category = req.params.category;
        const value = req.params.value;
        const result = await address.deleteFromTable(table, category, value);
        res.status(200).send(result);
    })