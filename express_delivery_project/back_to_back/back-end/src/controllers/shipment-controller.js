import express from 'express';
//import serviceErrors from '../services/errors.js'
export const shipmentRouter = express.Router();
import shipment from '../services/shipment-services.js'
import parcel from '../services/parcel-services.js';

const visualizer = (result) => {
    if(!(result.error)){
        return result
    } else {
        return result.error;
    }
}
shipmentRouter
    .get('/id/:id/',                                                                                        //----------------READY
        async (req, res) => {
            const result = await shipment.getBy("id", req.params.id);
            res.status(200).send(visualizer(result))
        })
    .get('/warehouse/:warehouse',                                                                           //-----------------READY
        async (req, res) => {
            const result = await shipment.getBy("warehouse", req.params.warehouse);
            res.status(200).send(visualizer(result))
        })
    .post('/',                                                                                              //-----------------READY
        async (req, res) => {
            const {origin, destination} = req.body
            const result = await shipment.createShipment(origin, destination);
            res.status(200).send(visualizer(result))
    })
    .put('/:id',                                                                                            //-----------------READY    
    async (req, res) => {
        const id = req.params.id
        const {origin, destination} = req.body
        const result = await shipment.updateShipment(id, origin, destination);
        res.status(200).send(visualizer(result))
    })
    .delete('/:id',                                                                                         //------------------READY
    async (req, res) => {
        const search = await parcel.filterByCriteria("shipment", req.params.id);
        if(!!search){
            const result = await shipment.deleteShipment(req.params.id);
            res.status(200).send(visualizer(result))
        }
    })
    .post('/depart/:id',                                                                                   //------------------READY
    async (req, res) => {
        const search = await parcel.filterByCriteria("shipment", req.params.id);
        if(!!search){
            const result = await shipment.departShipment(req.params.id);
            res.status(200).send(visualizer(result))
        }
    })
    .post('/shipped/:id',                                                                                   //---------------------READY
    async (req, res) => {
        const result = await shipment.finishShipment(req.params.id);
        res.status(200).send(visualizer(result));
    })

