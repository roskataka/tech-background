import express from 'express';
import { createUserValidator } from '../validator/createUserValidator.js';
import { createUserSchema } from '../schema/createUser.js';
import users from '../services/user-services.js';
import createToken from '../auth/create-token.js';
import errors from '../data/errors.js';
import authMiddleware from '../auth/auth.middleware.js'
import { userAdminValidator } from '../validator/checkAdmin.js';
import parcels from '../services/parcel-services.js';
//import serviceErrors from '../services/errors.js'
export const userRouter = express.Router();
const visualizer = (res) => {
    if(!res.error){
        return res;
    } else {
        return `"${res.error}"`
    }
}
userRouter

    .get('/id/:id',
    authMiddleware,                                                                        //get user by Id ----------------------------------- READY                                               
    async (req, res) => {
        const { id } = req.params;

        const foundUser = await users.getUserBy("id", id);
        res.status(200).send(visualizer(foundUser))
    })

    .get('/username/:username',
    authMiddleware,
    userAdminValidator,                                                                //get user by username    -------------------------------------READY                                    
    async (req, res) => {
        const { username } = req.params;
        if(req.user.idAdmin===0){
            res.status(200).send({error:"unauthicated"})
        } else{
            const foundUser = await users.getUserBy("username", username);
            res.status(200).send(visualizer(foundUser))
        }


    })
    .get('/username-bool/:username',                                                                //get user by username    -------------------------------------READY                                    
    async (req, res) => {
        const { username } = req.params;
        const foundUser = await users.getUserBy("username", username);
        if(!foundUser.error){
            res.status(200).send(true);
        } else {
            res.status(200).send(false)
        }
    })

    .post('/admin',                                                                   // create user ------------------- NEEDS VALIDATION MIDDLEWARE
        async (req, res) => {
            const {username,email, password, repeat, firstname, lastname, address} = req.body

            const result = await users.createUser(req.body, 1);
            if(!result.error){
                const id = result.id
                const payload = {
                    id,
                    username,
                    idAdmin:1
                }
                const createdToken = createToken(payload)
                res.status(200).send({
                    token: createdToken,
                    user: result
                })
            } else {
                res.status(401).send(result.error)
            }

    })
    .post('/',                                                                          // create user ------------------------ 
        //createUserValidator(createUserSchema),
        async (req, res) => {
            const {username} = req.body
            const result = await users.createUser(req.body,0);
            if(!result.error){
                const id = result.id
                const payload = {
                    id,
                    username,
                    idAdmin:0
                }
                const createdToken = createToken(payload)
                res.status(200).send({
                    token: createdToken,
                    user: result
                })
            } else {
                res.status(401).send(result.error)
            }
    })
    .post('/register',                                                                  //create user - you can use either normal post or register ----------------------NEEDS VALIDATION MIDDLEWARE
        async (req, res) => {
            const {username,email, password, repeat, firstname, lastname, address} = req.body
            const result = await users.createUser(req.body,0);
            if(!result.error){
                const id = result.id
                const payload = {
                    id,
                    username,
                    idAdmin:0
                }
                const createdToken = createToken(payload)

                res.status(200).send({
                    token: createdToken,
                    user: result
                })
            } else {
                res.status(401).send(result.error)
            }
        })
    .put('/:id',                                                                        //update user
        authMiddleware,                                                            
        async (req, res) => {
            const {id} = req.params
            if(req.user.id == id || req.user.idAdmin === 1) {
                console.log(req.user.idAdmin)
                const updateObj = req.body;
                const result = await users.updateUser(id, updateObj)

                    res.status(200).send(visualizer(result));
            } else {
                res.status(400).send("You're not authorized to change this user!")
            }

        }
    )
    .post('/login',                                                         //login with user credentials --------------------------------- READY
        async (req, res) => {
            const {username, password} = req.body
            const profile = await users.logIn(username, password);
            if (profile && !(profile.error)){
                const id = profile.id

                const payload = {
                    id,
                    username:profile.username,
                    idAdmin:profile.idAdmin
                }

                const createdToken = createToken(payload);

                res.status(200).send({
                    token: createdToken,
                    user: profile
                })
            } else {
                res.status(400).send({error: errors[13]})
            }
        }
    )
    .delete('/:id',
    authMiddleware,                                                            //delete user ----------------------------------------READY
    async (req, res) => {
        const {id} = req.params
        if(req.user.id == id || req.user.idAdmin === 1) {
            const result = await users.deleteUser(id);
            res.status(200).send(visualizer(result))
        } else {
            res.status(400).send("You're not authorized to delete this user!")
        }

    })
    
    .get('/firstname/:firstname',
    authMiddleware,
    userAdminValidator,                                           //get user by firstname ------------------------------------- READY
    async (req, res) => {
        const { firstname } = req.params;
        const foundUser = await users.getUserBy("firstname", firstname);
        res.status(200).send(visualizer(foundUser))
    })

    .get('/lastname/:lastname',
    authMiddleware,
    userAdminValidator,                                                  //get user by lastname     ----------------------------------READY                        
    async (req, res) => {
        const { lastname } = req.params;
        const foundUser = await users.getUserBy("lastname", lastname);
        res.status(200).send(visualizer(foundUser))

    })
    .get('/email/:email',
    authMiddleware,
    userAdminValidator,                                                       // GET USER BY EMAIL --------------------------------------- READY
    async (req, res) => {
        const { email } = req.params;
        const foundUser = await users.getUserBy("email", email);               
        res.status(200).send(visualizer(foundUser))
    })
    .get('/count',
    async (req, res) => {
        const result = await users.getCount();
        if(!result.error){
            res.status(200).send({count: result});
        } else {
            res.status(401).send({error: errors[3]})
        }
    })
    .get('/',
    authMiddleware,
    userAdminValidator,
    async (req, res) => {
        res.status(200).send('"hello"')
    })
    .get('/parcels/:user',
    authMiddleware,
    async(req, res) => {
        let id = null;
        if(isNaN(parseInt(req.params.user))){
            id = users.getUserBy("username", req.params.user);
        } else {
            id = req.params.user;
        }
        if(req.user.id == id || req.user.idAdmin === 1) {
            const result = await parcels.filterByCriteria("user", id);
            res.status(200).send(visualizer(result))
        } else {
            res.status(400).send("You're not authorized to delete this user!")
        }
    })
    .get('/checklogged',
    authMiddleware,
    async(req, res) => {
        if(req.user){
            res.status(200).send(true);
        } else {
            res.status(200).send(false);
        }
    }
    )
