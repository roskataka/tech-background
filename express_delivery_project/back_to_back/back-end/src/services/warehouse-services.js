import errors from '../data/errors.js';
import warehouse from '../data/warehouse-data.js';

const createWarehouse = async (id) => {
    const exists = await warehouse.checkForExistingWarehouse(id);
    if(exists){
        return {error: errors[8]}
    } else {
        const res = await warehouse.createWarehouse(id);
        return res;
    }
}
const updateWarehouse = async (warehouseId, addressId) => {
    const exists = await warehouse.warehouseExists(warehouseId);
    if(exists){
        const res = await warehouse.updateWarehouse(warehouseId, addressId);
        return res;
    }
    return {error: errors[9]}
}
const getWarehouse = async (column, value) => {
    if(column === "warehouse"){
        return await warehouse.getWarehouse("idWarehouse", value)
    } else if(column === "address"){
        return await warehouse.getWarehouse("adress_idAdress", value)
    }
}
const getAllWarehouses = async (city) => {
    return await warehouse.getAllWarehouses(city);
}

const deleteWarehouse = async (id) => {
    const exists = await warehouse.warehouseExists(id);
    if(exists){
        const res = await warehouse.deleteWarehouse(id)
        return res;
    }
    return {error: errors[9]}
}
export default {
    createWarehouse,
    updateWarehouse,
    getWarehouse,
    getAllWarehouses,
    deleteWarehouse
}