import errors from '../data/errors.js'
import product from '../data/product-data.js'
const getCategoryBy = async (column, value) => {
    const res = await product.getCategoryBy(column, value);
        if(res){
            return res;
        } else {
            return {error: errors[3]}
        }
}

const getAllCategories = async () => {
    const res = await product.getAllCategories();
    if(res){
        return res;
    } else {
        return {error: errors[3]}
    }
}

const createCategory = async (name) => {
    const res = await product.createCategory(name);
    return res;
}

const updateCategory = async (column, value, newName) => {
    const res = await product.getCategoryBy(column, value);
        if(res){
            return product.updateCategory( res.id , newName);
        } else {
            return {error: errors[3]}
        }
}

const deleteCategory = async (id) => {
    const category = await product.getCategoryBy("idcategory", id);
        if(category){
            return product.deleteCategory(category.idcategory);
        } else {
            return {error: errors[3]}
        }
}

const getProductById = async (id) => {
    const res = await product.getProductBy("idProduct", id);
    return res;
}

const getFullProductById = async (id) => {
    const res = await product.getProductBy("idProduct", id);
    if(!res.error){
        const category = await product.getCategoryBy("idcategory", res.category);
        if(category){
            res.category = category
            return res;
        }
        return {error: errors[17]}
    } 
    return { error: errors[18]}
}
const handleCategory = async (category) => {

    if(isNaN(parseInt(category))){
        return await product.getCategoryBy('categoryName', category);
    } 
    return category;
}
const createProduct = async (category, weight) => {
    const categoryId = await handleCategory(category);
    console.log(categoryId)
    if(weight<1){
        return {
            error: errors[15]
        }
    } else {
        if(categoryId) {
            const res = product.createProduct(weight, categoryId);
            if(res){
                return res;
            }
            return {
                error: errors[3]
            }
        }
    }
}

const updateProduct = async (obj) => {
    const res = updateCategory;
    return res;
}

const deleteProduct = async (id) => {
    const res = await product.deleteProduct(id);
    return res;
}

export default {
    getCategoryBy,
    getAllCategories,
    createCategory,
    updateCategory,
    deleteCategory,
    getProductById,
    getFullProductById,
    createProduct,
    updateProduct,
    deleteProduct
}