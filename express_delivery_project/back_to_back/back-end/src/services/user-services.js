import errors from '../data/errors.js';
import users from '../data/user-data.js';
import * as bcrypt from 'bcrypt';
import addresses from '../services/address-services.js'
const getUserBy = async (column, value) => {
    switch(column){
        case "id":
            return await users.getUserBy("idclients", value);
        case "username":
            return await users.getUserBy("username",value);       
        case "firstname":
            return await users.getUserBy("firstname", value);
        case "lastname":
            return await users.getUserBy("lastname", value);
        case "email":
            return await users.getUserBy("email", value);
        default:
            return {error: errors[11]};
    }
}

const createUser = async(userData, idAdmin) => {
    const {username,email, password, repeat, firstname, lastname,city, address} = userData;
    const hashedPassword = await bcrypt.hash(password, 10);
    let addressId = null;
    if(isNaN(parseInt(address))){
        addressId = await addresses.turnAddressToId(address);
        if(addressId.error){
            addressId = (await addresses.createAddress(address, city)).id;
        }
    } else {
        addressId = address;
    }

    if(password === repeat){
        const res = await users.createUser(username, email,hashedPassword, firstname, lastname, addressId, idAdmin)
        return res;
    }
    return {
        error: errors[10]
    }
}

const logIn = async (username, password) => {
    const foundUser = await users.getUserProfile(username);
    if(foundUser){
        if(await bcrypt.compare(password, foundUser.password)){
            return {
                id: foundUser.idclients,
                username: foundUser.username,
                idAdmin: foundUser.idAdmin
            }
        }
    }
    return {
        error: errors[13]
    }
}

const updateUser = async (id, updateObj) => {
    const user = await users.getForUpdate(id);
    if(!user){
        return {error: errors[12]}
    }
    if(updateObj.username){
        const check = await getUserBy("username", updateObj.username)
        if(check.username && user.username!==check.username){
            return {error: errors[14]}
        }
    }
    if(updateObj.password){
        updateObj.password = await bcrypt.hash(updateObj.password, 10);
        console.log("Password changed successfully!");
    }
    const updatedUser = {
        ...user,
        ...updateObj
    }

    return await users.updateUser(updatedUser);
}

const deleteUser = async (id) => {
    const user = await getUserBy("id", id);
    if(user){
        const deletedUser = await users.deleteUser(id);
        return deletedUser;
    } else {
        return {error: errors[12]}
    }
}

const getCount = async () => {
    return await users.getCount();
}
export default {
    getUserBy,
    createUser,
    logIn,
    updateUser,
    deleteUser,
    getCount
}