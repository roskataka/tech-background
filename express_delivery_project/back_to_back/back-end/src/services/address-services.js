import address from '../data/address-data.js'
import errors from '../data/errors.js';
const getStates = async () => {
    const res = await address.getStates();
    return res;
}

const getStateBy = async (column, value) => {
    if(column==="id"){
        const res = await address.getStateBy("idState", value);
        return res;
    } else if(column==="name"){
        const res = await address.getStateBy("name", value);
        return res;
    }
}

const createState = async (name, group) => {
    const res = await address.createState(name, group);
    return res;
}

const createStates = async (obj) => {
    const res = await obj.states.map(a=> address.createState(a, obj.group));
    return !!res;
}

// COUNTRIES -----------------------------------------------------------------------------------
const getCountries = async ()=> {
    const res = await address.getCountries();
    return res;
}

const getCountryBy = async (column, value) => {
    if(column==="id"){
        const res = await address.getCountryBy("idCountry", value);
        return res;
    } else if(column==="name"){
        const res = await address.getCountryBy("CountryName", value);
        return res;
    }

}

const createCountry = async (name) => {
    const res = await address.createCountry(name);
    return res;
}

const createCountries = async (obj) => {
    const res = await obj.map(a => address.createCountry(a));
    return !!res;
}

// CITIES ---------------------------------------------------------------------------------
const checkParent = async (parent) => {
    if(typeof parent==="string"){    
        const country = await address.getCountryBy("CountryName",parent);
        const state = await address.getStateBy("name", parent);
        const res =  country?country.idCountry:state.idState;
        return res || errors[1];
        } 
        else{
            return parent;
        }
    }
const getCities = async (parent) => {
    if(isNaN(parseInt(parent))){
        const id = await checkParent(parent);
        const res = await address.getCities(id);
        return res;
    } else {
        const res = await address.getCities(parent);
        return res;
    }

}
const createCity = async (name, parent) => {
    if(isNaN(parseInt(parent))){
        const id = await checkParent(parent);
        const res = await address.createCity(name, id);
        return res;
    } else {
        const res = await address.createCity(name, parent);
        return res;
    }
}

const createCities = async (obj, parent) => {
    if(isNaN(parseInt(parent))){
        const id = await checkParent(parent);
        const res = await obj.map(a=> createCity(a, id))
        return !!res;
    } else {
        const res = await obj.map(a=> address.createCity(a, parent));
        return !!res;
    }
}

const getCityBy = async (column,value)=>{

    if(column==="id"){
        return await address.getCityBy("idCities",value);
    }
    if(column==="name"){
        return await address.getCityBy("cityName",value);
    } else {
        return errors[4]
    }
}
//ADDRESSES ---------------------------------------------------------------------------------------------------
const handleCity = async (city) => {
    if(isNaN(parseInt(city))){
        const foundCity = await getCityBy("name",city);
        return foundCity.city.id
    } else {
        return city
    }
}
const getAddresses = async (city) => {
    const cityId = await handleCity(city);
    return await address.getAddresses(cityId);
}
const getAddressById = async (id, full=false) => {
    
    if(full){
        const res = await address.getAddressById(id);
        const fullAddress = await getCityBy("id", res.city);
        if(!res){
            return errors[4]
        }
        return {
            ...fullAddress,
            address:res
        }
    } else {
        const res = await address.getAddressById(id);
        if(!res) {
            return errors[4];
        }
        return res;
    }
}
const createAddress = async (addressData, city) => {
    if(addressData.length<10){
        return {error: errors[5]};
    }
    const cityId = await handleCity(city);
    const checker = await checkAddress(addressData);
    if(!checker){
        const res = await address.createAddress(addressData, cityId);
        return res;
    }
    const addressId = await turnAddressToId(addressData);
    return {
        id: addressId,
        address: addressData,
        city: cityId
    };
}
const createAddresses = async (obj, city) => {
    if(typeof obj === "string"){
        return {error: errors[6]};
    }
    const cityId = await handleCity(city);
    const res = await obj.map(a => createAddress(a, cityId));
    return !!res;
}
const checkAddress = async (addressData) => {
    return await address.checkAddress(addressData);
}
const turnAddressToId = async (addressData) => {
    return await address.turnAddressToId(addressData);
}
const deleteFromTable = async (table, column, value) => {
    switch(table){
        case "cities":
            if(column==="name"){
                return await address.deleteFromTable("cities","cityName",value);
            }
            if(column==="id"){
                return await address.deleteFromTable("cities","idCities",value);
            }
            if(column==="country"){
                const id = await checkParent(value)
                return await address.deleteFromTable("cities","country_idCountry",id);
            }
            if(column==="state"){
                const id = await checkParent(value)
                return await address.deleteFromTable("cities","state_idState",id);
            }
        break;
        case "country":
            if(column==="id"){
                return await address.deleteFromTable("country","idCountry",value);
            }
            if(column==="name"){
                return await address.deleteFromTable("country","CountryName",value)
            }
        break;
        case "state":
            if(column==="id"){
                return await address.deleteFromTable("state","idState",value);
            }
            if(column==="name"){
                return await address.deleteFromTable("state","name",value)
            }
        break;
        case "address":
            if(column==="city"){
                if(typeof value==="string"){
                    return await address.deleteFromTable("adress","cities_idCities",await address.getCityBy("name",value));
                }
                    return await address.deleteFromTable("adress","cities_idCities",value);
            }
            if(column==="id"){
                return await address.deleteFromTable("adress","idAdress",value);
            };
        default:
            return errors[2];
    }
}
export default {
    getStates,
    getStateBy,
    createState,
    createStates,
    getCountries,
    getCountryBy,
    createCountry,
    createCountries,
    getCities,
    getCityBy,
    createCity,
    createCities,
    getAddresses,
    getAddressById,
    createAddress,
    createAddresses,
    checkAddress,
    turnAddressToId,
    deleteFromTable
}