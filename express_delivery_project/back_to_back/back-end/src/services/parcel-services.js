import parcel from '../data/parcel-data.js'
import shipment from './shipment-services.js';
import address from './address-services.js';
import user from './user-services.js';
import product from './product-services.js';
import errors from '../data/errors.js';
import warehouse from './warehouse-services.js'
const getById = async(id) => {
    return await parcel.getById(id)
}
const createParcel = async(shipmentId, origin, client, productObj, destination, to=undefined) => {
    const checkError = (obj) => {
        for(const checkers in obj){
            if(obj[checkers]){
                if(obj[checkers].error){
                    return {error: obj[checkers].error};
                }
            }
        }
        return false;
    }
    const originAddress = await address.turnAddressToId(origin);
    const addressExists = await warehouse.getWarehouse("address", originAddress);

    const destinationAddress = await address.turnAddressToId(destination);
    const warehouseExists = await warehouse.getWarehouse("address", destinationAddress)

    const shipmentExistsByAddress = await shipment.getBy("warehouse", addressExists.warehouse);
    const shipmentExists =shipmentId ? (await shipment.getBy("id", shipmentId)).idshipments : ((!shipmentExistsByAddress.error)? shipmentExistsByAddress.idshipments : (await shipment.createShipment(addressExists.warehouse, warehouseExists.warehouse).shipmentId));
    const clientExists = await user.getUserBy("username", client)
    const productExists = productObj.id ? await product.getProductById(productObj.id):{};
    const newProduct = productObj.id ? {} : await product.createProduct(productObj.category, productObj.weight);

    const checkers = {originAddress, addressExists, destinationAddress, warehouseExists, shipmentExists, clientExists,productExists, newProduct}
    if(checkError(checkers).error){
        return checkError(checkers);
    }
    const productId = productExists.id ? productExists.id : newProduct.productId
    const res = await parcel.createParcel(shipmentExists, addressExists.address.id, clientExists.idclients, productId, warehouseExists.warehouse, to);
    if(res){
        return res;
    } return { error: errors[3]}
}
const updateParcel = async(id, shipmentId, origin, client, productObj, destination, to=undefined) => {
    const checkError = (obj) => {
        for(const checkers in obj){
            if(obj[checkers]){
                if(obj[checkers].error){
                    return {error: obj[checkers].error};
                }
            }
        }
        return false;
    }
    const originAddress = await address.turnAddressToId(origin);
    const addressExists = await warehouse.getWarehouse("address", originAddress);

    const destinationAddress = await address.turnAddressToId(destination);
    const warehouseExists = await warehouse.getWarehouse("address", destinationAddress);
    const shipmentExistsByAddress = await shipment.getBy("warehouse", addressExists.warehouse);
    const shipmentExists =shipmentId ? (await shipment.getBy("id", shipmentId)).idshipments : ((!shipmentExistsByAddress.error)? shipmentExistsByAddress.idshipments : (await shipment.createShipment(addressExists.warehouse, warehouseExists.warehouse).shipmentId));

    const clientExists = await user.getUserBy("username", client)
    const productExists = productObj.id ? await product.getProductById(productObj.id):{};
    const newProduct = productObj.id ? {} : await product.createProduct(productObj.category, productObj.weight);

    const checkers = {originAddress, addressExists, destinationAddress, warehouseExists, shipmentExists, clientExists,productExists, newProduct}
    if(checkError(checkers).error){
        return checkError(checkers);
    }
    const productId = productExists.id ? productExists.id : newProduct.productId
    const res = await parcel.updateParcel(id ,shipmentExists, addressExists.address.id, clientExists.idclients, productId, warehouseExists.warehouse, to);
    if(res){
        return res;
    } return { error: errors[3]}
}
const deleteParcel = async(id) => {
    const foundParcel = getById(id);
    if(!foundParcel.error){
        const res = await parcel.removeShipment(id);
        if(!res.error){
            return await parcel.deleteParcel(id);
        }
        return res;
    } return foundParcel

}
const filterByWeight = async(criteria) => {
    return await parcel.filterByWeight(criteria);
}
const filterByCriteria = async(column, value) => {
    switch(column){
        case "user":
            if(isNaN(parseInt(value))){
                const foundUser = await user.getUserBy("username", value)
                return await parcel.filterByCriteria("clients_idclients", await foundUser.idclients);
            }
            return await parcel.filterByCriteria("clients_idclients", value);
        case "warehouse":
            const addressId = await address.turnAddressToId(value);
            const warehouseId = await warehouse.getWarehouse("address",addressId)
            return await parcel.filterByCriteria("warehouse_idWarehouse", warehouseId.warehouse);
        case "category":
            if(isNaN(parseInt(value))){
                const id = await product.getCategoryBy("categoryName", value)
                return await parcel.filterByCriteria("category_idcategory", id.idcategory);
            }
            return await parcel.filterByCriteria("category_idcategory", value);
        case "shipment":
            return await parcel.filterByCriteria("shipments_idshipments", value);
        case "state":
            return await parcel.filterByCriteria("state", value);
        default: 
        return {error: errors[3]}
    }
}

const removeShipment = async(id) => {
    return await parcel.removeShipment(id);
}

const addShipment = async(id, shipmentId) => {
    return await parcel.addShipment(id, shipmentId);
}

export default {
    getById,
    createParcel,
    updateParcel,
    deleteParcel,
    filterByWeight,
    filterByCriteria,
    removeShipment,
    addShipment
}