import errors from '../data/errors.js';
import shipment from '../data/shipment-data.js';
import warehouseServices from './warehouse-services.js';

const getBy = async(column, value) => {
    switch(column){
        case "id":
            return shipment.getBy("idshipments", value)
        case "warehouse":
            const warehouseToAddress = await warehouseServices.getWarehouse("warehouse", value);
            if(!warehouseToAddress.error){
                const res = await shipment.getBy("origin_idWarehouse", warehouseToAddress.address.id)
                return res
            } 
            return warehouseToAddress;
        default: 
            return {error: errors[21]}
    }
}
const createShipment = async (originWarehouse, destinationWarehouse) => {
    const warehouseToAddress = await warehouseServices.getWarehouse("warehouse", originWarehouse);
    if(!warehouseToAddress.error){
        const res = await shipment.createShipment(warehouseToAddress.address.id, destinationWarehouse);
        return res
    } 
    return warehouseToAddress;
}
const updateShipment = async (id, originWarehouse, destinationWarehouse) => {
    const shipmentExists = await getBy("id", id);
    if(!shipmentExists.error && shipmentExists.state == "preparing"){
        const warehouseToAddress = await warehouseServices.getWarehouse("warehouse", originWarehouse);
        if(!warehouseToAddress.error){
            const res = await shipment.updateShipment(id, warehouseToAddress.address.id, destinationWarehouse);
            return res
        } 
        return warehouseToAddress;
    } 
    return shipmentExists;
}

const departShipment = async (id) => {
    const res = await shipment.departShipment(id);
    return res;
}

const finishShipment = async (id) => {
    const res = await shipment.finishShipment(id);
    return res;
}

const deleteShipment = async (id) => {
    const shipmentExists = await getBy("id", id);
    if(!shipmentExists.error && shipmentExists.state == "preparing"){
        const res = await shipment.deleteShipment(id);
        return res;
    } 
    return shipmentExists;
}

export default {
    getBy,
    createShipment,
    updateShipment,
    departShipment,
    finishShipment,
    deleteShipment
}