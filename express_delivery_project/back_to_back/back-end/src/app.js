import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import passport from 'passport';
import dotenv from 'dotenv';
import { userRouter } from './controllers/user-controller.js';
import { warehouseRouter } from './controllers/warehouse-controller.js';
import { parcelRouter } from './controllers/parcel-controller.js';
import { addressRouter } from './controllers/address-controller.js';
import { shipmentRouter } from './controllers/shipment-controller.js';
import { productRouter } from './controllers/product-controller.js';
import { jwtStrategy } from './auth/strategy.js';
const PORT = dotenv.config().parsed.PORT;   

const app = express();

passport.use(jwtStrategy)

app.use(cors(), helmet(), express.json());

app.use(passport.initialize());

app.use('/users', userRouter);
app.use('/warehouses', warehouseRouter);
app.use('/parcels', parcelRouter);
app.use('/address', addressRouter);
app.use('/shipment', shipmentRouter);
app.use('/product', productRouter);


app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));
