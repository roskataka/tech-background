/* eslint-disable no-negated-condition */
import React, { useEffect, useState } from 'react';
import backend from '../constants.js';
import AddressChoice from '../side-services/Address-manager.js';
const Home = () => {
        const [count, setCount] = useState(0);
        const [cityQuery, setCityQuery] =useState("");
        const [warehouses, setWarehouses] = useState("")
        useEffect(()=> {                                                                    //------------------------------------GETS USER COUNT
            fetch(`${backend}/users/count/`,{
                mode: 'cors',
                method: 'GET',
                headers: {
                    'Content-Type':'application/json'
                }})
                .then(res => res.json())
                .then(res=> setCount(res.count));
        },[]);
        useEffect(()=>{
            if(cityQuery!==""){
                fetch(`${backend}/warehouses/warehouses/${cityQuery}/`,{
                    mode: 'cors',
                    method: 'GET',
                    headers: {
                        'Content-Type':'application/json'
                    }})
                    .then(res => res.json())
                    .then(res=> setWarehouses(res));
            }

        },[cityQuery])
        const handleChange = event => {
            const {name, value} = event.target
            if(name==="city"){
                setCityQuery(value)
            }
        }
        return <div className="body">
                <div className="login-window">
                    <h1 className="intro-info">{count} registrations at express delivery.</h1>
                    <div className="split-screen">
                            <div className="first-split">
                            <form onChange={handleChange} action="/home2" className="warehouse-search">
                                <AddressChoice tag="1"/>
                            </form>
                            <div className="warehouse-result">
                                <label className="warehouse-text">{warehouses[0]?'We have warehouses at the following addresses:':[]}</label>
                                {warehouses[0]?<div className="warehouse-list">
                                                    {warehouses[0]?warehouses.map(a=><><label key={"address"+a.idAdress} className="address-list-name">{a.address}</label><br></br></>):[]}
                                                    </div>:[]
                                }
                                <label className="warehouse-text1">{!warehouses[0]?"We don't have warehouses in this city currently!":[]}</label>

                            </div>
                            </div>
                            <div className="second-split">
                                <a className = "home-login-redirect" href="/login">Login</a>
                                <br></br>
                                <h1 className="other-choice1" >OR</h1>
                                <div className="other-choice-cover1"></div>
                                <a className = "home-register-redirect" href="/register">Register</a> 
                            </div>
                    </div>
                </div>
        </div>
}

export default Home;