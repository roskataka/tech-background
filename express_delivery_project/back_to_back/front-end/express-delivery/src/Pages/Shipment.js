import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { useHistory } from 'react-router-dom';
import ParcelQuery from '../Components/PersonParcels';
import backend from '../constants';

const Shipment = ({match, location}) => {
    const currLink = useLocation();
    const token = localStorage.getItem('token');
    const [parcels, setParcels] = useState([]);
    const [query, setQuery] = useState('');
    const history = useHistory();

    useEffect(()=>{
        const param = location.search.slice(1);
        setQuery(param);
    },[currLink]);
    useEffect(()=> {
        const params = query.split('=')
        fetch(`${backend}/parcels/shipment/${params[1]}`,{
            mode: 'cors',
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${token}`
            }})
            .then(res => res.json())
            .then(res=> {
                if(res[0]){
                    setParcels(res);
                    console.log(res);
                } else {
                    setParcels([]);
                }
                if(typeof res === 'string'){
                    history.push('/')
                }
            });
    },[query]);
    return <>
        <ParcelQuery parcels={parcels} id={2}/>
    </>
}

export default Shipment;