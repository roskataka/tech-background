import React, { useState } from 'react';

const Menu = () => {
    return <div className="body">
        <div className="login-window">
            <div className="menu-buttons">
                <button className="parcel-create-redirection">
                    Create new shipment
                </button>
                <button className="view-existing-parcels-redirection">
                    View my shipments
                </button>
            </div>
        </div>
    </div>
}

export default Menu;