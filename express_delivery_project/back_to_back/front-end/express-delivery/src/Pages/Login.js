/* eslint-disable no-negated-condition */
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import backend from '../constants';
const Login = () => {
    const history = useHistory()
    const [form, setForm] = useState({
        username: {
            placeholder: 'Username',
            value: '',
            type: 'text'
        },
        password:{
            placeholder: 'Password',
            value: '',
            type: 'password'
        }
    })
    const [par, setPar] = useState("")
    const handleSubmit = event => {
        event.preventDefault()
        if(form.username.value.length<3 || form.password.value<3){
            setPar("Credentials are too short!")
        } else {
            const data = Object.keys(form)
            .reduce((acc, elementKey) => {
                return {
                    ...acc,
                    [elementKey]: form[elementKey].value
                }
            }, {})
            const dataRes = JSON.stringify(data);
            fetch(`${backend}/users/login`,{
                mode: 'cors',
                method: 'POST',
                body: dataRes,
                headers: {
                    'Content-Type':'application/json',
                }
            })
            .then(result => result.json())
            .then((response)=> {
                if(response['token']){
                    setPar('');
                    localStorage.setItem('token',response['token']);
                    history.push('/');
                }
            })
            .catch(a => {
                if(typeof a==="string"){
                    setPar('Wrong username or password!')
                }
            })
        }
    }
    const handleInputChange = event => {
        const {name, value} = event.target;

        const updatedElement = { ...form[name], value }
        const updatedForm = { ...form, [name]:updatedElement }
        setForm(updatedForm)

    }
    const formElements = Object.keys(form)
    .map(name => {
        return{
            id: name,
            config: form[name]
        }
    })
    .map(({id, config})=>{
        return (
            <div key={id+'login2'}className='login-duo' id={id+'-div'}>
                <label key={id+'login1'}>{config.placeholder}:</label>
                <br></br>
                <input
                    className='login-input'
                    type={config.type}
                    key={id+'login'}
                    name={id}
                    placeholder={config.placeholder}
                    value={config.value}
                    onChange={handleInputChange}
                />
            </div>
        )
    })
        return <div className="body">
                <div className="login-window">
                        <label className="react-response">{par}</label>
                        <form className="login-form" onSubmit={handleSubmit}>
                            {formElements}
                            <button type="submit" className="login-button">Login</button>
                        </form>
                        <br></br>
                        <h1 className="other-choice" >OR</h1>
                        <div className="other-choice-cover"></div>
                        <a className = "register-redirection" href="/register">Register Instead</a>
                </div>
        </div>
}

export default Login;