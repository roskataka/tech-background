/* eslint-disable no-restricted-globals */
/* eslint-disable no-negated-condition */
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import ParcelQuery from '../Components/PersonParcels';
import SingleProfile from '../Components/Profile';
import backend from '../constants';
import GoToHomeScreen from '../side-services/Admin-deny';
const SearchPage = ({match, location}) => {
        const currLink = useLocation();
        const token = localStorage.getItem('token');
        const [query, setQuery] = useState('');
        const [profile, setProfile] = useState({});
        const [par, setPar] = useState('');
        const [address, setAddress] = useState('');
        const [safe, turnOffSafe] = useState(true);
        const [parcels, setParcels] = useState([]);
        useEffect(()=>{
            if(query===''){
                setProfile({});
            } else {
                const searchCriteria = query.split('=')
                fetch(`${backend}/users/${searchCriteria[0]}/${searchCriteria[1]}`,{
                    mode: 'cors',
                    method: 'GET',
                    headers: {
                        'Content-Type':'application/json',
                        'Authorization':`Bearer ${token}`
                    }
                })
                .then(res => res.json())
                .then(res =>{
                    if(typeof res==='string'){
                        setPar(res);
                        setProfile({})
                    } else {
                        setPar("");
                        setProfile(res)
                    }
                })
                .catch()
            }
        },[query, token]);
        useEffect(()=>{
            const param = location.search.slice(1);
            setQuery(param);
        },[location.search]);
        useEffect(()=>{
            fetch(`${backend}/address/address-full/${profile.adress_idAdress}`,{
                mode: 'cors',
                method: 'GET',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization':`Bearer ${token}`
                }})
                .then(res => res.json())
                .then(res=> setAddress(res));
                fetch(`${backend}/users/parcels/${profile.idclients}`,{
                    mode: 'cors',
                    method: 'GET',
                    headers: {
                        'Content-Type':'application/json',
                        'Authorization':`Bearer ${token}`
                    }})
                    .then(res => res.json())
                    .then(res=> {
                        if(res[0]){
                            setParcels(res);
                        } else {
                            setParcels([]);
                        }
                    });
        },[profile, token]);
        useEffect(()=>{
            if(!safe){
                const res = confirm("Are you sure you want to delete the user!");
                if(res===false){
                    turnOffSafe(true)
                } else {
                    fetch(`${backend}/users/${profile.idclients}`,{
                        mode: 'cors',
                        method: 'DELETE',
                        headers: {
                            'Content-Type':'application/json',
                            'Authorization':`Bearer ${token}`
                        }})
                        .then(res=> res.json())
                        .then(res => {
                            if(res.error){
                                history.push('/')
                            }
                            setProfile({});
                            setAddress({})
                        })
                }
            }
        },[safe])
        const handleDeletion = event => {
            event.preventDefault()
            turnOffSafe(false);
        }
        console.log(parcels)
        return <div className="body">
            {par.length!==0?(<div className="search-response"><p className="above1">{par}</p></div>):[]}
            {profile!=={}?(<div className="search-content">
                {profile!=={}?(<SingleProfile profile={profile} address={address}/>):[]}
                {currLink.pathname.includes('search')?<h1 onClick={handleDeletion} className="delete-profile-button">🗑️</h1>:[]}
                <ParcelQuery parcels={parcels} id={1}/>
            </div>):[]}
            <GoToHomeScreen/>

        </div>
}

export default SearchPage;