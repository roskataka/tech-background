/* eslint-disable no-restricted-globals */
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { useHistory } from 'react-router-dom';
import backend from '../constants';
import IdToAddress from '../side-services/IdToAddress';
import IdToWarehouse from '../side-services/IdToWarehouse';
const Parcel = () => {
    const [parcel, setParcel] = useState({});
    const [safe, turnOffSafe] =useState(true);
    const token = localStorage.getItem('token');

    const [queryToDelete, deleteQuery]= useState('');
    useEffect(()=>{
        if(!safe){
            const res = confirm("Are you sure you want to delete the user!");
            if(res===false){
                turnOffSafe(true)
            } else {
                fetch(`${backend}/parcels/${queryToDelete}`,{
                    mode: 'cors',
                    method: 'DELETE',
                    headers: {
                        'Content-Type':'application/json',
                        'Authorization':`Bearer ${token}`
                    }})
                    .then(res=> res.json())
                    .then(res => {
                        console.log(res)
                    })
            }
        }
    },[safe, queryToDelete, token])
    const currLink = useLocation();
    const history = useHistory();
    const toShipment = event => {
        event.preventDefault();
        const {name} = event.target;
        console.log(name)
        history.push(`/shipment?id=${name}`)
    }
    console.log(parcel)
    const updateParcel = event => {
        event.preventDefault();

    }
    const handleDelete = event => {
        event.preventDefault();
        const {name} = event.target;
        deleteQuery(name);
        turnOffSafe(false);
    }
    useEffect(()=>{
        const params = ((currLink.search).slice(1)).split('=');
        if(params[1]){
            fetch(`${backend}/parcels/id/${params[1]}`,{
                mode: 'cors',
                method: 'GET',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization':`Bearer ${token}`
                }})
                .then(res => res.json())
                .then(res=> {
                    if(!res.error){
                        setParcel(res);
                        console.log(res)
                    }else {
                        console.log('hello')
                    };
                });
        }
    },[currLink, token])
    return <>
            <div className={"singleParcel"+4} >
                <div className={"two-rows"+4}>
                    <div className={"first-row"+4}>                
                        <h4 className={"parcel-type"+4}> Type:</h4>
                        <h4 className={"parcel-category"+4}> Category:</h4>
                        <h4 className={"parcel-status"+4}> status:</h4>
                        <h4 className={"parcel-weight"+4}> Weight:</h4>
                        <h4 className={"parcel-origin"+4}> From:</h4>
                        <h4 className={"parcel-destination"+4}> To:</h4>
                        <h4 className={"parcel-names"+4}>Name: </h4>

                    </div>
                    <div className={"second-row"+4}>
                        <h4 className={"parcel-type"+4}>{parcel.travelingTo} </h4>
                        <h4 className={"parcel-category"+4}>{parcel.categoryName} </h4>
                        <h4 className={"parcel-status"+4}>{parcel.state }</h4>
                        <h4 className={"parcel-weight"+4}>0.{parcel.Weight} Kg</h4>
                        <h4 className={"parcel-origin"+4}>{<IdToAddress id={parcel.adress_idAdress}/>}</h4>
                        <h4 className={"parcel-destination"+4}>{<IdToWarehouse id={parcel.warehouse_idWarehouse}/>}</h4>
                        <h4 className={"parcel-names"+4}>{parcel.firstname} {parcel.lastname}</h4>

                    </div>
                </div>
                <div className ={"parcel-buttons"+4}>                
                    {parcel?(<button name={parcel.shipments_idshipments} onClick={toShipment} className={"single-parcel-button"+4}><p>Go to shipment! 🔎</p></button>):[]}
                    {parcel?(<a name={parcel.IdParcel} onClick={updateParcel} className={"single-parcel-update"+4}>⚙️</a>):[]}
                    {parcel?(<a name={parcel.IdParcel} onClick={handleDelete} className={"single-parcel-delete"+4}>🗑️</a>):[]} 
                </div>

            </div>
    </>
}

export default Parcel;