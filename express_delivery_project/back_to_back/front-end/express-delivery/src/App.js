import './App.css';
import Header from './Components/Header.js';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import SearchPage from './Pages/SearchPage';
import Home from './Pages/Home';
import Login from './Pages/Login';
import Register from './Pages/Register';
import Shipment from './Pages/Shipment';
import Parcel from './Pages/parcel';
import Menu from './Pages/LoggedMenu';

function App() {
  return (
    <BrowserRouter>
        <Header/>
        <Switch>
        <Route path ="/parcel/" component={Parcel}/>

        <Route path ="/shipment/" component={Shipment}/>

        <Route exact path ="/login" component={Login}/>
        <Route exact path ="/register" component={Register}/>
        <Route exact path ="/" component={Home}/>
        <Route exact path ="/home" component={Menu}/>
        <Route exact path="/search" component= {SearchPage}/>
        </Switch>
    </BrowserRouter>

  );
}

export default App;
