/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';

const SingleProfile = (obj) => {
    
    const profile = obj.profile;
    const address = obj.address;
    const bool = (!!profile && !!address)?true:false
    return <div className="user-profile">

    {bool?<><div className="profile-first-column">
        <div className="profile-pic-placeholder">
            <img className="profile-pic" title="Sorry, storing images in the database costs money!" src='https://www.pngkey.com/png/full/52-523516_empty-profile-picture-circle.png'/>
        </div>
        <br></br>

    </div>
    <div className="profile-second-column">
        <h2>Username: </h2>
        <h1>{profile.username}</h1>
        <br></br>
        <h2>Name: </h2>
        <h1>{profile.firstname} {profile.lastname}</h1>
        <br></br>
        <h2>Email: </h2>
        <h1>{profile.email}</h1>
    </div>

    {address.address?(<div className="profile-third-column">
        <h2>Address:</h2>
        <div className="address">
            <h1>{address.address.address}, {address.city.name}, {address.parent.name} </h1>
        </div>
    </div>):[]}
    </>:[]}
    </div>
}

export default SingleProfile;