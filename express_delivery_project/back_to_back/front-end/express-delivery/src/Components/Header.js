/* eslint-disable no-negated-condition */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useLocation } from 'react-router-dom';
import backend from '../constants';

const Header = () => {
        const currLink = useLocation();
        const [bool, changeBool] = useState(false);
        const [logged, setLogged] = useState(false);
        const token = localStorage.getItem('token');
        useEffect(()=> {
                if(token){
                        fetch(`${backend}/users/`,{
                            mode: 'cors',
                            method: 'GET',
                            headers: {
                                'Content-Type':'application/json',
                                'Authorization':`Bearer ${token}`
                            }})
                            .then(res=> res.json())
                            .then(res => {
                                if(res.error){
                                    changeBool(false)
                                } else {
                                        changeBool(true);
                                }
                            })
                    }
                        fetch(`${backend}/users/checklogged/`,{
                            mode: 'cors',
                            method: 'GET',
                            headers: {
                                'Content-Type':'application/json',
                                'Authorization':`Bearer ${token}`
                            }})
                            .then(res => res.json())
                            .then(res=> {
                                if(res===true){
                                        if(currLink.pathname === '/login' || currLink.pathname === '/register'){
                                              history.push('/');  
                                        }
                                        setLogged(true);
                                } else {
                                        setLogged(false);
                                }
                            })
                            .catch(setLogged(false));
        },[currLink, token])

        const [search, setSearch] = useState({
                search: {
                        placeholder: 'Search...',
                        value: '',
                        type: 'text'
                }
        })
        const [query, setQuery] =useState("username");
        const handleInputChange = event => {
                const {value} = event.target;
                const updatedSearch = {...search[search],value}
                setSearch(updatedSearch)
        }
        const handleInputChange1 = event => {
                const {value} = event.target;
                setQuery(value);
        }
        const history = useHistory();
        const searchRedirect = event => {
                event.preventDefault();
                history.push(`/search?${query}=${search.value}`);
        }
        const homeButton = event => {
                event.preventDefault();
                if(!(currLink.pathname==="")){
                history.push('/')
                }
        }
        const logOut = event => {
                event.preventDefault();
                localStorage.removeItem('token');
                history.push('/login')
        }
        return <div className="header">
                {logged?<button onClick={logOut} className="log-out-button">Log out</button>:[]}
                <label className="search-tag">Express Delivery</label>
                {bool?(<><form className="search-bar"> 
                        <select onChange={handleInputChange1} className="search-query" name="criteria">
                                <option value="username">Username</option>
                                <option value="firstname">First name</option>
                                <option value="lastname">Last name</option>
                                <option value="email">Email</option>
                        </select>    
                        <input className='search-box' type='text' onChange={handleInputChange} placeholder="Search..."></input>
                        <br></br>
                        <button onClick={searchRedirect} className="search-redirection">🔎</button>
                </form>
                </>
                ):[]}
                {currLink.pathname!=="/"?<h1 className="header-home-button" onClick={homeButton}>🏠</h1>:[]}

        </div>
}

export default Header