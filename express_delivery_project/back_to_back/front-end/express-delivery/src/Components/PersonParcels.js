/* eslint-disable no-restricted-globals */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import backend from '../constants';
import IdToAddress from '../side-services/IdToAddress';
import IdToWarehouse from '../side-services/IdToWarehouse';

const ParcelQuery = (obj) => {
    const parcels = obj.parcels;
    const id = obj.id;
    const token = localStorage.getItem('token')
    const history = useHistory();
    const [safe, turnOffSafe] =useState(true);
    const [queryToDelete, deleteQuery]= useState('');
    useEffect(()=>{
        if(!safe){
            const res = confirm("Are you sure you want to delete the user!");
            if(res===false){
                turnOffSafe(true)
            } else {
                fetch(`${backend}/parcels/${queryToDelete}`,{
                    mode: 'cors',
                    method: 'DELETE',
                    headers: {
                        'Content-Type':'application/json',
                        'Authorization':`Bearer ${token}`
                    }})
                    .then(res=> res.json())
                    .then(res => {
                        if(res.error){
                            history.push('/')
                        }
                        if(res.idParcel){
                            window.location.reload();
                        }
                    })
            }
        }
    },[safe])
    const toShipment = event => {
        event.preventDefault();
        const {name} = event.target;
        history.push(`/shipment?id=${name}`)
    }
    const updateParcel = event => {
        event.preventDefault();
        const {name} = event.target;
        history.push(`/parcel?id=${name}`)
    }
    const handleDelete = event => {
        event.preventDefault();
        const {name} = event.target;
        deleteQuery(name);
        turnOffSafe(false);
    }
    return <div className={"parcels"+id}>
        {parcels[0]?(parcels.map(a=> {
            return <div className={"singleParcel"+id} >
                    <div className={"two-rows"+id}>
                        <div className={"first-row"+id}>                
                            <h4 className={"parcel-type"+id}> Type:</h4>
                            <h4 className={"parcel-category"+id}> Category:</h4>
                            <h4 className={"parcel-status"+id}> status:</h4>
                            <h4 className={"parcel-weight"+id}> Weight:</h4>
                            <h4 className={"parcel-origin"+id}> From:</h4>
                            <h4 className={"parcel-destination"+id}> To:</h4>
                            <h4 className={"parcel-names"+id}>Name: </h4>

                        </div>
                        <div className={"second-row"+id}>
                            <h4 className={"parcel-type"+id}>{a.travelingTo} </h4>
                            <h4 className={"parcel-category"+id}>{a.categoryName} </h4>
                            <h4 className={"parcel-status"+id}>{a.state }</h4>
                            <h4 className={"parcel-weight"+id}>0.{a.Weight} Kg</h4>
                            <h4 className={"parcel-origin"+id}>{<IdToAddress id={a.adress_idAdress}/>}</h4>
                            <h4 className={"parcel-destination"+id}>{<IdToWarehouse id={a.warehouse_idWarehouse}/>}</h4>
                            <h4 className={"parcel-names"+id}>{a.firstname} {a.lastname}</h4>

                        </div>
                    </div>
                    <div className ={"parcel-buttons"+id}>                
                        {id===1?(<button name={a.shipments_idshipments} onClick={toShipment} className={"single-parcel-button"+id}>Go to shipment! 🔎</button>):[]}
                        {id<3?(<a name={a.idParcel} onClick={updateParcel} className={"single-parcel-update"+id}>⚙️</a>):[]}
                        {id<3?(<a name={a.idParcel} onClick={handleDelete} className={"single-parcel-delete"+id}>🗑️</a>):[]} 
                    </div>
    
                </div>
        })):[]}
    </div>
}

export default ParcelQuery;