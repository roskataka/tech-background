/* eslint-disable prefer-destructuring */
/* eslint-disable spaced-comment */
/* eslint-disable capitalized-comments */
/* eslint-disable multiline-comment-style */
/* eslint-disable react/prop-types */
import React, { useEffect, useState} from 'react';

const Tooltip = (props) => {
    // eslint-disable-next-line react/prop-types
    // eslint-disable-next-line prefer-destructuring
    const bool = props.obj.bool;
    const tooltip = props.obj.tooltip;
    const [tooltips, toggleTooltip] = useState('hidden');
    useEffect(()=>{
        let mounted = true;
        if(mounted){
            // eslint-disable-next-line no-constant-condition
            if(bool){
                // eslint-disable-next-line no-console
                toggleTooltip("visible");
            } else {
                // eslint-disable-next-line no-console
                toggleTooltip("hidden");
            }
        }

        return function useEnd () {
            mounted=false;
        }
    },[bool])
    return <span className={tooltip} style={{visibility:`${tooltips}`}}>A user with this name already exists, please try another one!</span>
}

export default Tooltip;