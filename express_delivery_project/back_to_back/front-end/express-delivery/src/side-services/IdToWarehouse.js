import { useEffect, useState } from 'react';
import backend from '../constants';

const IdToWarehouse = (obj) => {
    const id = obj.id
    const [address, setAddress] = useState('');
    useEffect(()=>{
        if(id){
            fetch(`${backend}/warehouses/warehouse/warehouse/${id}`,{
                mode: 'cors',
                method: 'GET',
                headers: {
                    'Content-Type':'application/json'
                }})
                .then(res => res.json())
                .then(res=> {
                    if(typeof res !== "string"){
                        setAddress(`${res.address.address.address}, ${res.address.city.name}, ${res.address.parent.name}`)
                    }
                });
        }

    },[id, address])
    return address  ;
}

export default IdToWarehouse;