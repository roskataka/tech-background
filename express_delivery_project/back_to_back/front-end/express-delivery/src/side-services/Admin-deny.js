import { useEffect} from "react";
import { useHistory } from "react-router-dom";
import backend from "../constants";

const GoToHomeScreen = () => {
    const history = useHistory();

    const token = localStorage.getItem('token');
    useEffect(()=>{
        if(token){
            fetch(`${backend}/users/`,{
                mode: 'cors',
                method: 'GET',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization':`Bearer ${token}`
                }})
                .then(res=> res.json())
                .then(res => {
                    if(res.error){
                        history.push('/')
                    }
                })
        }
    })
    
    return <></>
}

export default GoToHomeScreen;