/* eslint-disable eqeqeq */
import React, { useState, useEffect } from 'react';
import backend from '../constants';
 
const AddressChoice = (obj) => {
    const id = obj.tag;
    const key=obj.key;
    const [parent, setParent] = useState("countries");
    const [parents, setParents] = useState([]);
    const [parentQuery, setParentQuery] = useState("");
    const [cities, setCities] = useState([]);
    const [cityQuery, setCityQuery] =useState("");
    useEffect(()=>{                                                                             
        fetch(`${backend}/address/${parent}`,{
            mode: 'cors',
            method: 'GET',
            headers: {
                'Content-Type':'application/json'
            }})
            .then(res => res.json())
            .then(res=> setParents(res));
    },[parent]);
    useEffect(()=>{

        if(parentQuery!==""){
            fetch(`${backend}/address/cities/${parentQuery}`,{
                mode: 'cors',
                method: 'GET',
                headers: {
                    'Content-Type':'application/json'
                }})
                .then(res => res.json())
                .then(res=> setCities(res));
        }
    },[parentQuery]);
    const changeParent = event => {
        const {value} = event.target;
        setParent(value);
        setCityQuery("");

    }
    const handleParentChange = event => {
        const {value} = event.target
        setParentQuery(value);
        setCityQuery("");
    }
    const handleCityChange = event => {
        const {value} = event.target
        setCityQuery(value);    
    }
    return (<>              
                <div className={"address-section"+id}>
                    <div className={"parent-section"+id}>
                        <input onChange={changeParent} type="radio" id="state" name="parent" className={"parent-button"+id} value="states"/>
                        <label htmlFor="state" className={"parent-button-text"+id}>State</label>
                        <input onChange={changeParent} type="radio" id="country" name="parent" className={"parent-button"+id} value="countries"/>
                        <label htmlFor="country" className={"parent-button-text"+id}>Country</label>
                    </div>
                    <select className={"parent-choice"+id} name="city-parent" onChange={handleParentChange}>
                        {parents[0]? (parents[0].idCountry?
                        parents.sort((a,b)=>a.CountryName.localeCompare(b.CountryName))
                        .map(a=> {
                            const {idCountry, CountryName} = a;
                        return (<option key={"parent"+idCountry} id={`${idCountry}`} value={CountryName}>{CountryName}</option>)
                        }) :
                        parents.sort((a,b)=>a.name.localeCompare(b.name))
                        .map(a=>{
                            const {idState, name} = a;
                        return <option key={"parent"+idState} id={`${idState}`} value={name}>{name}</option>
                        })):[]}
                    </select>
                    <br></br>
                    <select defaultValue={{ label: "Select a city!", value: 0 }} className={"parent-choice"+id} name="city" onChange={handleCityChange}>
                        <option selected disabled>Please select a city!</option>
                        {cities[0]? (cities.sort((a,b)=>a.cityName.localeCompare(b.cityName)).map(a=><option key={"city"+a.idCities} id={`${a.idCities}`} value={a.cityName}>{a.cityName}</option>)):[]}
                    </select>
                    <br></br>
                        {id==2 && cityQuery!=="" ?<input name="address" className="address-choice" placeholder="Address" type="text"/>:[]}
                </div>

    </>)
}

export default AddressChoice;