import {useEffect, useState} from 'react';
import backend from '../constants';

const RenderTooltip = (props) => {
    const [exists, flipExists] = useState(false);
    // Checks if there's already a user with the specific username
    const checkUsername = async (value) => {
        await fetch(`${backend}/users/username-bool/${value}`,{
            mode: 'cors',
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
            }
        })
        .then(response => {
            const data = response.json();
            return data
        })
        .then(res => {
            flipExists(res);
        })
        .catch()
    }


    useEffect(()=> {
        let mounted = true;
            if(mounted){
                checkUsername(props);
            }
        return function cleanup (){
            mounted = false;
        }
    })
    // eslint-disable-next-line no-console
    return exists;
}

export default RenderTooltip;