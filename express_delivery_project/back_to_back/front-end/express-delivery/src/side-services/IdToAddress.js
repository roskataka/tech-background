import { useEffect, useState } from 'react';
import backend from '../constants';

const IdToAddress = (obj) => {
    const id = obj.id
    const [address, setAddress] = useState('');
    useEffect(()=>{
        if(id){
            fetch(`${backend}/address/address-full/${id}`,{
                mode: 'cors',
                method: 'GET',
                headers: {
                    'Content-Type':'application/json'
                }})
                .then(res => res.json())
                .then(res=> {
                    if(typeof res !== "string"){
                        setAddress(`${res.address.address}, ${res.city.name}, ${res.parent.name}`)
                    }
                });
        }
    },[id])
    return address;
}

export default IdToAddress;