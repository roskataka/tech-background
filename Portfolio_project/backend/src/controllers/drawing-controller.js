import express from 'express';
import drawingServices from '../services/drawing-services.js';
export const drawingRouter = express.Router();
drawingRouter
.get('/id/:id',
    async (req, res) => {
        const { id } = req.params;
        const result = await drawingServices.getDrawing(id);
        res.status(200).send(result)
    }
)
.put('/:id',
    async (req, res) => {
        const { id } = req.params;
        res.status(200).send(id);
    })
.post('/',
    async (req, res) => {
        if(req.body.secretKey!=="thisISmyPageMothafocka"){
            res.status(200).send("Incorrect key");
        } else {
            const {link, name} = req.body;

            const result = await drawingServices.createDrawing(link, name)
            res.status(200).send(result)
        }
    }
)
.post('/drawings/',
    async (req, res) => {
        if(req.body.secretKey!=="thisISmyPageMothafocka"){
            res.status(200).send("Incorrect key");
        } else {
            const { links } = req.body;
            for(const link of links){
                const result = await drawingServices.createDrawing(link);
                console.log(result)
            }
            res.status(200).send("Hello")
        }
    }
)
.get('/all/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await drawingServices.getDrawings(page);
        res.status(200).send(result)
    }
)
.get('/all-reverse/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await drawingServices.getDrawingsReverse(page);
        res.status(200).send(result)
    }
)
.get('/count/',
    async (req, res) => {
        const result = await drawingServices.getCount();
        res.status(200).send(result)
    }
)