import express from 'express';
import videoServices from '../services/video-services.js';
export const videoRouter = express.Router();
videoRouter
.get('/id/:id',
    async (req, res) => {
        const { id } = req.params;
        const result = await videoServices.getVideo(id);
        res.status(200).send(result)
    }
)
.put('/:id',
    async (req, res) => {
        const { id } = req.params;
        console.log(id);
        res.status(200).send(id);
    })
.post('/',
async (req, res) => {
    if(req.body.secretKey!=="thisISmyPageMothafocka"){
        res.status(200).send("Incorrect key");
    } else {
        const {link, name} = req.body;

        const result = await videoServices.createVideo(link, name)
        res.status(200).send(result)
    }
}
)
.get('/all/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await videoServices.getVideos(page);
        res.status(200).send(result)
    }
)
.get('/all-reverse/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await videoServices.getVideosReverse(page);
        res.status(200).send(result)
    }
)
.post('/all/',
    async (req, res) => {
        if(req.body.secretKey!=="thisISmyPageMothafocka"){
            res.status(200).send("Incorrect key");
        } else {
            const { links } = req.body;
            const er = [];
            for( const link of links){
                const result = await videoServices.createVideo(link)
                er.push(result);
            }
            res.status(200).send(er);
        }
    }
)
.get('/count/',
    async (req, res) => {
        const result = await videoServices.getCount();
        res.status(200).send(result)
    }
)
