import express from 'express';
import mangaServices from '../services/manga-services.js';
export const mangaRouter = express.Router();
mangaRouter
.get('/icon/:id',                                                       //get icon
    async (req, res) => {
        const { id } = req.params;
        const result = await mangaServices.getIcon(id);
        res.status(200).send(result)
    }
)
.post('/icon/',                                                         //create icon
async (req, res) => {
    if(req.body.secretKey!=="thisISmyPageMothafocka"){
        res.status(200).send("Incorrect key");
    } else {
        const {link, name} = req.body;

        const result = await mangaServices.createIcon(link, name)
        res.status(200).send(result)
    }
}
)
.get('/icons/:page',                                                    //get icons
    async (req, res) => {
        const { page } = req.params;
        const result = await mangaServices.getIcons(page);
        res.status(200).send(result)
    }
)
.get('/icons-reverse/:page',                                                    //get icons
    async (req, res) => {
        const { page } = req.params;
        const result = await mangaServices.getIconsReverse(page);
        res.status(200).send(result)
    }
)
.get('/count/',
    async (req, res) => {
        const result = await mangaServices.getCount();
        res.status(200).send(result)
    }
)
.post('/page/',                                                         //create page
    async (req, res) => {
        if(req.body.secretKey!=="thisISmyPageMothafocka"){
            res.status(200).send("Incorrect key");
        } else {
            const data = req.body;
            if(data.pageNumber==0){
                const mangaIcon = await mangaServices.createIcon(data);
                if(mangaIcon.error){
                    res.status(200).send(mangaIcon.error)
                } else {
                    const result = await mangaServices.createPage(data)
                    res.status(200).send(result)
                }
    
            } else {
                const result = await mangaServices.createPage(data)
                res.status(200).send(result)
            }
        }
    }
)
.post('/pages/',                                                         //create page
    async (req, res) => {
        if(req.body.secretKey!=="thisISmyPageMothafocka"){
            res.status(200).send("Incorrect key");
        } else {
            const {links, title, language, description, bulgTitle} = req.body;
            let fin = true;
            for(let i = 0; i<links.length; i++){
                    if(i==0){
                        const data = {
                            "link":links[i],
                            title,
                            language,
                            description,
                            bulgTitle
                        }
                        const mangaIcon = await mangaServices.createIcon(data);
                        if(mangaIcon.error){
                            fin = false;
                        }
    
                    }
                    const data = {
                        "link":links[i],
                        title,
                        language,
                        "pageNumber":i
                    }
                    const result = await mangaServices.createPage(data)
                    if(!result){
                        fin = false;
                    }
    
            }
            res.status(200).send(fin?"Your upload was successful!":"There was an error with your upload!")
        }

    }
)
.get('/page/:id',                                                       //get page
    async (req, res) => {
        const { id } = req.params;
        const result = await mangaServices.getPage(id);
        res.status(200).send(result)
    }
)
.post('/pages/',                                                    //get pages
    async (req, res) => {
        const { title, language } = req.body;
        const result = await mangaServices.getPages(title, language);
        res.status(200).send(result)
    }
)

