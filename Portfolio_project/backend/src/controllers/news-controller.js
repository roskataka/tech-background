import express from 'express';
import newsServices from '../services/news-services.js';
export const newsRouter = express.Router();
newsRouter
.post('/news/:category/:id',                                                         //create icon
async (req, res) => {
    if(req.body.secretKey!=="thisISmyPageMothafocka"){
        res.status(200).send("Incorrect key");
    } else {
        const data = req.body;
        const {category, id} = req.params
        const result = await newsServices.creatNewsArticle(data, category, id)
        res.status(200).send(result)
    }
}
)
.get('/news-bulgarian/:page',                                                    //get icons
    async (req, res) => {
        const { page } = req.params;
        const result = await newsServices.getNews(page, "Bulgarian");
        res.status(200).send(result)
    }
)
.get('/news-english/:page',                                                    //get icons
    async (req, res) => {
        const { page } = req.params;
        const result = await newsServices.getNews(page, "English");
        res.status(200).send(result)
    }
)