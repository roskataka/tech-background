import express from 'express'; 
import bulletServices from '../services/bullet-services.js'
import bulletData from '../data/bullet-data.js';
export const bulletsRouter = express.Router();
bulletsRouter
.get('/:id',                                                       //get icon
    async (req, res) => {
        const {id} = req.params;
        const result = await bulletServices.getBulletBy("id", id);
        res.status(200).send(result); 
    }
)
.post('/',                                                         //create icon
async (req, res) => {

    const {content, section, language, description} = req.body;
    const result = await bulletServices.createBullet(content, section, language, description);
    res.status(200).send(result); 
}
)
.post('/bullets',                                                         //create icon
async (req, res) => {

    const {bullets} = req.body;
    const results = [];
    for(const bullet of bullets) {
        const {content, section, language, description} = bullet;
        const result = await bulletServices.createBullet(content, section, language, description);
        results.push(result);
    }
    res.status(200).send(results); 
}
)
.get('/language/:language',
    async (req, res) =>{
        const {language} = req.params;
        const result = await bulletData.getBulletsByLanguage(language);
        res.status(200).send(result);
    }
)
.get('/section/:section',                                                    //get icons
    async (req, res) => {
        const { section } = req.params;
        const result = await bulletServices.getBulletsByParagraph(section);
        res.status(200).send(result)
    }
)
.delete('/:column/:value',
    async (req, res) => {
        const {column, value} = req. params;
        const result = await bulletServices.deleteBulletBy(column, value);
        res.status(200).send(result);
    }
)

