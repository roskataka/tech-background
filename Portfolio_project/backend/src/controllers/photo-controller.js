import express from 'express';
import photoServices from '../services/photo-services.js';
export const photoRouter = express.Router();
photoRouter
.get('/id/:id',
    async (req, res) => {
        const { id } = req.params;
        const result = await photoServices.getPhoto(id);
        res.status(200).send(result)
    }
)
.put('/:id',
    async (req, res) => {
        const { id } = req.params;
        console.log(id);
        res.status(200).send(id);
    })
.post('/',
    async (req, res) => {
        if(req.body.secretKey!=="thisISmyPageMothafocka"){
            res.status(200).send("Incorrect key");
        } else {
            const {link, name} = req.body;

            const result = await photoServices.createPhoto(link, name)
            res.status(200).send(result)
        }
}
)
.post('/photos/',
    async (req, res) => {
        if(req.body.secretKey!=="thisISmyPageMothafocka"){
            res.status(200).send("Incorrect key");
        } else {
            const { links } = req.body;
            for(const link of links){
                const result = await photoServices.createPhoto(link);
                console.log(result)
            }
            res.status(200).send("Hello")
        }
    }
)
.get('/all/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await photoServices.getPhotos(page);
        res.status(200).send(result)
    }
)
.get('/all-reverse/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await photoServices.getPhotosReverse(page);
        res.status(200).send(result)
    }
)
.get('/count/',
    async (req, res) => {
        const result = await photoServices.getCount();
        res.status(200).send(result)
    }
)