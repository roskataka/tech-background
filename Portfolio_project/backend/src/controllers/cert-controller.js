import express from 'express';
import certServices from '../services/cert-services.js';
export const certRouter = express.Router();
certRouter
.post('/',                                                         //create icon
async (req, res) => {
    const data = req.body;
    if(data.secretKey!=="thisISmyPageMothafocka"){
        res.status(200).send("Incorrect key");
    } else {
        const result = await certServices.createCertificate(data)
        res.status(200).send(result)
    }
}
)
.get('/id/:id',
    async ( req, res ) => {
        const {id} = req.params;
        const result = await certServices.getCertificate(id);
        res.status(200).send(result);
    }
)
.get('/all/',                                                    //get icons
    async (req, res) => {
        const result = await certServices.getCertificates();
        res.status(200).send(result)
    }
)
