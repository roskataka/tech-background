import express from 'express';
import pictureServices from '../services/picture-services.js';
export const pictureRouter = express.Router();
pictureRouter
.get('/id/:id',
    async (req, res) => {
        const { id } = req.params;
        const result = await pictureServices.getPicture(id);
        res.status(200).send(result)
    }
)
.put('/:id',
    async (req, res) => {
        const { id } = req.params;
        console.log(id);
        res.status(200).send(id);
    })
.post('/',
async (req, res) => {
    if(req.body.secretKey!=="thisISmyPageMothafocka"){
        res.status(200).send("Incorrect key");
    } else {
        const {link, name} = req.body;

        const result = await pictureServices.createPicture(link, name)
        res.status(200).send(result)
    }
}
)
.get('/all/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await pictureServices.getPictures(page);
        res.status(200).send(result)
    }
)
.get('/all-reverse/:page',
    async (req, res) => {
        const { page } = req.params;
        const result = await pictureServices.getPicturesReverse(page);
        res.status(200).send(result)
    }
)
.get('/count/',
    async (req, res) => {
        const result = await pictureServices.getCount();
        res.status(200).send(result)
    }
)
