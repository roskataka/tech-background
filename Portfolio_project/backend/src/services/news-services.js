import newsData from "../data/news-data.js";
import { errors } from "../errors.js";
const creatNewsArticle = async(data, category, id)=> {
    const {bulgarianTitle,englishTitle, link, bulgarianDescription, englishDescription} = data;
    if(!bulgarianTitle||!englishTitle||!link ||!bulgarianDescription||!englishDescription){
        return {
            error: errors[4]
        }
    }
    const res = await newsData.createNewsArticle(bulgarianTitle,englishTitle, link, bulgarianDescription, englishDescription, category, id);
    return res;
}
const getNews = async(page, language) =>{
    const res = await newsData.getNews((page-1)*10, language);
    return res;
}
export default {
    creatNewsArticle,
    getNews,
}