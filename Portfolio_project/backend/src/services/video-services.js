import videoData from '../data/video-data.js'
import { errors } from "../errors.js";
const createVideo = async(link, name) => {
    if(!link){
        throw new Error(errors[1])
    } else {
       return await videoData.createVideo(link, name);
    }
}
const getVideo = async(id) => {
    if(id<0){
        throw new Error(errors[2])
    } else {
        const res = await videoData.getVideo(id);
            return res;
    }
}

const getVideos = async (page) => {
    const res = await videoData.getVideos((page-1)*12);
    return res
}
const getVideosReverse = async (page) => {
    const res = await videoData.getVideosReverse((page-1)*12);
    return res
}
const getCount = async () => {
    const res = await videoData.getCount();
    return res;
}
export default{
    createVideo,
    getVideos,
    getVideosReverse,
    getVideo,
    getCount
}