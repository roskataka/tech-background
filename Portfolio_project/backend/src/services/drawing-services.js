import drawingData from '../data/drawing-data.js'
import { errors } from "../errors.js";
const createDrawing = async(link, name) => {
    if(!link){
        throw new Error(errors[1])
    } else {
       return await drawingData.createDrawing(link, name);
    }
}
const getDrawing = async(id) => {
    if(id<0){
        throw new Error(errors[2])
    } else {
        const res = await drawingData.getDrawing(id);
            return res;
    }
}

const getDrawings = async (page) => {
    const res = await drawingData.getDrawings((page-1)*12);
    return res
}
const getDrawingsReverse = async (page) => {
    const res = await drawingData.getDrawingsReverse((page-1)*12);
    return res
}

const getCount = async () => {
    const res = await drawingData.getCount();
    return res;
}

export default{
    createDrawing,
    getDrawing,
    getDrawings,
    getDrawingsReverse,
    getCount
}