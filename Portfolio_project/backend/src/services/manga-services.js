import mangaData from "../data/manga-data.js";
import { errors } from "../errors.js";
const createIcon = async(data)=> {
    const {title, link, description, language, bulgTitle} = data;
    if((!description) || description ===''){
        return {
            error: errors[4]
        }
    }
    const res = await mangaData.createIcon(title,link, description, language, bulgTitle);
    return res;
}
const getIcon = async(id) =>{
    const res = await mangaData.getIcon(id);
    return res;
}
const getIcons = async(page) =>{
    const res = await mangaData.getIcons((page-1)*12);
    return res;
}
const getIconsReverse = async(page) =>{
    const res = await mangaData.getIconsReverse((page-1)*12);
    return res;
}
const createPage = async(data)=> {
    const {link, title, pageNumber, language} = data;
    const res = await mangaData.createPage(link, title, pageNumber, language);
    return res;
}
const getPage = async(id)=> {
    const res = await mangaData.getPage(id);
    return res;
}
const getPages = async(title, language)=> {
    const res = await mangaData.getPages(title, language);
    return res;
}

const getCount = async () => {
    const res = await mangaData.getCount();
    return res;
}

export default {
    createIcon,
    getIcon,
    getIcons,
    getIconsReverse,
    createPage, 
    getPage,
    getPages,
    getCount
}