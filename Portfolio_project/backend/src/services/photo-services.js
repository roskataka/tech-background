import photoData from '../data/photo-data.js'
import { errors } from "../errors.js";
const createPhoto = async(link, name) => {
    if(!link){
        throw new Error(errors[1])
    } else {
       return await photoData.createPhoto(link, name);
    }
}
const getPhoto = async(id) => {
    if(id<0){
        throw new Error(errors[2])
    } else {
        const res = await photoData.getPhoto(id);
            return res;
    }
}

const getPhotos = async (page) => {
    const res = await photoData.getPhotos((page-1)*12);
    return res
}
const getPhotosReverse = async (page) => {
    const res = await photoData.getPhotosReverse((page-1)*12);
    return res
}

const getCount = async () => {
    const res = await photoData.getCount();
    return res;
}
export default{
    createPhoto,
    getPhotos,
    getPhotosReverse,
    getPhoto,
    getCount
}