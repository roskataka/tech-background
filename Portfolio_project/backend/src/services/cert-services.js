import certificateData from '../data/cert-data.js'

const createCertificate = async(data) => {
    const {link, bgName, engName} = data;
    const res = await certificateData.createCertificate(link, bgName, engName)
    return res;
}
const getCertificate = async(id) => {
    const res = await certificateData.getCertificate(id);
    return res;
}
const getCertificates = async () => {
    const res = await certificateData.getCertificates();
    return res
}
export default{
createCertificate,
getCertificate,
getCertificates
}