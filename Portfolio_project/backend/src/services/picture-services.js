import pictureData from "../data/picture-data.js";
import { errors } from "../errors.js";
const createPicture = async(link, name) => {
    console.log(link)
    if(!link){
        throw new Error(errors[1])
    } else {
       return await pictureData.createPicture(link, name);
    }
}
const getPicture = async(id) => {
    if(id<0){
        throw new Error(errors[2])
    } else {
        const res = await pictureData.getPicture(id);
            return res;
    }
}

const getPictures = async (page) => {
    const res = await pictureData.getPictures((page-1)*12);
    return res
}
const getPicturesReverse = async (page) => {
    const res = await pictureData.getPicturesReverse((page-1)*12);
    return res
}
const getCount = async () => {
    const res = await pictureData.getCount();
    return res;
}
export default{
    createPicture,
    getPicture,
    getPictures,
    getPicturesReverse,
    getCount
}