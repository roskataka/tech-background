import bulletData from "../data/bullet-data.js";
import {errors} from '../errors.js'

const createBullet = async (content, section, language, description) => {
    const result = await bulletData.createBullet(content, section, language, description);
    return result;
}

const getBulletBy = async (column, value) => {
    switch (column) {
        case "id":
            return await bulletData.getBulletBy("idbullets", value);
        case "section":
            return await bulletData.getBulletBy("section", value);
        case "content":
            return await bulletData.getBulletBy("content", value);
        default: 
            return {
                error: errors[5]
            } 
    }

}

const getBulletsByParagraph = async (paragraph) => {
    const result = await bulletData.getBulletsByParagraph( paragraph);
    return result;


}

const deleteBulletBy = async (column, value) => {
    switch (column) {
        case "id":
            return await bulletData.deleteBulletBy("idbullets", value);
        case "section":
            return await bulletData.deleteBulletBy("section", value);
        case "content":
            return await bulletData.deleteBulletBy("content", value);
        default: 
            return {
                error: errors[5]
            } 
    }



}

export default {
    createBullet,
    getBulletBy,
    getBulletsByParagraph,
    deleteBulletBy
}