import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import { pictureRouter } from './controllers/picture-controller.js';
import { photoRouter } from './controllers/photo-controller.js';
import { mangaRouter } from './controllers/manga-controller.js';
import { videoRouter } from './controllers/video-controller.js';
import { bulletsRouter } from './controllers/bullet-controller.js';
import { newsRouter } from './controllers/news-controller.js';
import { appsRouter } from './controllers/apps-controller.js';
import { drawingRouter } from './controllers/drawing-controller.js';
import { certRouter } from './controllers/cert-controller.js';



const app = express();

app.use(cors(), helmet(), express.json());

app.use('/pictures', pictureRouter);
app.use('/photos', photoRouter);
app.use('/mangas', mangaRouter);
app.use('/videos', videoRouter);
app.use('/bullets', bulletsRouter);
app.use('/news', newsRouter);
app.use('/drawings', drawingRouter)
app.use('/apps', appsRouter);
app.use('/certificates', certRouter)

app.listen(process.env.PORT || 5000, () => console.log(`App is listening on port ${process.env.PORT}`));

