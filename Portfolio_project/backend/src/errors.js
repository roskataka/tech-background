export const errors = {
    1: "A link is required!",
    2: "Wrong ID!",
    3: "A picture with this ID doesn't exist!",
    4: "You need to put a description!",
    5: "Incorrect bullet data!"
}
