import pool from "./pool.js";
const createPicture = async(link, name='')=> {
    const sql = `
    INSERT into pictures (link, name)
    VALUES (?, ?)
    `
    const result = await pool.query(sql, [link, name]);
    return {
        id: result.insertId,
        link,
        name
    }
}
const getPicture = async(id)=> {
    const sql = `
    SELECT * FROM pictures
    WHERE idpictures = ${id}
    `
    const result = await pool.query(sql, [id]);
    return result;
}
const getPictures = async(offset)=> {
    const sql = `
    SELECT * FROM pictures
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}
const getPicturesReverse = async(offset)=> {
    const sql = `
    SELECT * FROM pictures ORDER BY idpictures DESC
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}

const getCount = async() => {
    const sql = `
    SELECT COUNT(*) from pictures
    `
    const res = await pool.query(sql, [])
    return res;
}
export default {
    createPicture,
    getPicture,
    getPictures,
    getPicturesReverse,
    getCount
}