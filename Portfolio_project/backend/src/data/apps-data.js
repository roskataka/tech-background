import pool from "./pool.js";
const createApp = async(logo, link, name, description)=> {
    const sql = `
    INSERT into apps (logo, link, name, description)
    VALUES (?, ?, ?, ?)
    `
    const result = await pool.query(sql, [logo, link, name, description]);
    return {
        id: result.insertId,
        ...result
    }
}
const getApp = async(id) => {
    const sql = `
    SELECT * FROM apps where idapps = ${id}
    `
    const res = await pool.query(sql, [id]);
    return res;
}
const getApps = async(offset) =>{
        const sql = `
        SELECT * FROM apps ORDER BY idapps ASC LIMIT 12
        OFFSET ${offset} 
        `
        const res = await pool.query(sql, [offset]);
        return res;
}
const getAppsReverse = async(offset) =>{
    const sql = `
    SELECT * FROM apps ORDER BY idapps DESC LIMIT 12
    OFFSET ${offset} 
    `
    const res = await pool.query(sql, [offset]);
    return res;
}

const getCount = async() => {
    const sql = `
    SELECT COUNT(*) from apps
    `
    const res = await pool.query(sql, [])
    return res;
}
export default {
    createApp,
    getApp, 
    getApps,
    getAppsReverse,
    getCount
}