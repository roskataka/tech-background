import pool from "./pool.js";

const createBullet = async (content, section, language, description) => {
    const sql = `
    INSERT into bullets (content, section, language, description )
    VALUES (N?, N?, ?, N?)
    `
    const result = await pool.query(sql,[content, section, language, description]);
    return result;
}

const getBulletBy = async (column, value) => {
    const sql = `
    SELECT * FROM bullets WHERE ${column} = '${value}' LIMIT 1
    `
    const result = await pool.query(sql,[column, value]);
    return result;
}
const getBulletsByLanguage = async (value) => {
    const sql = `
    SELECT * FROM bullets WHERE language = '${value}'
    `
    const result = await pool.query(sql,[value]);
    return result;
}
const getBulletsByParagraph = async (paragraph) => {
    const sql = `
    SELECT * FROM bullets WHERE section = '${paragraph}'
    `
    const result = await pool.query(sql,[paragraph]);
    return result;
}

const deleteBulletBy = async (column, value) => {
    const sql = `
    DELETE FROM bullets WHERE ${column} = '${value}'
    `
    const result = await pool.query(sql,[column, value]);
    return result;
}

export default {
    createBullet,
    getBulletBy,
    getBulletsByParagraph,
    deleteBulletBy,
    getBulletsByLanguage
}