import pool from "./pool.js";
const createVideo = async(link, name='')=> {
    const sql = `
    INSERT into videos (link, name)
    VALUES (?, ?)
    `
    const result = await pool.query(sql, [link, name]);
    return {
        id: result.insertId,
        link,
        name
    }
}
const getVideo = async(id)=> {
    const sql = `
    SELECT * FROM videos
    WHERE idvideos = ${id}
    `
    const result = await pool.query(sql, [id]);
    return result;
}
const getVideos = async(offset)=> {
    const sql = `
    SELECT * FROM videos
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}
const getVideosReverse = async(offset)=> {
    const sql = `
    SELECT * FROM videos ORDER BY idvideos DESC
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}
const getCount = async() => {
    const sql = `
    SELECT COUNT(*) from videos
    `
    const res = await pool.query(sql, [])
    return res;
}
export default {
    createVideo,
    getVideo,
    getVideos,
    getVideosReverse,
    getCount
}