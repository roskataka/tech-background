import pool from "./pool.js";
const createCertificate = async(link, bgName, engName)=> {
    const sql = `
    INSERT into certificates (link, bgName, engName)
    VALUES (?, N?, ?)
    `
    const result = await pool.query(sql, [link, bgName, engName]);
    return {
        id: result.insertId,
        ...result
    }
}
const getCertificate = async(id) => {
    const sql = `
    SELECT * FROM certificates where idcertificates = ${id}
    `
    const res = await pool.query(sql, [id]);
    return res;
}
const getCertificates = async() =>{
        const sql = `
        SELECT * FROM certificates
        `
        const res = await pool.query(sql, []);
        return res;
}
export default {
    createCertificate,
    getCertificate,
    getCertificates
}