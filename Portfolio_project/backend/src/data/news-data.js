import pool from "./pool.js";
const createNewsArticle = async(bulgarianTitle,englishTitle, link, bulgarianDescription, englishDescription, category, id)=> {
    const sql = `
    INSERT into news (bulgarianTitle, englishTitle, link, bulgarianDescription, englishDescription, obj_id, type)
    VALUES (N?, ?, ?, N?, ?, ?, ?)
    `
    const result = await pool.query(sql, [bulgarianTitle,englishTitle, link, bulgarianDescription, englishDescription, category, id]);
    return {
        id: result.insertId,
        ...result
    }
}
const getNews = async(offset, language) =>{
    if(language==="Bulgarian"){
        const sql = `
        SELECT idnews, date, link, bulgarianTitle, bulgarianDescription FROM news LIMIT 10
        OFFSET ${offset} 
        `
        const res = await pool.query(sql, [offset]);
        return res;
    } else {
            const sql = `
            SELECT idnews, date, link, englishTitle, englishDescription FROM news LIMIT 10
            OFFSET ${offset} 
            `
            const res = await pool.query(sql, [offset]);
            return res;
    }

}
export default {
    createNewsArticle,
    getNews,
}