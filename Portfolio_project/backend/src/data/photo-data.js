import pool from "./pool.js";
const createPhoto = async(link, name='')=> {
    const sql = `
    INSERT into photos (link, name)
    VALUES (?, ?)
    `
    const result = await pool.query(sql, [link, name]);
    return {
        id: result.insertId,
        link,
        name
    }
}
const getPhoto = async(id)=> {
    const sql = `
    SELECT * FROM photos
    WHERE idphotos = ${id}
    `
    const result = await pool.query(sql, [id]);
    return result;
}
const getPhotos = async(offset)=> {
    const sql = `
    SELECT * FROM photos
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}
const getPhotosReverse = async(offset)=> {
    const sql = `
    SELECT * FROM photos ORDER BY idphotos DESC
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}
const getCount = async() => {
    const sql = `
    SELECT COUNT(*) from photos
    `
    const res = await pool.query(sql, [])
    return res;
}
export default {
    createPhoto,
    getPhoto,
    getPhotos,
    getPhotosReverse,
    getCount
}