import pool from "./pool.js";
const createDrawing = async(link, name='')=> {
    const sql = `
    INSERT into drawings (link, name)
    VALUES (?, ?)
    `
    const result = await pool.query(sql, [link, name]);
    return {
        id: result.insertId,
        link,
        name
    }
}
const getDrawing = async(id)=> {
    const sql = `
    SELECT * FROM drawings
    WHERE iddrawings = ${id}
    `
    const result = await pool.query(sql, [id]);
    return result;
}
const getDrawings = async(offset)=> {
    const sql = `
    SELECT * FROM drawings
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}
const getDrawingsReverse = async(offset)=> {
    const sql = `
    SELECT * FROM drawings ORDER BY iddrawings DESC
    LIMIT 12
    OFFSET ${offset}
    `
    const result = await pool.query(sql, [offset]);
    return result;
}

const getCount = async() => {
    const sql = `
    SELECT COUNT(*) from drawings
    `
    const res = await pool.query(sql, [])
    return res;
}
export default {
    createDrawing,
    getDrawings,
    getDrawing,
    getDrawingsReverse,
    getCount
}