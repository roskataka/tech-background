import pool from "./pool.js";
const createIcon = async(title, link, description, language, bgTitle)=> {
    if(language==="English"){
        const sql = `
        INSERT into manga_icons (title, link, description, language)
        VALUES (?, ?, N?, ?)
        `
        const result = await pool.query(sql, [title, link, description, language]);
        return {
            id: result.insertId,
            ...result
        }
    }
    const sql = `
    INSERT into manga_icons (title, link, description, language, bulgTitle)
    VALUES (?, ?, N?, ?, N?)
    `
    const result = await pool.query(sql, [title, link, description, language, bgTitle]);
    return {
        id: result.insertId,
        ...result
    }
}
const getIcon = async(id) =>{
    const sql = `
    SELECT * FROM manga_icons WHERE mangaid=${id}
    `
    const res = await pool.query(sql, [id]);
    return res;
}
const getIcons = async(offset) =>{
    const sql = `
    SELECT * FROM manga_icons LIMIT 12
    OFFSET ${offset}
    `
    const res = await pool.query(sql, [offset]);
    return res;
}
const getIconsReverse = async(offset) =>{
    const sql = `
    SELECT * FROM manga_icons ORDER BY mangaid DESC LIMIT 12
    OFFSET ${offset}
    `
    const res = await pool.query(sql, [offset]);
    return res;
}
const createPage = async(link, title, pageNumber, language)=> {
    const sql = `
    INSERT into mangas (title, link, pageNumber, language)
    VALUES (?, ?, ?, ?)
    `
    const result = await pool.query(sql, [title, link, pageNumber, language]);
    return result;
}
const getPage = async(id)=> {
    const sql = `
    SELECT * FROM mangas WHERE idpage = ${id}
    `
    const result = await pool.query(sql, [id]);
    return result;
}
const getPages = async(title, language)=> {
    const sql = `
    SELECT * FROM mangas WHERE title = '${title}' and language = '${language}'
    `
    const result = await pool.query(sql, [title, language]);
    return result;
}

const getCount = async() => {
    const sql = `
    SELECT COUNT(*) from manga_icons
    `
    const res = await pool.query(sql, [])
    return res;
}
export default {
    createIcon,
    getIcon,
    getIcons,
    getIconsReverse,
    createPage, 
    getPage,
    getPages,
    getCount
}