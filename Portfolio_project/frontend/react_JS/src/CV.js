import React, { useEffect, useState } from "react";
import Header from "./components/Header";
import jsPDF from "jspdf";
import "jspdf-autotable";
const CV = (props) => {
    const [data, setData] = useState([]);
    const [certs, setCert] =useState([]);

    useEffect(()=>{
        const language = props?"English":"Bulgarian";
        fetch(`https://ren-portfolio-backend.herokuapp.com/bullets/language/${language}`,
        {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        })
        .then(response => response.json())
        .then(res => setData(res))

        fetch(`https://ren-portfolio-backend.herokuapp.com/certificates/all`,
        {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        })
        .then(response => response.json())
        .then(res => setCert(res))
    },[props]);
    const personalData = data.filter(a => a.section === "personalData").map(a => <tr id={props?"eng":"bg"}  key={a.description}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const workExperience = data.filter(a => a.section === "workExperience").map(a => <tr id={props?"eng":"bg"}  key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const education = data.filter(a => a.section === "education").map(a => <tr id={props?"eng":"bg"}  key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const languages = data.filter(a => a.section === "languages").map(a => <tr id={props?"eng":"bg"} key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const socialSkills = data.filter(a => a.section === "socialSkills").map(a => <tr id={props?"eng":"bg"} key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const organizationalSkills = data.filter(a => a.section === "organizationalSkills").map(a => <tr id={props?"eng":"bg"} key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const techSkills = data.filter(a => a.section === "techSkills").map(a => <tr id={props?"eng":"bg"} key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const artSkills = data.filter(a => a.section === "artSkills").map(a => <tr id={props?"eng":"bg"} key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const otherSkills = data.filter(a => a.section === "otherSkills").map(a => <tr id={props?"eng":"bg"} key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    const otherInfo = data.filter(a => a.section === "otherInfo").map(a => <tr id={props?"eng":"bg"} key={a.description+a.idbullets}><td className="description">{a.description}:</td><td className="content">{a.content}</td></tr>);
    // eslint-disable-next-line react/jsx-no-target-blank
    const certificates = certs.map(a =><tr id={props?"eng":"bg"} key={a.engName+a.idcertificates}><td className="description">{props?a.engName:a.bgName}:</td><td className="content"><a className="certificate-link" href={a.link} target="_blank">{props?"Click here to go to the certificate":"Натиснете, за да отидете на сертификата"}</a></td></tr> )
    
    const personalDataToDoc = personalData.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const workExperienceDoc = workExperience.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const educationDoc= education.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const languagesDoc= languages.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const socialSkillsDoc= socialSkills.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const organizationalSkillsDoc= organizationalSkills.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const techSkillsDoc= techSkills.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const artSkillsDoc= artSkills.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const otherSkillsDoc= otherSkills.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    const otherInfoDoc= otherInfo.map(a=>`<${a.type}><${a.props.children[0].type} class="description"> ${a.props.children[0].props.children.join('')} </${a.props.children[0].type}> <${a.props.children[1].type} class="content"> ${a.props.children[1].props.children} </${a.props.children[1].type}> </${a.type}>`)
    
    const printDiv = event=>{
        event.preventDefault();
        // var divContents = document.getElementById("example").innerHTML;
        var a = window.open('', '', 'height=500, width=500');
        a.document.write('<html>');
        a.document.write('<style>');
        a.document.write('@font-face {font-family: Alt;src: url(../src/fonts/brush-type-bg-font.ttf);}')
        a.document.write(':root{--gray:#363b44;--very-dark-blue:#081B26;--light-blue-semi:#acdef2a2;--light-blue:#ACDEF2;--light-blue-2:#3DADD9;--dark-blue:#0F3D59;--teal-blue:#1C6C8C;--teal-blue-semi:#1c6c8c9c;--turquoise: #94D2BD;--turquoise-semi:  #94d2bda8;--white: rgba(255, 255, 255, 0.726);--white-semi: rgba(255, 255, 255, 0.685);--white-sem2i:rgba(255, 255, 255, 0.377);--yellow: #E9D8A6;--yellow-semi:  #e9d8a6a9;--gradient: linear-gradient(65deg, #d367c1 10%,#dedf40 25%,#62cb5c 50%,#00bbcb 75%,#ab79d6 90%);}')
        a.document.write('table.down{  padding: 1%;top: 10%;left: 10%; position: relative; width: fit-content;display: block;background-color: var(--very-dark-blue);border-radius: 1vw;font-family: Arial}')
        a.document.write('table tr{ height: 4vh;}');
        a.document.write('td.content{min-width: 25vw; max-width: 50vw; height: fit-content; font-family: Arial;}')
        a.document.write('td:not(td#table-indentifier) {color: var(--white);background-color: var(--gray); font-size: 1.5vw; padding: 3%; border-color: var(--gray);border-radius: 1vw;border-style: solid;}')
        a.document.write('.description{  font-size: 1.9vw;  height: 5vh; text-align: center; overflow-wrap: break-word;}')
        
        a.document.write('#information-row{  font-size: 1.8vw;  height: 5vh; text-align: center;}')
        a.document.write('#table-indentifier{    background-color: var(--light-blue-semi);border-radius: 1vw;color: var(--white);}')
        a.document.write('background-color: var(--light-blue-semi); color: var(--white)')
        a.document.write('.zero-width{  display: none;}')
        a.document.write('</style>')
        a.document.write('<body>');
        a.document.write("<table class='down'>")
        a.document.write("<tbody>")
        a.document.write(`<tr id="information-row"><td colSpan="2" id="table-indentifier">${props?"Personal Information":"Лична информация"}</td><td className="zero-width"></td></tr>`)
        a.document.write(personalDataToDoc);
        a.document.write(`<tr id="information-row"><td colSpan="2" id="table-indentifier">${props?"Professional experience":"Професионален опит"}</td><td className="zero-width"></td></tr>`)
        a.document.write(workExperienceDoc);
        a.document.write(`<tr id="information-row"><td colSpan="2" id="table-indentifier">${props?"Special courses, qualifications and education":"Специални обучения, курсове и квалификации"}</td><td className="zero-width"></td></tr>`)
        a.document.write(educationDoc)
        a.document.write(`<tr id="information-row"><td colSpan="2" id="table-indentifier">${props?"Languages":"Езици"}</td><td className="zero-width"></td></tr>`)
        a.document.write(languagesDoc)
        a.document.write(`<tr id="information-row"><td colSpan="2" id="table-indentifier">${props?"Personal skills and competences":"Лични умения и компетенции"}</td><td className="zero-width"></td></tr>`)
        a.document.write(socialSkillsDoc)
        a.document.write(organizationalSkillsDoc)
        a.document.write(techSkillsDoc)
        a.document.write(artSkillsDoc)
        a.document.write(otherSkillsDoc)
        a.document.write(otherInfoDoc)
        a.document.write("</tbody>")
        a.document.write("</table>")
        a.document.write('</body></html>');
        a.document.close();
        a.print()
    }
   return<div className="screen" >
            <Header bool={props} parent="about"/>
            <a className="back-button" href="/react">Back</a>
            <button id="pdf-button" onClick={printDiv}>{props?"Download my CV":"Свалете CV-то ми"}</button>
            <div id="portfolio">
                <table className="scrollable" id="example">
                    <tbody className="scrollable" >
                        <tr id="information-row"><td colSpan="2" id="table-indentifier">{props?"Personal Information":"Лична информация"}</td><td className="zero-width"></td></tr>
                        
                        {personalData}
                        <tr><td id="table-indentifier" colSpan="2">{props?"Professional experience":"Професионален опит"}</td><td className="zero-width"></td></tr>
                        {workExperience}
                        <tr><td id="table-indentifier" colSpan="2">{props?"Special courses, qualifications and education":"Специални обучения, курсове и квалификации"}</td><td className="zero-width"></td></tr>
                        {education}
                        <tr><td id="table-indentifier" colSpan="2">{props?"Languages":"Езици"}</td><td className="zero-width"></td></tr>
                        {languages}
                        <tr><td id="table-indentifier" colSpan="2">{props?"Personal skills and competences":"Лични умения и компетенции"}</td><td className="zero-width"></td></tr>
                        {socialSkills}
                        {organizationalSkills}
                        {techSkills}
                        {artSkills}
                        {otherSkills}
                        {otherInfo}
                        <tr><td id="table-indentifier" colSpan="2">{props?"Certificates and other documents":"Сертификати и други документи"}</td><td className="zero-width"></td></tr>
                        {certificates}
                    </tbody>
                </table>
                
            </div>
            <div className="scrollable" id="logos">
                <div className="exp-logo"><img className="photoshop-logo" id="exp-logo" src="https://seeklogo.com/images/A/adobe-photoshop-logo-7B88D7B5AA-seeklogo.com.png" alt="Photoshop"/></div>
                <div className="exp-logo"><img className="blender-logo" id="exp-logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Blender_logo_no_text.svg/2503px-Blender_logo_no_text.svg.png" alt="Blender"/></div>
                <div className="exp-logo"><img className="csp-logo" id="exp-logo" src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/a7ab2c08-5091-4c8e-a708-9bfa3c9639bf/dbr01gl-46b8dbef-6184-4b5f-97f4-439800abd0b8.png" alt="Csp"/></div>
                <div className="exp-logo"><img className="js-logo" id="exp-logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/2048px-Unofficial_JavaScript_logo_2.svg.png" alt="Javascript"/></div>
                <div className="exp-logo"><img className="react-logo" id="exp-logo" src="https://icon-library.com/images/react-icon/react-icon-15.jpg" alt="react"/></div>
                <div className="exp-logo"><img className="qb64-logo" id="exp-logo" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxANEg4PDQ8QDQ0NEA0NEA0NDQ8PDQ0NFhEYFhURFhUYHiggGBolGxMTITElJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGy0lHyA3Ly0tLSstLS0tLS0tKzAtLS0tLS0vLS0tLSstLS0tLS0tLS0tLS0tKy0tLS0tLS0tLf/AABEIAOAA4AMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcBAgUECAP/xABMEAABAwEBCAwKCAUDBQAAAAAAAQIDBBEFFlJUc5KT0gYSFSEzNFFTcbGy0QcTFBcxMkF0kbM1coSUobTB4SJhgaPwJENjI0KCwuL/xAAcAQABBQEBAQAAAAAAAAAAAAAAAQMEBQYHAgj/xABBEQABAgIDCgsGBgIDAAAAAAABAAIDEQQFMRIUFSFSU3GRodEGFjNBUVRykqKxwRMyNLLh4hc1YWOBwtLxB0KC/9oADAMBAAIRAxEAPwCnAD9qaLxj2Mts27mst9Nlq2Wm6JUVfiCapsBdjSaBdYXgOxpuhXWPN2FXitaHl7DuUKBNbwFxpuhXWF4C403QrrC3YS4VoeXsO5QoE1vAXGm6FdYXgLjTdCusF2EYVoeXsO5QoE1vAXGm6FdYXgLjTdCusF2EYVoeXsO5QoE1vAXGm6FdYXgLjTdCusF2EYVoeXsO5QoE1vAdjTdCusLwXY0mgXWEuwkwrQ8vYdyhQPZR0ayytiRyNVyq3bKlqbyKvo/ode9V3PMzV7z2ATYpcSPDhmTzJRwEkvVdzzM1e8Xqu55maveLclN37Ayht3KNgkl6rueZmr3i9V3PMzV7wuSi/YGUNqjYJJeq7nmZq94vVdzzM1e8Lkov2BlDao2CSXqu55maveL1Xc8zNXvC5KL9gZQ2qNgkl6rueZmr3i9R3PMzV7wuSi/YGUNqjYO/V7HHRMfIsrFRjVdYjVS2z+pwhE9DisiCbDNanruXw0GVi7aHkPXczhoMrF2kPLrE4ri/cD9wMrnAsQGTV7tqjl5EVfggJQJrIIP5wHYqzSrqmb/3YqzSrqnq5KtMDUvoGsKbghF/7sVZpV1Rf+7FWaVdULkowNS+gawpuCEt2fOXe8lZpV1SbKJIhRaTQ4tHl7QW2Y52f7QACKKqpuJxuPKSdlxNbSFXE43HlJOy4mpNhWLU1ryo0eqWi0AcVYlotPHdO6TKVGK9HPR6qibREX0dKnNvqh5ubNi7zzdBPso0V7bprSQu9aLThX0w83N8I9YX0w83N8I9YLsL3eVIyCu7aLThX0w83N8I9Y7rHWoi8qIvxFDgU1EgRIYm9sktFoAqaXju3wE+Sf1EAUn92eAnyb+yQFRmJaryquTdp9Fqeu5nDQZWLtIeQ9dzOGgysXaQadYrRXF+4H7gZXOBYsmlR6sn1HdSm5pUeq/6juoF6baqUQBASF0VADKiIWzE306U6y6jn0tyqbaRr5NT2+KYtvk0dtu1T+R0BpxmsdWdPbSi0BpFzP03IFBhx5VUqquJxqLKP6nE1ITcTjUX139lxNibC91amteVGj1KAAcVWo9sx9WL60nUhFCwLqXMZVIxHq9qMVVTaKm/b0oc69WHnJs6PVGXNJM1dUWmwocIMdaJ836qIAl96sPOTZ0fcL1Yecmzou4S4KfwlA/XUoiWTB6rMm3qOJerDzs2dF3HdY2xETkRE+B7Y0hQafSocZrbjmQAitTsjma57UbFY1zmpax1tiLZhHpzgLVEgUZ8adxzeq712eAnyTuogKnXqdkE0rXMc2NGvRWrYx1ti8m+chRp7gTiV5QaO+Cwh/OfRanruZw0GVi7SHkPXczhoMrF2kG3WKari/cD9wMrnAsQ1qPVf9R3UpufnP6r8m7qBem2qlUAQEhdFQKAoiRXTRcHFkmdhD9D86Lg4skzsIfoMLnsT3j/AD5oFABeFU9w+NRZR/U4m5CricbjyknZcTUmwrFqa15UaPVAAOKrQAAlQAAkQAAhEK8ruElyj+0pYaFeV3CS5STtKNReZXFU/wDf+PVfgam1hgaVysHruZw0GVi7SHkPXczhoMrF2kEdYhXF+4H7gZXOBYsmlR6smTd1G5pIlrXInpVFRPgC9NtVKIDuXq12Lf3oNYXq12Lf3odYemt7fdHzjdYXDCncvVrsWXSw6wvVrsW/vQawXQSX5R843WFZtFwcWSZ8tD9DSmYrWMavpa1iKn80REU/QZWFf7x/nzWAAC8KqbicbjyknZcTUhVxONx5STsuJqTYVi1Na8qNHqgAHFVribKqh8bIlje5iq9UVWuVLUs/kRrdKfnpdK8kGzH1IPrr2SKDD7VoqBDaYAJHT5r17pT89LpXjdKfnpdK88loPKmeyZkjUF690p+el0ryd0blWOJVW1ViYqqvpVdqhXRYtDwcWSj7KDkO1VdaNAa2Q5z5L9TzPudAtqrDCqrvqqxttVfgekDslThzhYZLkXaoYWQSubFE1yIljmsaipvp7UIWpPLv8Xm+qnaQgajD7VfVa4uhGfSsHruZw0GVi7SHkPVc9yNliVy2I2SNVVfQiI5LVG3WKxVx/uDnLd+jxqD2/wC6wxu/R41BpWDK58IEXIOo7l0gc7d+jxqDSsN4rtUr1a1lTC5zlRrWpK1Vc5fQiAj2EXJOor3AGQTSwDJgEIZOdLd6kYrmuqI2uaqtc1XJaiotioa3xUeMx/EFIvSPkO1HcukDm3xUeMx/ExfFRYzHnAi9I+Q7Udyry4nG48pJ2XE1IPcmZrKmN7nI1iPequVbERFR3eSzdan56LPQmQiJLSVoxzooIBs9V7AePdWn56LPQbq0/PRZ6Dkwqz2T8k6ivW9iO9ZEdZyoimPEMwGZre48u61Pz0Weg3Vp+eiz0DEvQZEFgO1erxDMBma3uHiGYDM1vceXdan56LPQbrU/PRZ6BiRcxeg7V6fEMwGZrT9E6t48W61Pz0Weg3Wp+eiz0CYSGHENoOor2A8e6tPz0Weg3Vp+eiz0CYSeyfknUVpd/i831W9aEDUmd2bowvglayWNzlRLGo5FVf4kIYoy+1XlWtLYRBHOsAA8KwW1otNQCWa2tPdcLjNJ7xB20Oee25MzYp4JH7zI5onuWxVsaj0Vd4DYm4wLobgOcHyVwIDh330PPO0EvcYvwouedoZdUZkVhhQqRm3aiu6EOFfhRc87Qy6pm++i552gl1QkUGhUiXJu1FV3dfjFV7xUfMU8dp67pyI+ad7Fta+aZ7VsVLWq9VRbF/keMeBW5hAiG0HoHks2mABV7WxgwBEs1kGACJrIMAETWQYAImsgwARNZBgAia2NQAQgABIgMmBUqGyKagJIWbRaACEtFoAIWbTUAEIAZBCwAZBCwDIBCwDY1EQgABIgBkELAMgELAMmAQh3Nhtzo6ysp6ee1YpFm2yNdtXfwwvcm/0tQ4ZJ/Bv9I0v2j8vIQa0iOh0KM9hkQxxBHMQ0lOwQDEaD0jzVh+bi52BNplHm5udgTaZSXA4th6s+sP7xWkvSBkBRHzc3OwJtMo83NzsCbTKS4Bh6s+sP7xRekDIGpRHzc3OwJtMo83NzsCbTKS4Bh6s+sP7xRekDIGpRHzc3OwJtMo83NzsCbTKS4Bh6s+sP7xRekDIGpRHzc3OwJtMo83NzsCbTEuAYerPrD+8UXpAyBqUR83NzsCbTqF8HNzsGbl4ZfSS4wv6KKK9rPrD+8UhokDIGpfPdw6Rk9TDDJbtJJNo6xbFstX2k/vHosGTSEH2L8epsr+qltm44W1hSqNSITYMRzQWzMjLHO1M1PRoMWG8xGgkHn0KOXj0WDJpF7heRRYMmkJIDJ4brHPv7xVxeFFzbdSrXZrcOCiSDxG2Txiy7bbv23q7Wyz4kTJ94TvRSdM//AKEBOocHY8SPV0OJFcXON1MnGfeKylZw2w6S5rBICWL+AgALpQFsWRQbDKOSKF7vG7aSON62StRNsrUVbN7+ZWvJ0oXXcngKfIQfLQyPC6m0iiw4JgPLSSZyMp4grqpoEOM94iNBkBbpXEvGov8Am0jdUXjUX/NpG6pJgYbD1ZZ9+tX2D6LmxqUOursOpIYKiVnjdvFDLI22RFTbNYqpaln8ivFLku/xWr93qPluKaX29Km74JUykUqBFdHeXEESnjliVBXMCHCewQ2gTBs0rBJ/Bv8ASNL9p/LyEYJP4N/pGl+0/l5C7rj8vpHYf8pVXA5VukeavEAHA1q0IZ4QNlFRc11O2nSJUmbI53jY3O32qiJZYqcqkzKv8MfCUWTm7TTQcF6NCpNZshRmhzSHYjZiaSolOc5kElpkcXmud5za/Ap9C7XHnNr8Cn0LtchAOq8Xaq6uzuqjvuPllTfzm1+BT6F2uPObX4FPoXa5CAHF2qurs1IvyPllTfzm1+BT6F2uPObX4FPoXa5CAHF2qurs1IvyPllTfzm1+BT6F2uY85ldg02hdrkJAcXaq6uzUi+4+WV2diq21lKvLKi9ZbhUexPjlLlW9SluGO4bfFQuz/YrQVFyL9PoFkAGLV2oN4TvRSdM/UwgRPfCd6KTpn6mECOvcF/yuF/6+YrG1t8W/wDj5QgAL9VqcnShddyeAp8hB8tClOTpQuu5PAU+Qp/loYfhxyUDSfIK/qD34mgea9YAOdLSrn3f4rV+71Hy3FNL7elS5bv8Vq/d6j5biml9vSp0jgR8PG7Q8lma+5Rmg+awSfwb/SNL9p/LyEYJP4N/pGl+0/l5DSVx+X0jsP8AlKpoHKt0jzV4gA4GtWhW/haopZpKPxUUkqNZLasbJHI1ds2y2xN4sgyWVU1k6rqU2ktaHFs8RMhjBHQelM0iD7ZhZOS+dtxarFZ/u8vcNxqnFZ/u8vcfRINdx/j5hveO5QMFMyjq+q+dtxqnFZ/u8vcNxqnFZ/u8vcfRID8QI+Yb3juRgpmUdX1XztuNU4rP93l7huNU4rP93l7j6JAfiBHzDe8dyMFMyjq+q+dtxarFZ/u8vcNxarFp/u8uqfRJr/8AQcf4+Yb3juQaqblHUqA2KJ/rKXKp1KW6VJsX49TZX9VLbHOG3xULs/2Km1FyT9PosgAxavFCfCVGrkpNqm235/QiqibzCDeTPwFzFLuM2mtqzhU6g0ZtHEIOuZ47qVpJsl+qp6VVApEUxC+U5c3QJdIVIeTPwFzFHkz8BcxS77RaWHHh+YHf+1R8ANzmz6qkPJn4D/Z/2qXJcpP+hT5GHsIeu3/LQUddV+6s2MYYdzckm2c5jQFPoNXCiucQ6c5c0vVAAZ5WK593+K1fu9R8txTS+3pUuW7/ABWr93qPluKaX29KnSOBHw8btDyWZr7lGaD5rBJ/Bv8ASNL9p/LyEYJP4N/pGl+0/l5DSVx+X0jsP+Uqmgcq3SPNXiADga1aGj5Gt9ZzW24aolpuVl4ZU/iofq1XXGWdTVfhCmMo11c3U8cp2AmyY6ExSIxhQy8CclZHlDMNmkaPKGYbNI0+bfj8VHx+Km2/D9vWD3PuVbhU5A1/RfSXlDMNmkaPKGYbNI0+bfj8VFno9PpT2qKP+P2z+IPc+5GFTkDX9F9LoDzXO4KDJQ9hD0nN3i5cR0K4BmJoYX9FMmF/RwgSmxUFsX49TZX9VLds/wAsKNe6xVs3rNtvoti+k28pfhvzlOzV5weNZxWRBEubkS92c+fpCpKDWV6Nc25nMztl6K8NqvJ+A2q8n4FH+Uvw35yjyl+G/OUpOI7s/wCD7lO4wft+L6K8NqvJ+A2q8n4FH+Uvw35yjyl+G/OUOI7s/wCD7kcYP2/F9FeG1Xk/AbVeT8Cj/KX4b85R5S/DfnKHEd2f8H3I4wft+L6K8NqvJ+BqUilS/DfnqW7seW2mpFXfXxEW+vpX+Ep664PGrITYhiXV0Ze7Lmn0lTqBWV9uLbmUhO2fPoXSABm1Zrn7IOK1fu9R8txTXepct3+K1fu9R8txTS+3pU6RwI+HjdoeSzNfcozQfNYJP4N/pGl+0/l5CMEn8G/0jS/afy8hpK4/L6R2H/KVTwOVbpHmrxABwNatCC+Ey4NVXLSLSwul8UlQj9q+Nu12yss9ZU5F+BOgTqtrCJQKS2kQwCWzxGcsYI5pdKajQRFYWFUZeJdPFHaSDXF4t08UdpINcvMGp4+U7Ns8X+Sg4KhdJ2blRl4t08UdpINcJsEunvf6R3s/3INcvMAOHlOzbPF/kjBULpOzcvwomK2OJrksc2KJqpyKjURT9wDEuddEnpVkBISQwv6OMmOXoUAgr5rk9K/+XWaG8npXp/U0PowWLIIAASIAAQgABC2QuPY5xWkyEXZKbQuTY5xWkyEXZMZw3+Fhdr+qvai5V+j1XRABzVadc+7/ABWr93qPluKaX29Kly3f4rV+71Hy3FNL7elTpHAj4eN2h5LM19yjNBWCRbA6qOCup5ZntiiYs+2e9bGtthkalq9Kon9SOmTXUqAKRBfBcZB4LZi0TEsSo2OuXB3Qr+vqoMdg0iC+qgx2DSoUFaDGcQaHnX+HcrPCsTJG1X7fVQY7BpUF9VBjsGlQoK0WicQaHnn+HcjCsTJG1X7fVQY7BpUF9VBjsGlQoIWhxBoeef4dyMKxMkbVft9VBjsGlQX1UGOwaVCgrRaHEGh55/h3IwrEyRtV+31UGOwaVBfVQY7BpUKCFocQaHnn+HcjCsTJG1X7fVQY7BpUMrsqoMcg0iFAi0XiDQ86/wAO5GFYmSNqzL6V/wA9poZMG6VUgABCAAEIAAQtkLUuFdmlZTUzH1MLHthja5rpWo5rkbvoqFUm1pVVvVEOsobYcRxaGmeKXRLnU2hU11FcXNAM8WPXzK493aTGoNM3vG7tJjUGmb3lOW9It6TP8SKNnn6mqxw9FyBt3q1rt3apX09U1lTC574J2ta2Vquc5Y1REROUqhf1M2mpfVPVEOrGPYxxddGeOWjmVdTaa6lODnACWLEv/9k=" alt="react"/></div>
                <div className="exp-logo"><img className="arduino-logo" id="exp-logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAclBMVEX///8Al5wAk5gAlJkAkZfB4+Sey84Aj5UAmJ284eLZ8PA7o6f8///4/f3y+vqy19mWztBvtrqo19no9fbJ5ufg8fHt+Pig0dNBqq7i8PGHx8mPy83a6+xTsLSt2dt7wcRsvL8mo6dZtLdKrbFzv8JMsLOC16/gAAAKQklEQVR4nO2c65aqOgyApa0HUFBHQBTlprPf/xUPoDIqSa/sWeucle/X3o4Y0kuapGkXC4IgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIgCIIg/tdsksuy2p2Loi6K826/PCWbvy7xlFb5KDG8ZH9NYrzaN7fWE4IJ9kQIr701+/TvCI1X2+9bG7zKe0jM55eY7W8BY5x7UzhnzDvuVjNLXOXl70nMzq3PIFEvQpkf1Gk8l8RLoZYo/KCYRcm4ujEhlfWEifaczSAx2R0F05R4zA+O0jbnVsjb8g3By9RR4qrxFL33ChdBnbjoV2h236vINnSQmN5MGnSAicZWxzjnpvoNOvpH235clb6pfoOOrLAyrWFro9+go7jamIBDYzA8P3QMKmNxm6u1uF5HVhhLzD098wIjboZDNXQS10tszYZqdrQdMQ842xuIi2rfTVwvUZh0Yw4u7Wb4V+31ODk6duAd/YETXx078A5rNad/Gri35wD39Ebqqp2lRXt3TmulCt1H6IivMzdCF5v2KTBXy9vP1J53RK0UmM8yQkeB59+V182NRiHwPLNAlYHLZxyid9gfqcDvmRVUqbhVyutiM9EFwTwIvP4fGkaelZGbgpyLHzTmrNjh8lLF412Mvf46V+npdEqS5HQJd/XNUwVynsBVVCrYR4FlvauWl9PpckmrXV0Gvso5Z1tMXhLI1QuK5XRNjVb5TdGy4mqnYCexqZLP5omSquFyJRmyLkYt/lgfh+HLabz9w2UmmMEq1jIFOQtqfDlNm0Bq8+FYo8EFinaviE+yQiYSXDRkZpuLMpT7YIedJPbhN+iRCn2gi00kxmIUeZb0IzD7t/i3u/BLwxuK9wH+yoC1QSehfiS0afDpIT79qVTSHEdN/zKqUQMATMUS+a44GmSX0jX63uJdZILqxwKDMGiFCeTt51dDpMONYqCuVQt04ASvWbFojTW+uBqFshFmrj7nRYSMUWacHVh6yLu/zX7UqgkN1/kdzH6w9zxjAXY2DyzyLQcsunwxqJgZ5YFFEiuF2/TdI4ZnBQ+sErwRFs+O1gazMqy1ygtmcEDLX7sH7EJLBRf4Us7vCsRIiM2OlvstK3CO8Rc/4wC/j/2uAKIiPw5/beAuZEfrfQ/YoRaX8QtnSCRzyV0jJnWwzIhpYDcNtwIDDIp4M/4d6mTDZeITpBdZujjBZpsdneSBTSqe5hRaC7mbQGw94G10hM1C67g1By2wo+8GyeTO+2QlvADBAQxvXXfJMuh3H45NAmmvTOgoiSXB2BSJVct3T9DItgcyJuK+vObAn1zHTA/uek7fZCn5nfWYwviSiYvb6e+yu5MB+NzCZA8AJdXNMsmznOvn17hUw8UeELfu7TMUNgVzKKidmlRkHHU1hJYE1i+JgCVlxu4vwlUnvczX8imhrSHQoIM1BTw2Ple1SizNbT3fQmG2tTXcAK3XdJ/fJtPwxRdwRWMqKue8toaLZmpR2mgRAzJd/LUPdioVlWl/Aw2XU2H+YZFN8/iB6+r7ynSIfDSyckboa3iYTopuHZoaGl7OpNwA6Gu8vYEKfQ2BhU/kwHoP5tYu/6jApELL1M8LoLtv4ZNl+9P0y/FD+KHpnOD1op6qDT2+9ZkCtGFhV/tOgEVMsT96Mi+v+/zIX8NPTZMH/M/iOjWlkI8oyd8+QDWUjFN8jEbSwc0RDbPpV9vFcfIZaGgcNMTtqcSO2mkIrL/BogU+m1lDdJwGuB210xDQxgOcuUm22FnDFfy6QhINWWoItOVUQ/hpJw0Bc+a958J+V0Mwf+GkYdSCT7SS1JOlhn+0NJx/lCJpKVkpiqWGgAf1G/MQmYbdi17QZ2Ycpe3kI9iW+kIBriG65EsSerE/Cnz5/rjig72wgG2p5nqYhEsF6MtKIn1JYcjqSfbjl16z8UOkXaD1UNOnsecgG99CI4wx8LxBn6bQ80vtmTbha2vKVowHBhoCfmkJjCGLCmYJ2ObyszmlOdABAw2B2KKA4kOwVMMSICh9Rx1uG2g4HS9dfHgCYnyXMxofIDtpPyDVRC/oa7gBY3zA/GiMHF0UY3SQpsoK6WsI5L16Uwbk2jSmvx4Hna0Lrhin+hp+g7k2KF+qY8O1wIp03jVU5IW0NQTShsMjQAoOqpmyAdrzAVBUmKyfJyx9hYZARmjI3kPWbp59C9QfnbS01Meo9k8UC3U7/eV7Ph0YSrN0YqR9qoEHc5zNBOoDHh46VJ0vyS9oA2/LwNU9M9g2qDz24byAe8Dufg2cf2JwOZGsNlsTaNI/KxShxDtWSKxNCPdgiaT5maszDO44P4MswJoikb4+SJK0c5cA/38Q6FgaAXbTaKQh35F9u8g7wFZmKHWE1xDuNvXBOcHGn9xBMl328pE6jIezBPsBqq1gKUuwquunkwCP1XOZGjFSgPnYW0Z8OXa0LvtawT/4MvDBwjaPW55YxhQcfWzkaI61igk4J972DJAozk5FTEH2kzxEjpJYqggr+GG7wJnYfclioB6Q85KvlWtYvZRVhSlSQPtpKpFQ3LzseoX5am9NirmsFnXX2AHNz4ISaE0cVGzMLFyFhRP+e038HjkGyLlh+I2dX5z2DZZuYCbHzmP0rJb4XF7R9Ib4NpiM2Q0ruJ4mm9GUEReN7uRI0bNPU4FInWnfpvoF+2f0/CMUkMGO5CCS5zo6Zlf0ACoHMmqIBezxGx0XLtri555g+1FLTiIFZ1X+Lasl50lBP15SL8V4rdIxrtaS55FoTFbyyryrbOVYXmUHEH04nbaVHDpmXHp8bVUHkkOWaESNuMvPxwS7VtnUCkSb8Ft+phNdcqTVmVx4NXgGMV4WgfwSIjwrArt3L0+y56ncy6nnku6Lr1YozuVKjsjj58AGmBDrTtylP3WcJJ3AtDp/rZnqjiXZOQpsVXzV8rGJd7/MjKnPVksvHZAelB3FdUKCx7/g27/eBUqDItnUsEM0Mnmyo7mW+IoifNQnsUR5bYSyF00FKk8ZqDfsjeSpM1qKuWgqUMORTn/5qpFZb6rwtU6EItGIOZrXxSzCGS4YugvUvBBncSh/98qfua7gEWv9fF0xw8ARpX40u5mjTX2ToEQSJWjCDbPYO+O79j4FmgaW8beTwRFr01g9kzjSGvqJq3n+I7UXybjN0bez/R1xlpc1RpJ7J2RwvfBuyulqdW2iJ7ydbTI52pk3KxfWF0N2RvVoPh0Ft7sX8kGcG91f6jHmeIFpWjKTRuWiPbtudUZhF9zqKclF8I3XU+oiD27f5DFRbme52TfJj8q7mfrKyLKa58zbprqpYs4hVm13c1zq++CwvQYcuw6qa0seNNs5L6DebBuFvGs1Y+XWgywsynVwj0OfBSDdf4J1WYdz33Xdswrrsr/Ne5Q1yOO9vO3fkHcnOpwuyyrP+2vZi/M536anZLY7rgHi543svbRdvl9eTgeHqxcIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgvgv8C8KQpMUmv7j+wAAAABJRU5ErkJggg==" alt="react"/></div>
                <div className="exp-logo"><img className="react-logo" id="exp-logo" src="https://allpcworld.com/wp-content/uploads/2020/12/Download-MediBang-Paint-Pro-26.jpg" alt="react"/></div>
            </div>

    </div>

}

export default CV;