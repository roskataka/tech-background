import { useState } from 'react';
import { HashRouter, Routes, Route } from 'react-router-dom'
import './App.css';
import CV from './CV';
import Gallery from './Gallery';
import HomePage from './HomePage';

function App() {
  const rememberedLanguage = localStorage.getItem('language');
  const [language, changeLanguage] = useState(rememberedLanguage==='true');
  if(rememberedLanguage==null){
    localStorage.setItem('language', true)
    changeLanguage(true);
  };
  const langButtonPressed = event =>{
      event.preventDefault();
      localStorage.setItem('language', !language);
      changeLanguage(!language)
  }
  return (
        <>
        <HashRouter hashType="noslash">
            <button id={language?"eng":"bg"}className="language-changer" type="button" onClick={langButtonPressed}><p className="language-change-content">{language?"Bulgarian":"English"}</p></button>
            <Routes>
              <Route path="/" element={HomePage(language)}/>
              <Route exact path="/about" element={CV(language)}/>
              <Route exact path="/gallery" element={Gallery(language)}/>
              <Route exact path="/gallery/:category/:query" element={Gallery(language)}/>
              <Route exact path="/gallery/:category/:query/:id" element={Gallery(language)}/>
            </Routes>
        </HashRouter>

        </>
  );
}

export default App;
