import React, { useEffect, useState } from "react";
import Header from "./components/Header";

export const VideosGrid = (props) => {  // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {obj, videoId} = props;
    const id = obj?(obj.link?(obj.link.split('/')[4].split('?')[0]):undefined):null;           
    const bool = window.localStorage.getItem('language');
    const favorites = (localStorage.favoriteVideos)?localStorage.favoriteVideos.split(';').filter(a => a!==''):false;
    const isInFavorites = obj?(favorites?favorites.includes(`${obj.idvideos}`):false):false;                                                    //obj
    const [data, setData] = useState("");
                                          

    useEffect(()=>{
        if(videoId!=="" && videoId!==undefined){
        fetch(`https://ren-portfolio-backend.herokuapp.com/videos/id/${videoId}`,
        {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        }
        )
        .then(response => response.json())
        .then(result => {
            const objToDisplay = result[0]
            setData(objToDisplay)
        })
        .catch()
        }
    },[videoId])
    const [fav, toggleFav] =useState(isInFavorites);
    const [heart, toggleHeart] = useState(true);
    const heartEvent = event => {
        event.preventDefault();
        toggleHeart(!heart);
    }
    const toggleFavouriteFunc = event => {
        event.preventDefault();
        if(favorites){
            if(isInFavorites){
                const storageIndex = favorites.indexOf(`${obj.idvideos}`);
                const deep = [...favorites];
                deep.splice(storageIndex, 1);
                localStorage.favoriteVideos = deep.join(';') ; 
            } else {
                const deep = [...favorites];
                deep.push(`${obj.idvideos}`);
                localStorage.favoriteVideos = deep.join(';');
            }
            toggleFav(!fav);
            toggleHeart(!heart)
        } else {
            localStorage.setItem('favoriteVideos', `${obj.idvideos};`)
        }
    } 
    const removeFromFavs = event => {
        const storageIndex = favorites.indexOf(`${data.idvideos}`);
        const deep = [...favorites];
        deep.splice(storageIndex, 1);
        localStorage.favoriteVideos = deep.join(';') ;
        window.location.reload();
    }
    const VideoRender = () => {
        return<>
            {obj.idvideos?
            <>
                <div key={obj.idvideos} className="single-video-grid">
                    <img onClick={openVideo} className="video-thumbnail" name="single" src={`https://img.youtube.com/vi/${id}/0.jpg`} alt="lol"/>
                    <button onClick={openVideo} className="video-button" name="zoom"></button>

                    <div key={obj.videos} className="favorite-button-placeholder"><button onClick={toggleFavouriteFunc} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button" href={`../${obj.idvideos}`}>{result}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }

    const openVideo = event => {
        event.preventDefault()
        const {name} = event.target;
        if(name==="zoom"){
            const loc = window.location.hash
            if(loc.includes('id')){
                let paramsArray = loc.split('/');
                paramsArray.pop();
                let loc2 = paramsArray.join('/')
                let result = `${loc2}/id=${obj?(obj.idvideos):(data.idvideos)}`;
                window.location.hash=result;
                window.location.reload()
            } else {
                const result =  `${loc}/id=${obj?(obj.idvideos):(data.idvideos)}`
                window.location.hash = result;
                window.location.reload()
            }


        } else {
            
        }
    }

    const FavVideoRender = () => {
        const id2 = typeof data === 'object'?data.link.split('/')[4].split('?')[0]:null;                                                                      
        const isInFavorites2 = typeof data === 'object'?(favorites?favorites.includes(`${data.idvideos}`):false):null;
        const result2 = bool==="true"?(!isInFavorites2?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites2?"Добавете в любими ♡":"Махнете от любими ♥");

        return <>{data.idvideos?<>
        <img onClick={openVideo} className="video-thumbnail" name="single" src={`https://img.youtube.com/vi/${id2}/0.jpg`} alt="lol"/>
        <button onClick={openVideo} className="video-button" name="zoom"></button>
        <div key={data.idvideos + 'fav-video-button'} className="favorite-button-placeholder"><button onClick={removeFromFavs} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button" href={`../${data.idvideos}`}>{result2}</button></div>
        </>
        :
        <>
        </>
        }</>
    }
    const result = bool==="true"?(!isInFavorites?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites?`Добавете в любими ${heart?"♡":"♥"}`:"Махнете от любими ❤");
    if(obj){
    return <VideoRender/>
    } else {
        return <div key={videoId} style={{display:videoId===""?"none":"block"}} className="single-video-grid">
            <FavVideoRender/>
    </div>
    }
}
export const PicturesGrid = (props) => {  // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {obj, pictureId} = props;
    const bool = window.localStorage.getItem('language');
    const favorites = (localStorage.favoritePictures)?localStorage.favoritePictures.split(';').filter(a => a!==''):false;
    const isInFavorites = obj?(favorites?favorites.includes(`${obj.idpictures}`):false):null;                                                    //obj
    const [fav, toggleFav] =useState(isInFavorites);
    const [heart, toggleHeart] = useState(true);
    const [data, setData] = useState("");

    useEffect(()=>{
        if(pictureId!=="" && pictureId!==undefined){
        fetch(`https://ren-portfolio-backend.herokuapp.com/pictures/id/${pictureId}`,
        {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        }
        )
        .then(response => response.json())
        .then(result => {
            const objToDisplay = result[0]
            setData(objToDisplay)
        })
        .catch()
        }
    },[pictureId])
    const heartEvent = event => {
        event.preventDefault();
        toggleHeart(!heart);
    }
    const toggleFavouriteFunc = event => {
        event.preventDefault();
        if(favorites){
            if(isInFavorites){
                const storageIndex = favorites.indexOf(`${obj.idpictures}`);
                const deep = [...favorites];
                deep.splice(storageIndex, 1);
                localStorage.favoritePictures = deep.join(';') ; 
            } else {
                const deep = [...favorites];
                deep.push(`${obj.idpictures}`);
                localStorage.favoritePictures = deep.join(';');
            }
            toggleFav(!fav);
            toggleHeart(!heart)
        } else {
            localStorage.setItem('favoritePictures', `${obj.idpictures};`)
        }
    } 
    const removeFromFavs = event => {
        const storageIndex = favorites.indexOf(`${data.idpictures}`);
        const deep = [...favorites];
        deep.splice(storageIndex, 1);
        localStorage.favoritePictures = deep.join(';') ; 
        window.location.reload();
    }
    const openPicture = event => {
        event.preventDefault()
        const {title} = event.target;
        if(title==="zoom"){
            const loc = window.location.hash
            if(loc.includes('id')){
                let paramsArray = loc.split('/');
                paramsArray.pop();
                let loc2 = paramsArray.join('/')
                let result = `${loc2}/id=${obj?(obj.idpictures):(data.idpictures)}`;
                window.location.hash=result;
                window.location.reload()
            } else {
                const result =  `${loc}/id=${obj?(obj.idpictures):(data.idpictures)}`
                window.location.hash = result;
                window.location.reload()
            }


        } else {
            
        }
    }

    const PictureRender = () => {
        return<>
            {obj.idpictures?
            <>
                <div key={obj.idpictures} className="single-pic-grid">
                    <div className="pic-box">
                        <img className="pic-thumbnail" title="zoom" onClick={openPicture}  name={obj.idpictures} src={obj.link} alt="lol"/>
                    </div>
                    <div key={obj.idpictures + "fav-button"} className="favorite-button-placeholder-pic"><button onClick={toggleFavouriteFunc} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-pic" href={`../${obj.idpictures}`}>{result}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }
    const FavPictureRender =()=>{
        const isInFavorites2 = typeof data === 'object'?(favorites?favorites.includes(`${data.idpictures}`):false):null;
        const result2 = bool==="true"?(!isInFavorites2?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites2?"Добавете в любими ♡":"Махнете от любими ♥");

        return<>
            {data.idpictures?
            <>
                <div key={data.idpictures +"single-pic"} className="single-pic-grid">
                    <div className="pic-box">
                        <img className="pic-thumbnail" title="zoom" onClick={openPicture} name={data.idpictures} src={data.link} alt="lol"/>
                    </div>
                    <div key={data.idpictures + "fav-button"} className="favorite-button-placeholder-pic"><button onClick={removeFromFavs} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-pic" href={`../${data.idpictures}`}>{result2}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }
    const result = bool==="true"?(!isInFavorites?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites?`Добавете в любими ${heart?"♡":"♥"}`:"Махнете от любими ❤");

    if(obj){
        return <PictureRender/>
        } else {
            return <div key={pictureId} style={{display:pictureId===""?"none":"block"}} className="single-video-grid">
                <FavPictureRender/>
        </div>
        }
}

export const DrawingsGrid = (props) => {
    const {obj, drawingId} = props;
    const bool = window.localStorage.getItem('language');
    const favorites = (localStorage.favoriteDrawings)?localStorage.favoriteDrawings.split(';').filter(a => a!==''):false;
    const isInFavorites = obj?(favorites?favorites.includes(`${obj.iddrawings}`):false):null;                                                    //obj
    const [fav, toggleFav] =useState(isInFavorites);
    const [heart, toggleHeart] = useState(true);
    const [data, setData] = useState("");

    useEffect(()=>{
        if(drawingId!=="" && drawingId!==undefined){
        fetch(`https://ren-portfolio-backend.herokuapp.com/drawings/id/${drawingId}`,
        {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        }
        )
        .then(response => response.json())
        .then(result => {
            const objToDisplay = result[0]
            setData(objToDisplay)
        })
        .catch()
        }
    },[drawingId])
    const heartEvent = event => {
        event.preventDefault();
        toggleHeart(!heart);
    }
    const openDrawing = event => {
        event.preventDefault()
        const {title} = event.target;
        if(title==="zoom"){
            const loc = window.location.hash
            if(loc.includes('id')){
                let paramsArray = loc.split('/');
                paramsArray.pop();
                let loc2 = paramsArray.join('/')
                let result = `${loc2}/id=${obj?(obj.iddrawings):(data.iddrawings)}`;
                window.location.hash=result;
                window.location.reload()
            } else {
                const result =  `${loc}/id=${obj?(obj.iddrawings):(data.iddrawings)}`
                window.location.hash = result;
                window.location.reload()
            }


        } else {
            
        }
    }
    const toggleFavouriteFunc = event => {
        event.preventDefault();
        if(favorites){
            if(isInFavorites){
                const storageIndex = favorites.indexOf(`${obj.iddrawings}`);
                const deep = [...favorites];
                deep.splice(storageIndex, 1);
                localStorage.favoriteDrawings = deep.join(';') ; 
            } else {
                const deep = [...favorites];
                deep.push(`${obj.iddrawings}`);
                localStorage.favoriteDrawings = deep.join(';');
            }
            toggleFav(!fav);
            toggleHeart(!heart)
        } else {
            localStorage.setItem('favoriteDrawings', `${obj.iddrawings};`)
        }
    } 
    const removeFromFavs = event => {
        const storageIndex = favorites.indexOf(`${data.iddrawings}`);
        const deep = [...favorites];
        deep.splice(storageIndex, 1);
        localStorage.favoriteDrawings = deep.join(';') ; 
        window.location.reload();
    }

    const PictureRender = () => {
        return<>
            {obj.iddrawings?
            <>
                <div key={obj.iddrawings} className="single-drawing-grid">
                    <div className="drawing-box">
                        <img title="zoom" onClick={openDrawing} className="drawing-thumbnail" name={obj.iddrawings} src={obj.link} alt="lol"/>
                    </div>
                    <div key={obj.idpictures + "fav-button"} className="favorite-button-placeholder-drawing"><button onClick={toggleFavouriteFunc} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-drawing" href={`../${obj.iddrawings}`}>{result}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }
    const FavPictureRender =()=>{
        const isInFavorites2 = typeof data === 'object'?(favorites?favorites.includes(`${data.iddrawings}`):false):null;
        const result2 = bool==="true"?(!isInFavorites2?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites2?"Добавете в любими ♡":"Махнете от любими ♥");

        return<>
            {data.iddrawings?
            <>
                <div key={data.iddrawings +"single-drawing"} className="single-drawing-grid">
                    <div className="drawing-box">
                        <img title="zoom" onClick={openDrawing} className="drawing-thumbnail" name={data.iddrawings} src={data.link} alt="lol"/>
                    </div>
                    <div key={data.iddrawings + "fav-button"} className="favorite-button-placeholder-drawing"><button onClick={removeFromFavs} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-drawing" href={`../${data.iddrawings}`}>{result2}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }
    const result = bool==="true"?(!isInFavorites?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites?`Добавете в любими ${heart?"♡":"♥"}`:"Махнете от любими ❤");

    if(obj){
        return <PictureRender/>
        } else {
            return <div key={drawingId} style={{display:drawingId===""?"none":"block"}} className="single-drawing-grid">
                <FavPictureRender/>
        </div>
        }
}
export const MangasGrid = (props) => { // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {obj, idManga} = props;
    const bool = window.localStorage.getItem('language');
    const favorites = (localStorage.favoriteMangas)?localStorage.favoriteMangas.split(';').filter(a => a!==''):false;
    const isInFavorites = obj?(favorites?favorites.includes(`${obj.mangaid}`):false):null;                                                    //obj
    const [fav, toggleFav] =useState(isInFavorites);
    const [heart, toggleHeart] = useState(true);
    const [data, setData] = useState("");
    useEffect(()=>{
        if(idManga!=="" && idManga!==undefined){
        fetch(`https://ren-portfolio-backend.herokuapp.com/mangas/icon/${idManga}`,
        {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        }
        )
        .then(response => response.json())
        .then(result => {
            const objToDisplay = result[0]
            setData(objToDisplay)
        })
        .catch()
        }
    },[idManga])

    const heartEvent = event => {
        event.preventDefault();
        toggleHeart(!heart);
    }
    const toggleFavouriteFunc = event => {
        event.preventDefault();
        if(favorites){
            if(isInFavorites){
                const storageIndex = favorites.indexOf(`${obj.mangaid}`);
                const deep = [...favorites];
                deep.splice(storageIndex, 1);
                localStorage.favoriteMangas = deep.join(';') ; 
            } else {
                const deep = [...favorites];
                deep.push(`${obj.mangaid}`);
                localStorage.favoriteMangas = deep.join(';');
            }
            toggleFav(!fav);
            toggleHeart(!heart)
        } else {
            localStorage.setItem('favoriteMangas', `${obj.mangaid};`)
        }
    } 
    const removeFromFavs = event => {
        event.preventDefault()
        const storageIndex = favorites.indexOf(`${data.mangaid}`);
        const deep = [...favorites];
        deep.splice(storageIndex, 1);
        localStorage.favoriteMangas = deep.join(';') ; 
    }
    const openManga = event => {
        event.preventDefault()
        const {title} = event.target;
        if(title==="zoom"){
            const loc = window.location.hash
            if(loc.includes('id')){
                let paramsArray = loc.split('/');
                paramsArray.pop();
                let loc2 = paramsArray.join('/')
                let result = `${loc2}/id=${obj?(obj.mangaid):(data.mangaid)}`;
                window.location.hash=result;
                window.location.reload()
            } else {
                const result =  `${loc}/id=${obj?(obj.mangaid):(data.mangaid)}`
                window.location.hash = result;
                window.location.reload()
            }


        } else {
            
        }
    }
    const MangaRender = () => {
        return<>
            {obj.mangaid?
            <>

                        <div key={obj.mangaid} className="single-manga-grid">

                        <div className="manga-box">                        
                             <div className="img-and-popup-store">
                            <img className="manga-thumbnail" name={obj.mangaid} title="zoom" onClick={openManga} src={obj.link} alt="lol"/>
                                <div className="manga-popup">
                                    <div className="popup-information">
                                        <div className="manga-popup-title-placeholder"><h2 className="manga-popup-title">{obj.language==="Bulgarian"?obj.bulgTitle:obj.title}</h2></div>
                                        <div className="manga-popup-description-placeholder"><p className="manga-popup-description">{obj.description}</p></div>
                                    </div>
                                </div>
                             </div>
                        </div>
                        <div key={obj.idpictures + "fav-button"} className="favorite-button-placeholder-manga"><button onClick={toggleFavouriteFunc} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-manga" href={`../${obj.mangaid}`}>{result}</button></div>
                        </div>


            </>
            :
            <>
            </>    
        }
        </>
    }

    const FavMangaRender =()=>{
        const isInFavorites2 = typeof data === 'object'?(favorites?favorites.includes(`${data.mangaid}`):false):null;
        const result2 = bool==="true"?(!isInFavorites2?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites2?"Добавете в любими ♡":"Махнете от любими ♥");

        return<>
            {data.mangaid?
            <>
                <div key={data.mangaid} className="single-manga-grid">
                        <div className="manga-box">                        
                                <div className="img-and-popup-store">
                                <img className="manga-thumbnail" name={data.mangaid} title="zoom" onClick={openManga} src={data.link} alt="lol"/>
                                        <div className="manga-popup">
                                                <div className="popup-information">
                                                        <div className="manga-popup-title-placeholder"><h2 className="manga-popup-title">{data.language==="Bulgarian"?data.bulgTitle:data.title}</h2></div>
                                                        <div className="manga-popup-description-placeholder"><p className="manga-popup-description">{data.description}</p></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                    <div key={data.idpictures + "fav-button"} className="favorite-button-placeholder-manga"><button onClick={removeFromFavs} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-manga" href={`../${data.mangaid}`}>{result2}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }
    const result = bool==="true"?(!isInFavorites?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites?`Добавете в любими ${heart?"♡":"♥"}`:"Махнете от любими ❤");

    if(obj){
        return <MangaRender/>
        } else {
            return <div key={idManga} style={{display:idManga===""?"none":"block"}} className="single-video-grid">
                <FavMangaRender/>
        </div>
        }
}
export const ThreeDGrid = (props) => {  // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {obj, photoId} = props;
    const bool = window.localStorage.getItem('language');
    const favorites = (localStorage.favoritePhotos)?localStorage.favoritePhotos.split(';').filter(a => a!==''):false;
    const isInFavorites = obj?(favorites?favorites.includes(`${obj.idphotos}`):false):null;                                                    //obj
    const [fav, toggleFav] =useState(isInFavorites);
    const [heart, toggleHeart] = useState(true);
    const [data, setData] = useState("");


    useEffect(()=>{
        if(photoId!=="" && photoId!==undefined){
        fetch(`https://ren-portfolio-backend.herokuapp.com/photos/id/${photoId}`,
        {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        }
        )
        .then(response => response.json())
        .then(result => {
            const objToDisplay = result[0]
            setData(objToDisplay)
        })
        .catch()
        }
    },[photoId])

    const heartEvent = event => {
        event.preventDefault();
        toggleHeart(!heart);
    }
    const toggleFavouriteFunc = event => {
        event.preventDefault();
        if(favorites){
            if(isInFavorites){
                const storageIndex = favorites.indexOf(`${obj.idphotos}`);
                const deep = [...favorites];
                deep.splice(storageIndex, 1);
                localStorage.favoritePhotos = deep.join(';') ; 
            } else {
                const deep = [...favorites];
                deep.push(`${obj.idphotos}`);
                localStorage.favoritePhotos = deep.join(';');
            }
            toggleFav(!fav);
            toggleHeart(!heart)
        } else {
            localStorage.setItem('favoritePhotos', `${obj.idphotos};`)
        }
    } 
    const removeFromFavs = event => {
        const storageIndex = favorites.indexOf(`${data.idphotos}`);
        const deep = [...favorites];
        deep.splice(storageIndex, 1);
        localStorage.favoritePhotos = deep.join(';') ; 
        window.location.reload()
    }

    const openModel = event => {
        event.preventDefault()
        const {title} = event.target;
        if(title==="zoom"){
            const loc = window.location.hash
            if(loc.includes('id')){
                let paramsArray = loc.split('/');
                paramsArray.pop();
                let loc2 = paramsArray.join('/')
                let result = `${loc2}/id=${obj?(obj.idphotos):(data.idphotos)}`;
                window.location.hash=result;
                window.location.reload()
            } else {
                const result =  `${loc}/id=${obj?(obj.idphotos):(data.idphotos)}`
                window.location.hash = result;
                window.location.reload()
            }


        } else {
            
        }
    }
 
    const PhotoRender = () => {
        return<>
            {obj.idphotos?
            <>
                <div key={obj.idphotos} className="single-photo-grid">
                    <div className="photo-box">
                        <img className="photo-thumbnail" title="zoom" onClick={openModel} name={obj.idphotos} src={obj.link} alt="lol"/>
                    </div>
                    <div key={obj.idphotos + "fav-button"} className="favorite-button-placeholder-photo"><button onClick={toggleFavouriteFunc} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-photo" href={`../${obj.idphotos}`}>{result}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }
    const result = bool==="true"?(!isInFavorites?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites?`Добавете в любими ${heart?"♡":"♥"}`:"Махнете от любими ❤");
   
    const FavPhotoRender = () => {
        const isInFavorites2 = typeof data === 'object'?(favorites?favorites.includes(`${data.idphotos}`):false):null;
        const result2 = bool==="true"?(!isInFavorites2?`Add to favorites ${heart?"♡":"♥"}`:`Remove from favorites ❤`):(!isInFavorites2?"Добавете в любими ♡":"Махнете от любими ♥");
        return<>{data.idphotos?<>
                <div key={data.idphotos +"single-photo-gr"} className="single-photo-grid">
                    <div className="photo-box">
                        <img className="photo-thumbnail" title="zoom" onClick={openModel} name={data.idphotos} src={data.link} alt="lol"/>
                    </div>
                    <div key={data.idphotos + "fav-button"} className="favorite-button-placeholder-photo"><button onClick={removeFromFavs} onMouseEnter={heartEvent} onMouseLeave={heartEvent} className="favorite-button-photo" href={`../${data.idphotos}`}>{result2}</button></div>
                </div>
            </>
            :
            <>
            </>    
        }
        </>
    }
    if(obj){
        return <PhotoRender/>
        } else {
            return <div key={photoId} style={{display:photoId===""?"none":"block"}} className="single-video-grid">
                <FavPhotoRender/>
        </div>
        }
}
export const AppsGrid = (props) => {
    const {obj} = props;
    return <div key={obj.idapps} className="single-video-grid">

    </div>
}
// -------------------------------------------------------------------------------------------------------------------
const ResultsBrowsers = (props) => {
    const {arr, category, bool} = props;
    if(arr.length===0 || category===""){
        return <Response/>
    }
    switch(category){
        case 'digital-art':
            return <Pictures arr={arr} bool={bool}/>
        case 'traditional-art':
            return <Drawings arr={arr} bool={bool}/>
        case '3dmodels':
            return <Photos arr={arr} bool={bool}/>
        case 'animations':
            return <Videos arr={arr} bool={bool}/>
        case 'manga':
            return <Mangas arr={arr} bool={bool}/>
        case 'apps-and-software':
            if(arr[0]?(!(arr[0].idapps)):false &&  (typeof arr[0]!=="string")){
                return <Loading/>
            }
            return <Apps arr={arr} bool={bool}/>
        default: 
            return <Response/>
    }
}
const Videos = (props) => { // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {arr, bool} = props;
    const x=Boolean(arr[0].idvideos);
    const y = typeof arr[0]==="number"
    if(bool && y){
        return  <>{arr.map(a => <VideosGrid videoId={a} obj={undefined} key={a+'videos-object'}/>)}</>
    } else if(x) {
        return <>{arr.map(a => <VideosGrid obj={a} videoId={undefined} key={a.idvideos+'videos-object'}/>)}</>
    } else {
        return <Loading/>
    }
}

const Pictures = (props) => { // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {arr, bool} = props;
    const x=Boolean(arr[0].idpictures);
    const y = typeof arr[0]==="number";
    if(bool && y){
        return <>{arr.map(a => <PicturesGrid obj={undefined} pictureId={a}  key={a+'pictures-object'}/>)}</>
    } else if(x) {
        return <>{arr.map(a => <PicturesGrid obj={a} pictureId={undefined}  key={arr.indexOf(a)+'pictures-object'}/>)}</>
    } else {
        return <Loading/>
    }
}

const Mangas = (props) => { // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {arr, bool} = props;
    const x=Boolean(arr[0].mangaid);
    const y = typeof arr[0]==="number"
    if(bool && y){
        return  <>{arr.map(a => <MangasGrid idManga={a} obj={undefined} key={a+'mangas-object'}/>)}</>
    } else if(x) {
        return <>{arr.map(a => <MangasGrid obj={a} idManga={undefined} key={a.mangaid+'mangas-object'}/>)}</>
    } else {
        return <Loading/>
    }
}

const Photos = (props) => {  // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    const {arr, bool} = props;
    const x=Boolean(arr[0].idphotos);
    const y = typeof arr[0]==="number";
    if(bool && y){
        return  <>{arr.map(a => <ThreeDGrid obj={undefined} photoId={a} key={a+'pictures-object'}/>)}</>
    }else if(x) {
        return  <>{arr.map(a => <ThreeDGrid obj={a} photoId={undefined} key={a.idphotos+'pictures-object'}/>)}</>
    } else {
        return <Loading/>
    }
}

const Drawings = (props) => {
    const {arr, bool} = props;
    const x=Boolean(arr[0].iddrawings);
    const y = typeof arr[0]==="number";
    if(bool && y){
        return  <>{arr.map(a => <DrawingsGrid obj={undefined} drawingId={a} key={a+'pictures-object'}/>)}</>
    }else if(x) {
        return  <>{arr.map(a => <DrawingsGrid obj={a} drawingId={undefined} key={a.iddrawings+'pictures-object'}/>)}</>
    } else {
        return <Loading/>
    }
}

const Apps = (props) => {
    const {arr} = {props};
    return <>{arr.map(a=><AppsGrid obj={a} key={a.idapps}/>)}</>
}

const Response = (props) => { // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    return <h1 className="gallery-category-incline">{props?"Welcome to my gallery, please choose a category to view my work":"Добре дошли в галерията ми, изберете категория, за да можете да разглеждата работата ми"}</h1>
}

const Loading = () => { // --------------------------------------------------------------------------------READY IN BOTH NORMAL AND FAVORITE
    return <div className="loading-circle"></div>
}
//------------------------------------------------------------------SINGLE PAGES--------------------------------------------------//
const ZooomSinglePages = (props) => {
    const {obj} = props;
    if(!!obj[0]){
        return <ZoomMangas obj={obj}/>;
    }
    switch(Object.keys(obj)[0]){
        case 'idvideos':
            return <ZoomVideoPage obj={obj}/>;
        case 'iddrawings':
            return <ZoomTraditionalArt obj={obj}/>
        case 'idpictures':
             return <ZoomDigitalArt obj={obj}/>
             case 'idphotos':
                return <Zoom3dmodels obj={obj}/>
        // case 'animations':
        //     return <Videos arr={arr} bool={bool}/>
        // case 'manga':
        //     return <Mangas arr={arr} bool={bool}/>
        // case 'apps-and-software':
        //     if(arr[0]?(!(arr[0].idapps)):false &&  (typeof arr[0]!=="string")){
        //         return <Loading/>
        //     }
        //     return <Apps arr={arr} bool={bool}/>
        default: 
            return <></>
    }

}
const ZoomMangas = (props) =>{
    const {obj} = props
    const [curPageNumber, changePageNumber] = useState(0);
    const [sizes, changeSize] = useState("2");
    const [pageSize, changePage] = useState("1");
    const [response, setResponse] = useState("");
    const [settingsId, toggleSettingsState] = useState("2");
    const bool = window.localStorage.getItem('language')==="true"


    const changePageEvent = event => {
        event.preventDefault()
        changePage(event.target.name);
        if(sizes==="2" && event.target.name==="2"){
            document.documentElement.requestFullscreen();
        } else {
            if(document.fullscreenElement){
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                  }
            }
        }
    }

    useEffect(()=>{
        obj.forEach((picture) => {
            const img = new Image();
            img.src = picture.link;
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    useEffect(()=>{
        if(pageSize==="1"){
           const el = document.querySelector(".button-response");
            el.focus()
        } else {
            const el = document.querySelector(".button-response1");
            el.focus()
        }
    },[pageSize])

    const toggleSettings = event => {
      event.preventDefault()
      if(settingsId==="1"){
          toggleSettingsState("2")
      } else {
          toggleSettingsState("1");
      }
    }
    const renderPageButton = event => {
        event.preventDefault();
        const {target} = event
        if(target.name==="next"){
            changePageNumber((curPageNumber+1));
        } else {
            changePageNumber((curPageNumber-1));
        }
    }
    const closePage = event => {
        event.preventDefault()
        if(event.target.name==="black-back"){
            const loc = window.location.hash;
            const linkToGoTo = loc.split('/')
            linkToGoTo.pop();
            window.location.hash = `${linkToGoTo.join('/')}`;
            window.location.reload();
        }
    }
    const eventSize = event => {
        event.preventDefault()
        const {target} = event
        // eslint-disable-next-line eqeqeq
        if(target.name==undefined){
            changeSize(target.parentNode.name);
            if(target.parentNode.name==="2"){
            }
        } else {
            changeSize(event.target.name);
            if(target.name==="2" && pageSize==="2"){
                document.documentElement.requestFullscreen();
            } else {
                if(document.fullscreenElement){
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                      }
                }
            }
        }
    }
    useEffect(()=>{
    },[curPageNumber])
    const navigateEvent = event => {
        event.preventDefault()
        const {code} = event;
        if(code==="ArrowRight"){
            if(curPageNumber===obj.length-1){
                setResponse(bool?"Last page":"Последна страница")
                setTimeout(() => {
                    setResponse("")
                }, 1000);
            } else {
                changePageNumber(curPageNumber+1)

            }
        }
        if(code==="ArrowLeft"){
            if(curPageNumber===0){
                setResponse(bool?"First page":"Първа страница");
                setTimeout(() => {
                    setResponse("")
                }, 1000);
            } else {
                changePageNumber(curPageNumber-1)
            }
        }
    }
    return <div className="zoom-page-layout">
        <button className="button-response"  tabIndex={-1} onKeyDown={navigateEvent}></button>
            <div className={"settings-window"+settingsId+pageSize}>
                            <h1 className={"settings-window-title"+pageSize}>{bool?"Change player-palette":"Сменете цветовата палета на плейъра"}</h1>
                            <div className="palette1">
                                <input type="radio" id="color-one" className="radio-button-choices" name="theme" value="blue-white"/>
                                <label htmlFor="color-one" className="color-one-label">{bool?"Blue":"Синя"}</label>
                            </div>
                            <div className="palette2">
                                <input type="radio" id="color-two"  className="radio-button-choices" name="theme" value="green-red"/>
                                <label className="color-two-label" htmlFor="color-two">{bool?"Green":"Зелена"}</label>
                            </div>
                            <div className="palette3">
                                <input type="radio" id="color-three"  className="radio-button-choices" name="theme" value="yellow-orange"/>
                                <label className="color-three-label" htmlFor="color-three">{bool?"Yellow":"Жълта"}</label>
                            </div>
                            <div className="palette4">
                                <input type="radio" id="color-four"  className="radio-button-choices" name="theme" value="purple-pink"/>
                                <label className="color-four-label" htmlFor="color-four">{bool?"Purple":"Лилава"}</label>
                            </div>
            </div>
        <div className={"zoom-page-settings-bar-d-art"+pageSize}>
 
            <button className="settings-zoom-page" id="zoom-video-cog-button" name="settings-button"  onClick={toggleSettings}>⚙</button>
           <button onClick={changePageEvent} className={"settings-zoom-page-fullscreen"+pageSize} id={"zoom-video-fullscreen-button"+pageSize} name={pageSize==="1"?"2":"1"}>⛶</button>

            <button onClick={closePage} name="black-back" className={"close-zoom-page"+pageSize} id={"zoom-video-close-button"+pageSize}>X</button>
        </div>
        <div className={`zoom-single-d-art-page`+pageSize} tabIndex={-1} onKeyDown={navigateEvent}>
            <button className="button-response1"  tabIndex={-1} onKeyDown={navigateEvent} ></button>

            <div className="size-choices3">
                <h1 className="sizes-header">{bool?"Sizes: ":"Размери: "}&nbsp;</h1>
                <button onClick={eventSize} className="size-choice" id="size-choice-1" cursor={sizes==="1"?"0":"1"} style={{opacity:sizes==="1"?"0":"1"}} name={"1"}>1</button>
                <button onClick={eventSize} className="size-choice" id="size-choice-2" cursor={sizes==="2"?"0":"1"} style={{opacity:sizes==="2"?"0":"1"}} name={"2"}>2</button>
            </div>
            {curPageNumber>=1?<button className={`prev-page${sizes}${pageSize}`} onClick={renderPageButton} name="prev">&lt;</button>:<></>}
            <img src={obj[curPageNumber].link} alt="lol" title="lol" tabIndex={-1} onKeyDown={navigateEvent} className={"manga-placeholder"+sizes+pageSize}/>
            {curPageNumber<obj.length-1?<button className={`next-page${sizes}${pageSize}`} onClick={renderPageButton} name="next">&gt;</button>:<></>}
            <h1 className={(sizes==="1"?"zoom-response-d-art1":"zoom-response-d-art2")+(pageSize)} opacity={response===""?"0":"1"} style={{padding:response===""?"0":"2%"}}>{response}</h1>
        </div>
    </div>
}
const Zoom3dmodels = (props) =>{
    const {obj} = props
    const [sizes, changeSize] = useState("1");
    const [pageSize, changePage] = useState("1");
    const [response, setResponse] = useState("");
    const [settingsId, toggleSettingsState] = useState("2");
    const changePageEvent = event => {
        event.preventDefault()
        changePage(event.target.name);
    }

    useEffect(()=>{
        if(pageSize==="1"){
           const el = document.querySelector(".button-response");
            el.focus()
        } else {
            const el = document.querySelector(".button-response1");
            el.focus()
        }
    },[pageSize])
    const bool = window.localStorage.getItem('language')==="true"

    const toggleSettings = event => {
      event.preventDefault()
      if(settingsId==="1"){
          toggleSettingsState("2")
      } else {
          toggleSettingsState("1");
      }
    }
    const closePage = event => {
        event.preventDefault()
        if(event.target.name==="black-back"){
            const loc = window.location.hash;
            const linkToGoTo = loc.split('/')
            linkToGoTo.pop();
            window.location.hash = `${linkToGoTo.join('/')}`;
            window.location.reload();
        }
    }
    const eventSize = event => {
        event.preventDefault()
        const {target} = event
        // eslint-disable-next-line eqeqeq
        if(target.name==undefined){
            changeSize(target.parentNode.name);
        } else {
            changeSize(event.target.name);
        }
    }
    const navigateEvent = event => {
        event.preventDefault()
        const {code} = event;
        if(code.includes("Digit")){
            const pressed = code.split("Digit")[1];
            if(/[1-2]/.test(pressed)){
                if(pressed===sizes){
                    setResponse(bool?"The picture is already in this size":"Картината вече е в този размер")
                    setTimeout(() => {
                        setResponse("")
                    }, 1000);
                } else {
                    changeSize(pressed);
                    setResponse("")
                }
            }
        } 
    }
    return <div className="zoom-page-layout">
        <button className="button-response"  tabIndex={-1} onKeyDown={navigateEvent}></button>
            <div className={"settings-window"+settingsId+pageSize}>
                            <h1 className={"settings-window-title"+pageSize}>{bool?"Change player-palette":"Сменете цветовата палета на плейъра"}</h1>
                            <div className="palette1">
                                <input type="radio" id="color-one" className="radio-button-choices" name="theme" value="blue-white"/>
                                <label htmlFor="color-one" className="color-one-label">{bool?"Blue":"Синя"}</label>
                            </div>
                            <div className="palette2">
                                <input type="radio" id="color-two"  className="radio-button-choices" name="theme" value="green-red"/>
                                <label className="color-two-label" htmlFor="color-two">{bool?"Green":"Зелена"}</label>
                            </div>
                            <div className="palette3">
                                <input type="radio" id="color-three"  className="radio-button-choices" name="theme" value="yellow-orange"/>
                                <label className="color-three-label" htmlFor="color-three">{bool?"Yellow":"Жълта"}</label>
                            </div>
                            <div className="palette4">
                                <input type="radio" id="color-four"  className="radio-button-choices" name="theme" value="purple-pink"/>
                                <label className="color-four-label" htmlFor="color-four">{bool?"Purple":"Лилава"}</label>
                            </div>
            </div>
        <div className={"zoom-page-settings-bar-d-art"+pageSize}>
 
            <button className="settings-zoom-page" id="zoom-video-cog-button" name="settings-button"  onClick={toggleSettings}>⚙</button>
           <button onClick={changePageEvent} className={"settings-zoom-page-fullscreen"+pageSize} id={"zoom-video-fullscreen-button"+pageSize} name={pageSize==="1"?"2":"1"}>⛶</button>

            <button onClick={closePage} name="black-back" className={"close-zoom-page"+pageSize} id={"zoom-video-close-button"+pageSize}>X</button>
        </div>
        <div className={`zoom-single-d-art-page`+pageSize} tabIndex={-1} onKeyDown={navigateEvent}>
            <button className="button-response1"  tabIndex={-1} onKeyDown={navigateEvent} ></button>

            <div className="size-choices2">
                <h1 className="sizes-header">{bool?"Sizes: ":"Размери: "}&nbsp;</h1>
                <button onClick={eventSize} className="size-choice" id="size-choice-1" cursor={sizes==="1"?"0":"1"} style={{opacity:sizes==="1"?"0":"1"}} name={"1"}>1</button>
                <button onClick={eventSize} className="size-choice" id="size-choice-2" cursor={sizes==="2"?"0":"1"} style={{opacity:sizes==="2"?"0":"1"}} name={"2"}>2</button>
            </div>
            <img src={obj.link} alt="lol" title="lol" tabIndex={-1} onKeyDown={navigateEvent}  className={"d-art-placeholder"+sizes+pageSize}/>
            <h1 className={(sizes==="1"?"zoom-response-d-art1":"zoom-response-d-art2")+(pageSize)} opacity={response===""?"0":"1"} style={{padding:response===""?"0":"2%"}}>{response}</h1>
        </div>
    </div>
}
const ZoomDigitalArt = (props) =>{
    const {obj} = props
    const [sizes, changeSize] = useState("1");
    const [pageSize, changePage] = useState("1");
    const [response, setResponse] = useState("");
    const [settingsId, toggleSettingsState] = useState("2");
    const changePageEvent = event => {
        event.preventDefault()
        changePage(event.target.name);
    }

    useEffect(()=>{
        if(pageSize==="1"){
           const el = document.querySelector(".button-response");
            el.focus()
        } else {
            const el = document.querySelector(".button-response1");
            el.focus()
        }
    },[pageSize])
    const bool = window.localStorage.getItem('language')==="true"

    const toggleSettings = event => {
      event.preventDefault()
      if(settingsId==="1"){
          toggleSettingsState("2")
      } else {
          toggleSettingsState("1");
      }
    }
    const closePage = event => {
        event.preventDefault()
        if(event.target.name==="black-back"){
            const loc = window.location.hash;
            const linkToGoTo = loc.split('/')
            linkToGoTo.pop();
            window.location.hash = `${linkToGoTo.join('/')}`;
            window.location.reload();
        }
    }
    const eventSize = event => {
        event.preventDefault()
        const {target} = event
        // eslint-disable-next-line eqeqeq
        if(target.name==undefined){
            changeSize(target.parentNode.name);
        } else {
            changeSize(event.target.name);
        }
    }
    const navigateEvent = event => {
        event.preventDefault()
        const {code} = event;
        if(code.includes("Digit")){
            const pressed = code.split("Digit")[1];
            if(/[1-2]/.test(pressed)){
                if(pressed===sizes){
                    setResponse(bool?"The picture is already in this size":"Картината вече е в този размер")
                    setTimeout(() => {
                        setResponse("")
                    }, 1000);
                } else {
                    changeSize(pressed);
                    setResponse("")
                }
            }
        }
    }
    return <div className="zoom-page-layout">
        <button className="button-response"  tabIndex={-1} onKeyDown={navigateEvent}></button>
            <div className={"settings-window"+settingsId+pageSize}>
                            <h1 className={"settings-window-title"+pageSize}>{bool?"Change player-palette":"Сменете цветовата палета на плейъра"}</h1>
                            <div className="palette1">
                                <input type="radio" id="color-one" className="radio-button-choices" name="theme" value="blue-white"/>
                                <label htmlFor="color-one" className="color-one-label">{bool?"Blue":"Синя"}</label>
                            </div>
                            <div className="palette2">
                                <input type="radio" id="color-two"  className="radio-button-choices" name="theme" value="green-red"/>
                                <label className="color-two-label" htmlFor="color-two">{bool?"Green":"Зелена"}</label>
                            </div>
                            <div className="palette3">
                                <input type="radio" id="color-three"  className="radio-button-choices" name="theme" value="yellow-orange"/>
                                <label className="color-three-label" htmlFor="color-three">{bool?"Yellow":"Жълта"}</label>
                            </div>
                            <div className="palette4">
                                <input type="radio" id="color-four"  className="radio-button-choices" name="theme" value="purple-pink"/>
                                <label className="color-four-label" htmlFor="color-four">{bool?"Purple":"Лилава"}</label>
                            </div>
            </div>
        <div className={"zoom-page-settings-bar-d-art"+pageSize}>
 
            <button className="settings-zoom-page" id="zoom-video-cog-button" name="settings-button"  onClick={toggleSettings}>⚙</button>
           <button onClick={changePageEvent} className={"settings-zoom-page-fullscreen"+pageSize} id={"zoom-video-fullscreen-button"+pageSize} name={pageSize==="1"?"2":"1"}>⛶</button>

            <button onClick={closePage} name="black-back" className={"close-zoom-page"+pageSize} id={"zoom-video-close-button"+pageSize}>X</button>
        </div>
        <div className={`zoom-single-d-art-page`+pageSize} tabIndex={-1} onKeyDown={navigateEvent}>
            <button className="button-response1"  tabIndex={-1} onKeyDown={navigateEvent} ></button>

            <div className="size-choices2">
                <h1 className="sizes-header">{bool?"Sizes: ":"Размери: "}&nbsp;</h1>
                <button onClick={eventSize} className="size-choice" id="size-choice-1" cursor={sizes==="1"?"0":"1"} style={{opacity:sizes==="1"?"0":"1"}} name={"1"}>1</button>
                <button onClick={eventSize} className="size-choice" id="size-choice-2" cursor={sizes==="2"?"0":"1"} style={{opacity:sizes==="2"?"0":"1"}} name={"2"}>2</button>
            </div>
            <img src={obj.link} alt="lol" title="lol" tabIndex={-1} onKeyDown={navigateEvent}  className={"d-art-placeholder"+sizes+pageSize}/>
            <h1 className={(sizes==="1"?"zoom-response-d-art1":"zoom-response-d-art2")+(pageSize)} opacity={response===""?"0":"1"} style={{padding:response===""?"0":"2%"}}>{response}</h1>
        </div>
    </div>
}
const ZoomTraditionalArt = (props) =>{
    const {obj} = props
    const [sizes, changeSize] = useState("1");
    const [pageSize, changePage] = useState("1");
    const [response, setResponse] = useState("");
    const [settingsId, toggleSettingsState] = useState("2");
    const changePageEvent = event => {
        event.preventDefault()
        changePage(event.target.name);
    }

    useEffect(()=>{
        if(pageSize==="1"){
           const el = document.querySelector(".button-response");
            el.focus()
        } else {
            const el = document.querySelector(".button-response1");
            el.focus()
        }
    },[pageSize])
    const bool = window.localStorage.getItem('language')==="true"

    const toggleSettings = event => {
      event.preventDefault()
      if(settingsId==="1"){
          toggleSettingsState("2")
      } else {
          toggleSettingsState("1");
      }
    }
    const closePage = event => {
        event.preventDefault()
        if(event.target.name==="black-back"){
            const loc = window.location.hash;
            const linkToGoTo = loc.split('/')
            linkToGoTo.pop();
            window.location.hash = `${linkToGoTo.join('/')}`;
            window.location.reload();
        }
    }
    const eventSize = event => {
        event.preventDefault()
        const {target} = event
        // eslint-disable-next-line eqeqeq
        if(target.name==undefined){
            changeSize(target.parentNode.name);
        } else {
            changeSize(event.target.name);
        }
    }
    const navigateEvent = event => {
        event.preventDefault()
        const {code} = event;
        if(code.includes("Digit")){
            const pressed = code.split("Digit")[1];
            if(/[1-2]/.test(pressed)){
                if(pressed===sizes){
                    setResponse(bool?"The picture is already in this size":"Картината вече е в този размер")
                    setTimeout(() => {
                        setResponse("")
                    }, 1000);
                } else {
                    changeSize(pressed);
                    setResponse("")
                }
            }
        }
    }
    return <div className="zoom-page-layout">
        <button className="button-response"  tabIndex={-1} onKeyDown={navigateEvent}></button>
            <div className={"settings-window"+settingsId+pageSize}>
                            <h1 className={"settings-window-title"+pageSize}>{bool?"Change player-palette":"Сменете цветовата палета на плейъра"}</h1>
                            <div className="palette1">
                                <input type="radio" id="color-one" className="radio-button-choices" name="theme" value="blue-white"/>
                                <label htmlFor="color-one" className="color-one-label">{bool?"Blue":"Синя"}</label>
                            </div>
                            <div className="palette2">
                                <input type="radio" id="color-two"  className="radio-button-choices" name="theme" value="green-red"/>
                                <label className="color-two-label" htmlFor="color-two">{bool?"Green":"Зелена"}</label>
                            </div>
                            <div className="palette3">
                                <input type="radio" id="color-three"  className="radio-button-choices" name="theme" value="yellow-orange"/>
                                <label className="color-three-label" htmlFor="color-three">{bool?"Yellow":"Жълта"}</label>
                            </div>
                            <div className="palette4">
                                <input type="radio" id="color-four"  className="radio-button-choices" name="theme" value="purple-pink"/>
                                <label className="color-four-label" htmlFor="color-four">{bool?"Purple":"Лилава"}</label>
                            </div>
            </div>
        <div className={"zoom-page-settings-bar-trad"+pageSize}>
 
            <button className="settings-zoom-page" id="zoom-video-cog-button" name="settings-button"  onClick={toggleSettings}>⚙</button>
           <button onClick={changePageEvent} className={"settings-zoom-page-fullscreen"+pageSize} id={"zoom-video-fullscreen-button"+pageSize} name={pageSize==="1"?"2":"1"}>⛶</button>

            <button onClick={closePage} name="black-back" className={"close-zoom-page"+pageSize} id={"zoom-video-close-button"+pageSize}>X</button>
        </div>
        <div className={`zoom-single-trad-page`+pageSize} tabIndex={-1} onKeyDown={navigateEvent}>
            <button className="button-response1"  tabIndex={-1} onKeyDown={navigateEvent} ></button>

            <div className="size-choices1">
                <h1 className="sizes-header">{bool?"Sizes: ":"Размери: "}&nbsp;</h1>
                <button onClick={eventSize} className="size-choice" id="size-choice-1" cursor={sizes==="1"?"0":"1"} style={{opacity:sizes==="1"?"0":"1"}} name={"1"}>1</button>
                <button onClick={eventSize} className="size-choice" id="size-choice-2" cursor={sizes==="2"?"0":"1"} style={{opacity:sizes==="2"?"0":"1"}} name={"2"}>2</button>
            </div>
            <img src={obj.link} alt="lol" title="lol" tabIndex={-1} onKeyDown={navigateEvent}  className={"trad-placeholder"+sizes+pageSize}/>
            <h1 className={(sizes==="1"?"zoom-response-trad1":"zoom-response-trad2")+(pageSize)} opacity={response===""?"0":"1"} style={{padding:response===""?"0":"2%"}}>{response}</h1>
        </div>
    </div>
}

const ZoomVideoPage = (props) =>{
    const {obj} = props
    const link = obj.link.split(' ')[1];
    const link2 = link.slice(5,link.length-1).replace('controls=0',"controls=1").replace('disablekb=1', 'disablekb=0')
    const [sizes, changeSize] = useState("2");
    // eslint-disable-next-line no-unused-vars
    const [pageSize, changePage] = useState("1");
    const [response, setResponse] = useState("");
    const [display, setDisplay] = useState("block")
    const [settingsId, toggleSettingsState] = useState("2");
    const changePageEvent = event => {
        event.preventDefault()
  
    }
    useEffect(()=>{
        if(pageSize==="1"){
           const el = document.querySelector(".button-response");
            el.focus()
        } else {
            const el = document.querySelector(".button-response1");
            el.focus()
        }
    },[pageSize])
    const bool = window.localStorage.getItem('language')==="true"

    const toggleSettings = event => {
      event.preventDefault()
      if(settingsId==="1"){
          toggleSettingsState("2")
      } else {
          toggleSettingsState("1");
      }
    }
    // const changeTheme = event => {
        
    // }
    const closePage = event => {
        event.preventDefault()
        if(event.target.name==="black-back"){
            const loc = window.location.hash;
            const linkToGoTo = loc.split('/')
            linkToGoTo.pop();
            window.location.hash = `${linkToGoTo.join('/')}`;
            window.location.reload();
        }

    }
    const changeFocuse = event => {
        event.preventDefault()
        if(event.target.title==="lol"){
            setTimeout(() => {
                if(pageSize==="1"){
                    const el = document.querySelector(".button-response");
                     el.focus()
                 } else {
                     const el = document.querySelector(".button-response1");
                     el.focus()
                 }
            }, 100);
        } else {
            if(pageSize==="1"){
                const el = document.querySelector(".button-response");
                 el.focus()
             } else {
                 const el = document.querySelector(".button-response1");
                 el.focus()
             }
        }

    }
    useEffect(()=>{
        setTimeout(() => {
            setDisplay("none");
        }, 5000);
    },[])
    const eventSize = event => {
        event.preventDefault()
        const {target} = event
        // eslint-disable-next-line eqeqeq
        if(target.name==undefined){
            if(sizes==="4"){
                toggleFullScreen();   
            }
            changeSize(target.parentNode.name);
            if(target.parentNode.name==="4"){
                toggleFullScreen();   
            }
        } else {
            if(sizes==="4"){
                toggleFullScreen();   
            }
            changeSize(event.target.name);
            if(target.name==="4"){
                toggleFullScreen();   
            }
        }
    }
    const toggleFullScreen =()=>{
        
        if (!document.fullscreenElement) {
            if(settingsId==="1"){
                toggleSettingsState("2");
            }
            if(pageSize==="2"){
                setTimeout(() => {
                    document.documentElement.requestFullscreen();
                    
                }, 100);
            } else {
                    document.documentElement.requestFullscreen();

            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
              }
        }
      }
    const navigateEvent = event => {
        event.preventDefault()
        const {code} = event;
        if(code==="Space"){
            const el = document.querySelector(`.video-placeholder${sizes}`);
            el.focus();
        }
        if(code.includes("Digit")){
            const pressed = code.split("Digit")[1];
            if(/[1-3]/.test(pressed) && sizes!=="4"){
                if(pressed===sizes){
                    setResponse(bool?"The video is already in this size":"Видеото вече е в този размер")
                    setTimeout(() => {
                        setResponse("")
                    }, 1000);
                } else {
                    changeSize(pressed);
                    setResponse("")
                    setDisplay("none")
                }
            }
        } else {
                if(code==="KeyF" && sizes!=="4"){
                    if(document.fullscreenEnabled){
                        toggleFullScreen();
                        setResponse("");
                        changeSize("4");
                    }
                    else{
                        setResponse("")
                    }
                }
                if((code==="Escape" || code==="KeyF") && sizes==="4"){

                    if(document.fullscreenElement){
                        toggleFullScreen();
                    }                         
                    changeSize("2")
                    setResponse("");

                }
        }
    }
    return <div className="zoom-page-layout">
        <button className="button-response"  tabIndex={-1} onKeyDown={navigateEvent}></button>
        <p className="zoom-video-instructions" style={{display:display}}>{bool?"Controls:":"Контроли:"}<br/>{bool?"<1> <2> and <3> for sizes":"<1> <2> и <3> за размерите"}<br/>{bool?"<F> for fullscreen":"<F> за цял екран"}<br/>{bool?"<Esc> or <F> to exit fullscreen":"<Esc> или <F> за изход от цял екран"}</p>
            <div className={"settings-window"+settingsId+pageSize}>
                            <h1 className={"settings-window-title"+pageSize}>{bool?"Change player-palette":"Сменете цветовата палета на плейъра"}</h1>
                            <div className="palette1">
                                <input type="radio" id="color-one" className="radio-button-choices" name="theme" value="blue-white"/>
                                <label htmlFor="color-one" className="color-one-label">{bool?"Blue":"Синя"}</label>
                            </div>
                            <div className="palette2">
                                <input type="radio" id="color-two"  className="radio-button-choices" name="theme" value="green-red"/>
                                <label className="color-two-label" htmlFor="color-two">{bool?"Green":"Зелена"}</label>
                            </div>
                            <div className="palette3">
                                <input type="radio" id="color-three"  className="radio-button-choices" name="theme" value="yellow-orange"/>
                                <label className="color-three-label" htmlFor="color-three">{bool?"Yellow":"Жълта"}</label>
                            </div>
                            <div className="palette4">
                                <input type="radio" id="color-four"  className="radio-button-choices" name="theme" value="purple-pink"/>
                                <label className="color-four-label" htmlFor="color-four">{bool?"Purple":"Лилава"}</label>
                            </div>
            </div>
        <div className={"zoom-page-settings-bar-video"+pageSize}>
 
            <button className="settings-zoom-page" id="zoom-video-cog-button" name="settings-button"  onClick={toggleSettings}>⚙</button>
           <button onClick={changePageEvent} className={"settings-zoom-page-fullscreen"+pageSize} id={"zoom-video-fullscreen-button"+pageSize} name={pageSize==="1"?"2":"1"}>⛶</button>

            <button onClick={closePage} name="black-back" className={"close-zoom-page"+pageSize} id={"zoom-video-close-button"+pageSize}>X</button>
        </div>
        <div className={`zoom-single-video-page`+pageSize} tabIndex={-1} onKeyDown={navigateEvent} onMouseOver={changeFocuse}>
            <button className="button-response1"  tabIndex={-1} onKeyDown={navigateEvent} ></button>

            <div className="size-choices">
                <h1 className="sizes-header">{bool?"Sizes: ":"Размери: "}&nbsp;</h1>
                <button onClick={eventSize} className="size-choice" id="size-choice-1" cursor={sizes==="1"?"0":"1"} style={{opacity:sizes==="1"?"0":"1"}} name={"1"}>1</button>
                <button onClick={eventSize} className="size-choice" id="size-choice-2" cursor={sizes==="2"?"0":"1"} style={{opacity:sizes==="2"?"0":"1"}} name={"2"}>2</button>
                <button onClick={eventSize} className="size-choice" id="size-choice-3" cursor={sizes==="3"?"0":"1"} style={{opacity:sizes==="3"?"0":"1"}} name={"3"}>3</button>
            </div>
            <button onMouseDownCapture={eventSize}  id={"size-choice-4-"+sizes} name={"4"}><p className="enter-fullscreen">🡤🡥🡧🡦</p></button>
            <iframe src={link2} title="lol" tabIndex={-1} onKeyDown={navigateEvent} onMouseMove={changeFocuse} onMouseOver={changeFocuse} className={"video-placeholder"+sizes} width='560' height='315' frameBorder='0'/>
            {sizes==="4"?<button onClick={eventSize} name={"3"} className="zoom-video-close-fullscreen-button"><p className="exit-fullscreen">🡦🡧🡥🡤</p></button>:<></>}
            <h1 className="zoom-response">{response}</h1>
        </div>
    </div>
}
// const Inception = (props) =>{
//     const {obj} = props
//     const link = obj.link.split(' ')[1].split('=');
//     const closePage = event => {
//         event.preventDefault()
//         const loc = window.location.hash;
//         const linkToGoTo = loc.split('/')
//         linkToGoTo.pop();
//         window.location.hash = `${linkToGoTo.join('/')}`;
//         window.location.reload();
//     }
//     return <div onClick={closePage} className="zoom-page-layout">
//         <div className="zoom-page-settings-bar-video1">
//             <button className="settings-zoom-page">X</button>

//             <button className="close-zoom-page">X</button>
//         </div>
//         <div className="zoom-single-video-page">
//             <iframe src={"link"} title={obj.idvideos +"zoom-page-video"} className="video-placeholder-inception" width='560' height='315' frameborder='0'></iframe>
//         </div>
//     </div>
// }
//----------------------------------------------------------------------------------------------------------
const Gallery = (props) => {
     const [activeQuery, setActiveQuery] =useState(window.location.hash)
    let params = activeQuery.split('/');
    let activeBranch = params[2];                                                                                                       
    let pageQuery = params[3];
    // eslint-disable-next-line no-unused-vars
    let idQuery = params[4];
    let pageSet = pageQuery?pageQuery.split('?')[1].split(';'):null;


    let pageNumber = pageSet?pageSet[0].split('=')[1]:null;
    let order = pageSet?pageSet[1]:null;
    const [queryData, setQueryData] = useState([order, activeBranch, pageNumber]);
    const [dataToDisplay, changeData] = useState([]);
    const [totalCount, setCount] = useState(0)
    const [favoriteBool, toggleFavBool] = useState(localStorage.isInFavorites?(localStorage.isInFavorites==="true"):false)
    const [singleObjectToLoad, loadObject] = useState([{}]);
    useEffect(()=>{
        const favDraw = localStorage.getItem('favoriteDrawings');
        if(favDraw===null){
            localStorage.setItem('favoriteDrawings','');
        }
        const favAn = localStorage.getItem('favoriteAnimations');
        if(favAn===null){
            localStorage.setItem('favoriteAnimations','');
        }
        const favPic = localStorage.getItem('favoritePictures');
        if(favPic===null){
            localStorage.setItem('favoritePictures','');
        }
        const favModels = localStorage.getItem('favoritePhotos');
        if(favModels===null){
            localStorage.setItem('favoritePhotos','');
        }
        const favMangas = localStorage.getItem('favoriteMangas');
        if(favMangas===null){
            localStorage.setItem('favoriteMangas','');
        }
    },[favoriteBool])
    window.onhashchange = function () {
        if(window.location.hash.includes('gallery')){
            setActiveQuery(window.location.hash);
        } else {
            if(dataToDisplay.length!==0){
                localStorage.removeItem("isInFavorites")
                window.location.reload();
            }
        }
    }
    useEffect(()=>{
        const params = activeQuery.split('/');
        const activeBranch = params[2];                                                                                                       
        const pageQuery = params[3];
        const idQuery = params[4];
        const pageSet = pageQuery?pageQuery.split('?')[1].split(';'):null;
        
        const idNumber =idQuery?parseInt(idQuery.split('=')[1]):null;
    
        const pageNumber = pageSet?pageSet[0].split('=')[1]:null;
        const order = pageSet?pageSet[1]:null;
        setQueryData([order,activeBranch, pageNumber, idNumber])
    },[activeQuery])

    useEffect(()=>{
    },[totalCount])
    useEffect(()=>{
        const link = queryData[0];
        const category = queryData[1];
        const page = queryData[2];
        const itemId = queryData[3];
       if(link!==null && category!==null && page!==null){
        let controller = '';
        switch(category){
            case 'digital-art':
                controller = "pictures";
                break;
            case 'traditional-art':
                controller = "drawings";
                break;
            case '3dmodels':
                controller = "photos";
                break;
            case 'animations':
                controller = "videos";
                break;
            case 'manga':
                controller = "mangas";
                break
            case 'apps-and-software':
                controller = "apps/apps";
                break
            default: 
                controller = "";
        }
        if(controller!=="mangas" && controller!=="apps/apps"){
            // CHECKING IF NOT MANGA AND ALSO NOT FAVORITES
            if(!favoriteBool){
                fetch(`https://ren-portfolio-backend.herokuapp.com/${controller}/all${link==="asc"?"":"-reverse"}/${page}`,
                {
                    method: 'GET',
                    headers:{
                        'Content-Type': 'application/json',
                    }
                }
                )
                .then(response => response.json())
                .then(res => {
                    if(itemId){
                        if(controller!=="apps/apps"){
                            const idKeyword = `id${controller}`
                            const objToSingle = res.filter(a=> a[idKeyword]===itemId)
                            loadObject(objToSingle);
                        }  
                    } else {
                        loadObject([{}]);
                    }
                    changeData(res)

                })
                .catch(a => a==="SyntaxError: Unexpected token < in JSON at position 0"?changeData([{value: "empty-gallery"}]):"")
                fetch(`https://ren-portfolio-backend.herokuapp.com/${controller}/count`,
                {
                    method: 'GET',
                    headers:{
                        'Content-Type': 'application/json',
                    }
                }
                )
                .then(response => response.json())
                .then(res => {
                    setCount(res[0]["COUNT(*)"])  
                })
            } else { 
                // IN FAVORITES BUT NOT IN MANGA
                if(controller!=="apps/apps"){
                    const favCategory = `favorite${controller[0].toUpperCase()+controller.slice(1,controller.length)}`
                    const favString = localStorage[favCategory];
                    const favArray = favString.split(';').filter(a=>a!=="").map(a=>parseInt(a));

                    if(queryData[0]==="asc"){
                        const res = favArray.slice((queryData[2]-1)*12, (queryData[2]-1)*12+12);
                        changeData([...res]);
                        if(itemId){
                            fetch(`https://ren-portfolio-backend.herokuapp.com/${controller}/id/${itemId}`,
                            {
                                method: 'GET',
                                headers:{
                                    'Content-Type': 'application/json',
                                }
                            }
                            )
                            .then(response => response.json())
                            .then(res => {
                                loadObject(res);
                            })
 
                        } else {
                            loadObject([{}]);
                        }

                        } else {
                            const res = favArray.reverse();
                            const res2 = res.slice((queryData[2]-1)*12, (queryData[2]-1)*12+12);
                            changeData(res2);

                            if(itemId){
                                fetch(`https://ren-portfolio-backend.herokuapp.com/${controller}/id/${itemId}`,
                                {
                                    method: 'GET',
                                    headers:{
                                        'Content-Type': 'application/json',
                                    }
                                }
                                )
                                .then(response => response.json())
                                .then(res => {
                                    loadObject(res);
                                })
                            loadObject([{}]);
                        }
                    }
                            setCount(favArray.length);
                    } 

            }

        } else if(controller!=="apps/apps") {
            if(!favoriteBool){
                //IN MANGA, BUT NOT IN FAVORITES
                fetch(`https://ren-portfolio-backend.herokuapp.com/mangas/icons${link==="asc"?"":"-reverse"}/${page}`,
                {
                    method: 'GET',
                    headers:{
                        'Content-Type': 'application/json',
                    }
                }
                )
                .then(response => response.json())
                .then(res => {
                    if(itemId){
                            const idKeyword = "mangaid"
                            const objToSingle = res.filter(a=> a[idKeyword]===itemId);
                            const data = {
                                "title":objToSingle[0].title,
                                "language":objToSingle[0].language
                            }
                            const readyToFetch = JSON.stringify(data);
                            fetch(`https://ren-portfolio-backend.herokuapp.com/mangas/pages/get`,
                            {
                                mode: 'cors',
                                method: 'POST',
                                body: readyToFetch,
                                headers: {
                                    'Content-Type':'application/json',
                                }
                            }
                            )
                            .then(response => response.json())
                            .then(a=> loadObject([[...a]]))
                            
                    } else {
                        loadObject([{}]);
                    }
                    changeData(res)
                })
                .catch(a => a==="SyntaxError: Unexpected token < in JSON at position 0"?changeData([{value: "empty-gallery"}]):"")
                fetch(`https://ren-portfolio-backend.herokuapp.com/mangas/count`,
                {
                    method: 'GET',
                    headers:{
                        'Content-Type': 'application/json',
                    }
                }
                )
                .then(response => response.json())
                .then(res => {
                    setCount(res[0]["COUNT(*)"])
                })
            } else {
                    // IN FAVORITES AND IN MANGA
                    if(controller!=="apps/apps"){
                        const favCategory = `favorite${controller[0].toUpperCase()+controller.slice(1,controller.length)}`
                        const favString = localStorage[favCategory];
                        const favArray = favString.length!==0?(favString.split(';').filter(a=>a!=="").map(a=>parseInt(a))):[];;
                        const res = favArray.slice((queryData[2]-1)*12, (queryData[2]-1)*12+12);
                        if(queryData[0]==="asc"){
                            changeData([...res]);
                            if(itemId){
                                const objToSingle = res.filter(a=> a===itemId);
                                if(objToSingle[0]){
                                    fetch(`https://ren-portfolio-backend.herokuapp.com/mangas/pages/id/${itemId}`,
                                    {
                                        mode: 'cors',
                                        method: 'POST',
                                        headers: {
                                            'Content-Type':'application/json',
                                        }
                                    }
                                    )
                                    .then(response => response.json())
                                    .then(a=> loadObject([[...a]]))
                                }
                            }

                        } else {
                            const res2 = res.reverse();
                            changeData(res2);
                        }
                        setCount(favArray.length);
                    } 
            }

        }
    } else {
            changeData([])
        }
    },[queryData, favoriteBool])
    const eventToggler = event => {
        event.preventDefault();
        if(localStorage.isInFavorites===undefined){
            localStorage.isInFavorites = "true"
        } else {
            if(localStorage.isInFavorites==="true"){
                localStorage.isInFavorites="false";
            } else {
                localStorage.isInFavorites="true";
            }
        }
        toggleFavBool(!favoriteBool);
        const linkToGoTo = '#/gallery/'+ queryData[1] + '/q?page=' + 1 + ';' + queryData[0];
        window.location.hash=linkToGoTo;
        window.location.reload();
    }

    const goToSearchQuery = event => {
        const { value } = event.target
        setActiveQuery(value);                                                                                              //---------------------STEP 1
        setQueryData(["asc",value.split('/')[2], 1])
        window.location.hash = value;
        if(dataToDisplay.length===0){
            window.location.reload();
        }
        }
    const changeOrderQuery = event => {
        const {value} = event.target;
        if(queryData[0]!==value){
            const linkToGoTo = '#/gallery/'+ queryData[1] + '/q?page=' + queryData[2] + ';' + value;
            window.location.hash = linkToGoTo;

            setActiveQuery(linkToGoTo)
            setQueryData([value, queryData[1], queryData[2]])
        }
    }
    const RenderPagination = (props) => {
        const {page, count} = props;

        // const count = 400;
        // const page = 11;
        const [backwards, setBack] = useState([]);
        const [forwards, setForward] = useState([]);
        // eslint-disable-next-line no-unused-vars
        const [lastPage, setLastPage] = useState(Math.ceil(count/12))
        const changePage= event => {
            event.preventDefault();
            const pageToGoTo = event.target.name;
            const linkToGoTo = '#/gallery/'+ queryData[1] + '/q?page=' + pageToGoTo + ';'+ queryData[0];
            window.location.hash = linkToGoTo;
            setQueryData([queryData[0],queryData[1],pageToGoTo])
        }
        useEffect(()=>{
            const arr = [];
            const arr2 = [];
            for(let i = parseInt(page)-1; i>1 && arr.length<5; i--){
                arr.push(i);
            }
            for(let y =parseInt(page)+1; y<lastPage && arr2.length<5; y++){
                arr2.push(y);
            }
            setBack(arr.reverse());
            setForward(arr2);
        },[page, lastPage])
        if(!page){
            return <></>;
        }
        return <div className="pagination">
            <div className="previous-pages-group">
            {parseInt(page)!==1?<button id="page-button" name={1} onClick={changePage} className="first-page-button">1</button>:<></>}
            {(2<backwards[0] && 2<parseInt(page))?<h2 className="distance-between-buttons">..</h2>:<></>}
            {parseInt(page)!==1?backwards.map(a=> <button key={"prev-button-"+ a} onClick={changePage} id="page-button" name={a} className="secondary-previous-page-buttons">{a}</button>):<></>}
            </div>

            <div className="current-page-placeholder"><h2 className="current-page-count">{page}</h2></div>

            <div className="next-pages-group">
            {parseInt(page)!==lastPage?forwards.map(a=>  <button  key={"next-button-"+ a} onClick={changePage} id="page-button1" name={a} className="secondary-next-page-buttons">{a}</button>):<></>}
            {((lastPage-1)>forwards[forwards.length-1] && page<(lastPage-1))?<h2 className="distance-between-buttons1">..</h2>:<></>}
            {(parseInt(page)<lastPage)?<button id="page-button1" onClick={changePage} name={lastPage} className="first-page-button">{lastPage}</button>:<></>}

            </div>
        </div>
    }
//-----------------------------------------------------------GALLERY RETURN
    return <>
        <Header bool={props} parent="gallery"/>
                       {singleObjectToLoad[0]!=={}?<ZooomSinglePages category={queryData[1]} obj={singleObjectToLoad[0]}/>:<></>}        
        <div className="gallery-screen">
            <div className="gallery-search-browser">
                    <div className="gallery-browser-header">
                        <div className="gallery-button-choices">

                            <button onClick={goToSearchQuery} value="#/gallery/digital-art/q?page=1;asc" className="gallery-link" id="to-digital-art-link">{props?"Digital drawings":"Дигитални картини"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/traditional-art/q?page=1;asc" className="gallery-link" id="to-traditional-art-link">{props?"Traditional drawings":"Традиционни картини"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/3dmodels/q?page=1;asc" className="gallery-link" id="to-3dmodels-link">{props?"3D models":"3D модели"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/animations/q?page=1;asc" className="gallery-link" id="to-animations-link">{props?"Animations":"Анимации"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/manga/q?page=1;asc" className="gallery-link" id="to-manga-link">{props?"Manga":"Манга"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/apps-and-software/q?page=1;asc" className="gallery-link" id="to-apps-link">{props?"Apps and software":"Приложения и програми"}</button>
                        </div>
                        {queryData[1]!==undefined?(<button onClick={eventToggler} className="toggle-favorites">{!favoriteBool?(props?`Go to favorites`:"Отидете в любими"):(props?"Go back to normal gallery":"Върнете се в нормална галерия")}</button>):<></>}
                        <div className="option-1">
                                <input type="radio"  onChange={changeOrderQuery} id="ascending" className="radio-button-choices" checked={queryData[0]==="asc"} name="order" value="asc"/>
                                <label htmlFor="ascending" className="radio-label">{props?"Oldest":"Стари"}</label>
                            </div>
                            <div className="option-2">
                                <input type="radio"  onChange={changeOrderQuery} id="descending" checked={queryData[0]==="desc"} className="radio-button-choices" name="order" value="desc"/>
                                <label className="radio-label2" htmlFor="descending">{props?"Newest":"Нови"}</label>
                            </div>
                    </div>
                    <div className="gallery-browser-body">
                       <ResultsBrowsers arr={dataToDisplay} category={queryData[1]} bool={favoriteBool}/>
                       <RenderPagination page={queryData[2]} count={totalCount}/>

                    </div>
            </div>
        </div>
    </>
}

export default Gallery;