import React, { useEffect, useState } from "react";
import Header from "./components/Header";
import { VideosGrid, PicturesGrid, MangasGrid, ThreeDGrid,DrawingsGrid, AppsGrid } from "./Gallery";

const Videos = (props) => {
    const {arr} = props;
    console.log("step 3 entered normally in videos lol")
    if(arr.length===0){

    }
    return <>{arr.map(a => <VideosGrid obj={a} id={undefined} key={a.idvideos+'videos-object'}/>)}</>
}

const Pictures = (props) => {
    console.log("step 3 entered normally in pictures")
    const {arr} = props;
    return <>{arr.map(a => <PicturesGrid obj={a} key={arr.indexOf(a)+'pictures-object'}/>)}</>
}

const Mangas = (props) => {
    const {arr} = props;
    console.log("step 3 entered normally in mangas")
    return  <>{arr.map(a => <MangasGrid obj={a} key={a.mangaid+'mangas-object'}/>)}</>
}

const Photos = (props) => {
    const {arr} = props;
    console.log("step 3 entered normally in 3d models");
    return  <>{arr.map(a=><ThreeDGrid obj={a} key={a.idphotos+'photos-object'}/>)}</>
}

const Drawings = (props) => {
    const {arr} = {props};
    console.log("step 3 entered normally in drawings");
    return <>{arr.map(a=><DrawingsGrid obj={a} key={a.iddrawings+'drawing-object'}/>)}</>
}

const Apps = (props) => {
    const {arr} = {props};
    console.log("step 3 entered normally in apps");
    return <>{arr.map(a=><AppsGrid obj={a} key={a.idapps}/>)}</>
}

const Response = (props) => {
    return <h1 className="gallery-category-incline">{props?"Welcome to my gallery, please choose a category to view my work":"Добре дошли в галерията ми, изберете категория, за да можете да разглеждата работата ми"}</h1>
}

const Loading = () => {
    return <div className="loading-circle"></div>
}

const FavoritesGallery = (props) => {
    let activeQuery = window.location.hash
     let params = activeQuery.split('/');
    let activeBranch = params[2];
    let pageQuery = params[3];
    // let idQuery = params[4];
    let pageSet = pageQuery?pageQuery.split('?')[1].split(';'):null;


    let pageNumber = pageSet?pageSet[0].split('=')[1]:null;
    let order = pageSet?pageSet[1]:null;

    const [queryData, setQueryData] = useState([order, activeBranch, pageNumber]);
    const [dataToDisplay, changeData] = useState([]);

    useEffect(()=>{
        const link = queryData[0];
        const category = queryData[1];
        const page = queryData[2];
       if(link!==null && category!==null && page!==null){
        let storage = '';
        switch(category){
            case 'digital-art':
                storage = "favoriteDigitalArt";
                break;
            case 'traditional-art':
                storage = "favoriteDrawings";
                break;
            case '3dmodels':
                storage = "favoriteModels";
                break;
            case 'animations':
                storage = "favoriteVideos";
                break;
            case 'manga':
                storage = "favoriteMangas";
                break
            case 'apps-and-software':
                storage = "favoriteApps";
                break
            default: 
                storage = "";
            }  
            const favString = localStorage.getItem(storage);
            changeData([favString.split(';').filter(a=>a!=="").slice((queryData[2]-1)*12, 12),storage])
        }
    },[queryData])

    const goToSearchQuery = event => {
        const { value } = event.target
        window.location.hash = value;
        setQueryData(["asc",value.split('/')[2], 1])
    }
    const changeOrderQuery = event => {
        const {value} = event.target;
        if(order!==value){
            const linkToGoTo = activeQuery.split(';')[0] + ';' + value;
            window.location.hash = linkToGoTo;
            setQueryData([value, ...(queryData.splice(1))])
        }
    }
    
    return <>
        <Header bool={props} parent="favorites"/>
        <div className="gallery-screen">
            <div className="gallery-search-browser">
                    <div className="gallery-browser-header">
                        <div className="gallery-button-choices">

                            <button onClick={goToSearchQuery} value="#/gallery/favorite-digital-art/q?page=1;asc" className="gallery-link" id="to-digital-art-link">{props?"Digital art":"Дигитални картини"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/favorite-traditional-art/q?page=1;asc" className="gallery-link" id="to-traditional-art-link">{props?"Traditional art":"Традиционни картини"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/favorite-3dmodels/q?page=1;asc" className="gallery-link" id="to-3dmodels-link">{props?"3D models":"3D модели"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/favorite-animations/q?page=1;asc" className="gallery-link" id="to-animations-link">{props?"Animations":"Анимации"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/favorite-manga/q?page=1;asc" className="gallery-link" id="to-manga-link">{props?"Manga":"Манга"}</button>
                            <button onClick={goToSearchQuery} value="#/gallery/favorite-apps-and-software/q?page=1;asc" className="gallery-link" id="to-apps-link">{props?"Apps and software":"Приложения и програми"}</button>
                        </div>
                        <div className="option-1">
                                <input type="radio" onChange={changeOrderQuery} id="ascending" className="radio-button-choices" checked={queryData[0]==="asc"} name="order" value="asc"/>
                                <label htmlFor="ascending" className="radio-label">{props?"Oldest":"Стари"}</label>
                            </div>
                            <div className="option-2">
                                <input type="radio" onChange={changeOrderQuery} id="descending" checked={queryData[0]==="desc"} className="radio-button-choices" name="order" value="desc"/>
                                <label className="radio-label2" htmlFor="descending">{props?"Newest":"Нови"}</label>
                            </div>
                    </div>
                    <div className="gallery-browser-body">
                        <ResultsBrowsers/>
                    </div>
            </div>
        </div>
    </>
}

export default FavoritesGallery;