

const Header = (props) => {

    return <div className="header">
        {props.parent==="about"?
        <>        
        <a id="label-font" href="#/gallery" className="gallery-content">{props.bool?"Personal gallery =":"Лична галерия ="}&gt;</a>
        </>
        :
        (props.parent==="gallery"?
        <>
        <a id="label-font" href="#/about" className="gallery-about-redirect">&lt;{props.bool?"= To my CV":"= Към CV-то ми"}</a>
        </>
        :
        <>
        <a id="label-font" href="#/about" className="gallery-about-redirect">&lt;{props.bool?"= To my CV":"= Към CV-то ми"}</a>
        </>)
        }

    </div>
}

export default Header;